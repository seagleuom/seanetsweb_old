/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seanets.repoHandler;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author teo
 */
public class ProjectInfoTest {

    ProjectInfo instance;
    String repoUrl1 = "https://code.google.com/p/universal-media-server/";
    String repoUrl2 = "https://github.com/mongodb/mongo-java-driver.git";
    String repoUrl3 = "http://git.code.sf.net/p/pmd/code";
    
    String wrongUrl = "http://git.code.uom.net/k/seanets/code";

    String name1 = "universal-media-server";
    String name2 = "mongo-java-driver";
    String name3 = "pmd";

    public ProjectInfoTest() {
        
    }

    @Before
    public void setUp() {
        instance = new ProjectInfo();

    }

    /**
     * Test of setProjectName method, of class ProjectInfo.
     */
    @Test
    public void testSetProjectRemoteRepositoryURL() {
        instance.setProjectRemoteRepositoryURL(repoUrl1);
        assertEquals(name1, instance.getProjectName());
        instance.setProjectRemoteRepositoryURL(repoUrl2);
        assertEquals(name2, instance.getProjectName());
        instance.setProjectRemoteRepositoryURL(repoUrl3);
        assertEquals(name3, instance.getProjectName());
        instance.setProjectRemoteRepositoryURL(wrongUrl);
        assertEquals(wrongUrl, instance.getProjectName());
    }

}
