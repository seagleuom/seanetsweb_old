package gr.uom.java.seanets.util;

import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.JFileChooser;
import javax.swing.ProgressMonitorInputStream;
import javax.swing.filechooser.FileNameExtensionFilter;

public class StorageManager {

    public static final String FILE_SEPARATOR = System.getProperty("file.separator");

    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    public static final String NEW_LINE = LINE_SEPARATOR;

    private static final String seanetsLocalFolder = SEANetsProperties.getInstance().getSeanetsFolderPath();
    private String evolutionAnalysisFolderPath;
    private final String pathFiltersFile;

    private static StorageManager instance;

    public static StorageManager getInstance() {
        if (instance == null) {
            instance = new StorageManager();
        }
        return instance;
    }

    private StorageManager() {

        evolutionAnalysisFolderPath = seanetsLocalFolder + FILE_SEPARATOR;
        pathFiltersFile = seanetsLocalFolder + FILE_SEPARATOR + "data" + FILE_SEPARATOR + "pathFilters.txt";
    }

    public static String getSeanetsLocalFolder() {
        return seanetsLocalFolder;
    }

    public ProjectEvolutionAnalysis loadProjectEvolutionAnalysisData(final String projectName) {

        ProjectEvolutionAnalysis projectEvolutionAnalysis = null;
        try {
            File file = new File(evolutionAnalysisFolderPath + projectName + ".ser");
            FileInputStream is = new FileInputStream(file);

            ProgressMonitorInputStream ios = null;
            ByteArrayOutputStream ous = null;

            try {
                byte[] buffer = new byte[2048];
                ous = new ByteArrayOutputStream();
                ios = new ProgressMonitorInputStream(null, "Loading " + projectName + " Evolution Analysis", is);
                int read = 0;
                while ((read = ios.read(buffer)) != -1) {
                    ous.write(buffer, 0, read);
                }
            } finally {
                try {
                    if (ous != null) {
                        ous.close();
                    }
                } catch (IOException e) {
                }

                try {
                    if (ios != null) {
                        ios.close();
                    }
                } catch (IOException e) {
                }
            }
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(ous.toByteArray()));
            ous = null;
            projectEvolutionAnalysis = (ProjectEvolutionAnalysis) ois.readObject();
            ois.close();
            is.close();
        } catch (Exception ex) {

        }
        return projectEvolutionAnalysis;
    }

    public static String loadTextFile(String path) {
        try {
            File file = new File(path);
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();
            String s = new String(data, "UTF-8");
            return s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns filters of the given project. The filter file has the form:
     * projectName,-excludeFilter1, -excludeFilter2, -excludeFilter3,
     * +includeFilter1, +includeFilter2 etc...
     *
     * @param: The name of the Project to return the filters
     * @return: The filters in String array format
     */
    private ArrayList<String> loadFiltersForProject(String projectName) {
        ArrayList<String> filters = new ArrayList<String>();
        String filtersFile = loadTextFile(pathFiltersFile);
        StringTokenizer tokenizer = new StringTokenizer(filtersFile, "\n");
        while (tokenizer.hasMoreTokens()) {
            StringTokenizer st = new StringTokenizer(tokenizer.nextToken(), ",");
            String project = st.nextToken();
            while (st.hasMoreTokens()) {
                if (project.equalsIgnoreCase(projectName)) {
                    filters.add(st.nextToken().trim());
                } else {
                    st.nextToken();
                }
            }
        }
        return filters;
    }

    private ArrayList<String> loadSpecificFiltersForProject(String projectName, String symbol) {
        ArrayList<String> filters = loadFiltersForProject(projectName);

        ArrayList<String> excludeFilters = new ArrayList<String>();
        for (String filter : filters) {

            if (filter.startsWith(symbol)) {
                excludeFilters.add(filter.replace(symbol, ""));
            }
        }
        return excludeFilters;
    }

    public ArrayList<String> loadExcludeFiltersForProject(String projectName) {
        return loadSpecificFiltersForProject(projectName, "-");
    }

    public ArrayList<String> loadIncludeFiltersForProject(String projectName) {
        return loadSpecificFiltersForProject(projectName, "+");
    }

    public boolean isProjectEvolutionAnalysisStored(String projectName) {
        try {
            File f = new File(evolutionAnalysisFolderPath + projectName + ".ser");
            if (f.exists()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public static void saveTextFileToLocalFolder(String fileName, String textToWrite) throws Exception {
        File outDir = new File(fileName);
        if (!outDir.exists()) {
            outDir.mkdir();
        }
        FileOutputStream fos = new FileOutputStream(fileName + ".txt");
        PrintStream ps = new PrintStream(fos);
        ps.print(textToWrite);
        ps.close();
        fos.close();
    }

    public static void saveTextFile(String textToWrite) throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text or csv files only", "txt", "csv");
        fileChooser.setFileFilter(filter);
        fileChooser.setDialogTitle("Specify file name");
        int userSelection = fileChooser.showSaveDialog(null);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = new File(fileChooser.getSelectedFile().getAbsolutePath());
            FileOutputStream fos = new FileOutputStream(fileToSave);
            PrintStream ps = new PrintStream(fos);
            ps.print(textToWrite);
            ps.close();
            fos.close();
        }
    }

    public void saveTextFileLocally(String fileName, String textToWrite) {
        try {
            File f = new File(fileName);
            FileOutputStream fos = new FileOutputStream(f);
            PrintStream ps = new PrintStream(fos);
            ps.print(textToWrite);
            ps.close();
            fos.close();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }

    public void saveTableToFile(String fileName, String[][] tableToPrint) throws Exception {
        StringBuilder sb = new StringBuilder();
        int rows = tableToPrint.length;
        int cols = tableToPrint[1].length;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (tableToPrint[i][j] != null) {
                    sb.append(tableToPrint[i][j]);
                } else {
                    sb.append("");
                }
                sb.append(",");
            }
            sb.append("\n");
        }
        saveTextFileLocally(fileName, sb.toString());
    }
    
    public void frequencyMapSaver(Map<Integer, Integer> map, String key, String value, String fileName)
    {
        try {
            File f = new File(fileName);
            StringBuilder sb = new StringBuilder();
            sb.append(key).append("\t").append("value").append("\n");
            for(Integer keyNumber : map.keySet()) {
                sb.append(keyNumber).append("\t").append(map.get(keyNumber)).append("\n");
            }
            FileOutputStream fos = new FileOutputStream(f);
            PrintStream ps = new PrintStream(fos);
            ps.print(sb.toString());
            ps.close();
            fos.close();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        
    }
}
