package gr.uom.java.seanets.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class StoredConfiguration implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String CONFIG_FILE_NAME =  "config.ser";
	private static final String CONFIG_FILE_PATH = SEANetsProperties.getInstance().getSeanetsFolderPath()+ File.separator + CONFIG_FILE_NAME;

	private ArrayList<String> latestCVSRepositories;
	private Set<String> downloadedProjectNames;

	private static StoredConfiguration instance;

	public static StoredConfiguration getInstance(){
		loadConfigurationFromDisk();
		if(instance == null)
			instance = new StoredConfiguration();
		return instance;
	}

	private StoredConfiguration(){
		latestCVSRepositories = new ArrayList<String>();
		downloadedProjectNames = new HashSet<String>();
		try {
			File outDir = new File(SEANetsProperties.getInstance().getSeanetsFolderPath());
			if(!outDir.exists())
				outDir.mkdir();
			File f = new File(CONFIG_FILE_PATH);
			f.createNewFile();
		//	StorageManager.makeFileUndeletable(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String[] getLatestCVSRepositories(){
		return latestCVSRepositories.toArray(new String[1]);
	}

	public void addCVSRepositoryInHistory(String repo){
		if(repo != null && !latestCVSRepositories.contains(repo))
			latestCVSRepositories.add(repo);
		saveConfigurationLocally();
	}

	public void removeProject(String projectName){
		downloadedProjectNames.contains(projectName);
		saveConfigurationLocally();
	}

	public boolean containsProject(String projectName){
		return downloadedProjectNames.contains(projectName);
	}

	public void addDownloadedProject(String projectName){
		downloadedProjectNames.add(projectName);
		saveConfigurationLocally();
	}

	public String[] getDownloadedProjectNames(){
		return downloadedProjectNames.toArray(new String[1]);
	}

	public void saveConfigurationLocally() {
		try{	
			File outDir = new File(SEANetsProperties.getInstance().getSeanetsFolderPath());
			if(!outDir.exists())
				outDir.mkdir();
			FileOutputStream fos = new FileOutputStream(CONFIG_FILE_PATH);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.flush();
			oos.close();
			fos.close();
		}
		catch(Exception ioe) { ioe.printStackTrace(); }
	}

	private static void loadConfigurationFromDisk(){
		try {
			FileInputStream fileIn = new FileInputStream(CONFIG_FILE_PATH);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			instance = (StoredConfiguration) in.readObject();
			in.close();
			fileIn.close();
		}
		catch(Exception e){
			instance = null;
		}
	}	


}