/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.uom.java.seanets.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author teohaik
 */
public class MyFileLogger {
    
    private final File logFile;
    private BufferedWriter writer;
    
    public MyFileLogger(String fileName){
        logFile = new File("/var/www/seagle/logs"+fileName+".txt");
        try {
            writer = new BufferedWriter(new FileWriter(logFile));
        } catch (IOException ex) {
            Logger.getLogger(MyFileLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void appendToLog(String text){
       try {
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.append(text + "\n");
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(MyFileLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
