/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seanets.util;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 *
 * @author Theodore Chaikalis
 */
public class JSONImporter {
    
    
    public static ArrayList<String> getVersionIDs(String versionsInJSON){
         ArrayList<String>  versionIDs = new ArrayList<>();
        try {
            JSONParser parser =new JSONParser();
            Object obj = parser.parse(versionsInJSON);
            JSONArray array = (JSONArray)obj;
            for(int i=0; i<array.size(); i++) {
                String versionID = (String)array.get(i);
                versionIDs.add(versionID);
            }
            return versionIDs;
        } catch (ParseException ex) {
            Logger.getLogger(JSONImporter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return versionIDs;
    }
    
}
