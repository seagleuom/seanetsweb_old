package gr.uom.java.seanets.util;

import java.io.File;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class SEANetsProperties {

    private static SEANetsProperties instance;
    private PropertiesConfiguration configuration;
    private static final String CONFIG_FILE_NAME = "seanets.properties";
    private static final String SEANETS_LOCAL_PATH_VARIABLE = "local_path";
    private static final String SEANETS_LOG_FOLDER_VARIABLE = "log_folder";
    private static final String SEANETS_LOCAL_PROJECTS_VARIABLE = "local_projects_folder";
    private static final String SEANETS_LOCAL_REPOS_VARIABLE = "local_repositories_folder";
    private static final String SEANETS_LOCAL_SIMULATION_FOLDER_VARIABLE = "local_simulation_folder";
    private static final String SIMULATION_RANDOMNESS = "simulation_randomness";
    private static final String SIMULATION_CUTOFF_PERCENTAGE = "cutOffPercentage";
    private static final String APPLY_DOMAIN_RULES = "apply_domain_rules";
    private static final String APPLY_AGE_RULES = "apply_age_rules";
    private static final String PRESERVE_HOP_DISTANCE = "preserve_hop_distance";
    private static final String DB_URL_VARIABLE = "db_url";
    private static final String DB_USER_VARIABLE = "db_user";
    private static final String DB_PASSWORD_VARIABLE = "db_pass";

    public static SEANetsProperties getInstance() {
        if (instance == null) {
            instance = new SEANetsProperties();
        }
        return instance;
    }

    private SEANetsProperties() {
        try {
            configuration = new PropertiesConfiguration(CONFIG_FILE_NAME);
            configuration.setAutoSave(true);
        } catch (ConfigurationException e) {
            System.err.println("Error while trying to create configuration file");
            e.printStackTrace();
        }
    }

    public String getSeanetsFolderPath() {
        return configuration.getString(SEANETS_LOCAL_PATH_VARIABLE);
    }

    public String getProjectsFolder() {
        return getSeanetsFolderPath() + File.separator + configuration.getString(SEANETS_LOCAL_PROJECTS_VARIABLE);
    }

    public String getRepositoriesFolder() {
        return getSeanetsFolderPath() + File.separator + configuration.getString(SEANETS_LOCAL_REPOS_VARIABLE);
    }

    public String getSimulationFolder() {
        return getSeanetsFolderPath() + File.separator + configuration.getString(SEANETS_LOCAL_SIMULATION_FOLDER_VARIABLE);
    }

    public String getLogFolderPath() {
        return configuration.getString(SEANETS_LOG_FOLDER_VARIABLE);
    }

    public String getDBUrl() {
        return configuration.getString(DB_URL_VARIABLE);
    }

    public String getDBUser() {
        return configuration.getString(DB_USER_VARIABLE);
    }

    public String getDBPassword() {
        return configuration.getString(DB_PASSWORD_VARIABLE);
    }

    public double getSimulationRandomness() {
        return configuration.getDouble(SIMULATION_RANDOMNESS);
    }

    public boolean getApplyDomainRules() {
        return configuration.getBoolean(APPLY_DOMAIN_RULES);
    }
    
    public boolean getApplyAgeRules() {
        return configuration.getBoolean(APPLY_AGE_RULES);
    }

    public boolean getPreserveMaxHopDistance() {
        return configuration.getBoolean(PRESERVE_HOP_DISTANCE);
    }

    public void setLocalProjectsFolderName(String newPath) {
        configuration.setProperty(SEANETS_LOCAL_PROJECTS_VARIABLE, newPath);
    }

    public double getCutOffValue() {
        return configuration.getDouble(SIMULATION_CUTOFF_PERCENTAGE);
    }

}
