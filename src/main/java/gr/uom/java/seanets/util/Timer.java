package gr.uom.java.seanets.util;


import java.util.logging.Level;

/**
 *
 * @author Theodore Chaikalis
 */
public class Timer {
    
    private final static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(Timer.class.getName());
    private long startTime;
    private long duration;
    private String title;
     
    public Timer(String text){
        title = text;
        duration = 0;
    }
    
    public void start(){
        startTime = System.currentTimeMillis();
    }
    
    public void printDurationInMinutes(){
        long mins = (long) (Math.floor(duration / 1000) / 60) ;
        long seconds = (long) (Math.floor(duration / 1000) ) ;
        logger.log(Level.INFO, "{0} finished. \t Time {1} mins, \t {2} sec", new Object[]{title, mins, seconds});
    }
    
    public void printDurationInSeconds(){
        long seconds = (long) (Math.floor(duration / 1000) ) ;
        logger.log(Level.INFO, "{0} finished. Total Time {1} sec ", new Object[]{title, seconds});
    }
    
    public void stop(){
        duration += (System.currentTimeMillis() - startTime);
    }

}
