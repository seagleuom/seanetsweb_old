package gr.uom.java.seanets.util;

import gr.uom.java.seanets.ejb.CommitVersion;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JSONExporter {

	private static String color = "#557EA";
	private static JSONObject colorObject = new JSONObject();

	@SuppressWarnings("unchecked")
	public static String exportJSONString(MyGraph graph) {
		colorObject.put("$color",color);
		JSONArray graphFullList = new JSONArray();
		for(MyNode node : graph.getVertices()) {
			Map mapForANode = new LinkedHashMap();
			Collection<MyEdge> edges = graph.getOutEdges(node);
			JSONArray adjacenciesList = new JSONArray();
			for(MyEdge edge : edges) {
				Map m1 = new LinkedHashMap();
				MyNode from = edge.getSourceNode();
				MyNode to = edge.getTargetNode();
				m1.put("nodeTo", to.getSimpleName());
				m1.put("nodeFrom", from.getSimpleName());
				m1.put("data",colorObject);
				adjacenciesList.add(m1);
			}
			mapForANode.put("adjacencies", adjacenciesList);
			mapForANode.put("id",node.getSimpleName());
			mapForANode.put("name",node.getSimpleName());
			graphFullList.add(mapForANode);
		}
		String jsonText = JSONValue.toJSONString(graphFullList);
		//System.out.println(jsonText);

		return jsonText;
	}
        
        public static String exportJSONString(List<CommitVersion> availableVersionList)
        {
            JSONArray versionFullList = new JSONArray();
            for(CommitVersion versionCommit : availableVersionList) {
                JSONObject obj = new JSONObject();
                obj.put("id", versionCommit.getCommit().getID());
                obj.put("name", versionCommit.getVersionName());
                obj.put("date", versionCommit.getDate());
                versionFullList.add(obj);
            }
            return versionFullList.toJSONString();
        }



}
