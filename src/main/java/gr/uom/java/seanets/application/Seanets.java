package gr.uom.java.seanets.application;

import gr.uom.java.seanets.ejb.RemoteRepositoryReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class Seanets {

    private static final String ANALYZE = "-a";
    private static final String SIMULATE = "-s";
    private static final String HELP = "-help";

    public static void main(String... args) throws Exception {

        if (args.length == 2) {
            if (args[0].equalsIgnoreCase(ANALYZE) && args[1].contains("http") && args[1].contains("git")) {
                String remoteRepoUrl = args[1];
                RemoteRepositoryReader reader = new RemoteRepositoryReader();

            }
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase(HELP)) {
                System.out.println(helpString);
            }
        } else {
            System.err.println("Argument must be an integer.");
        }
    }

    private static final String helpString
            = "Software Evolution Analysis with Networks \n"
            + "Command line execution example: \n\n"
            + "java Seanets -a \"remoteGitRepositoryURL\" for the analysis of the project";

    public static void main1(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        File f = new File("c:/tmp/mapped.txt");
        f.delete();

        FileChannel fc = new RandomAccessFile(f, "rw").getChannel();

        long bufferSize = 8 * 1000;
        MappedByteBuffer mem = fc.map(FileChannel.MapMode.READ_WRITE, 0, bufferSize);

        int start = 0;
        long counter = 1;
        long HUNDREDK = 100000;
        long startT = System.currentTimeMillis();
        long noOfMessage = HUNDREDK * 10 * 10;
        for (;;) {
            if (!mem.hasRemaining()) {
                start += mem.position();
                mem = fc.map(FileChannel.MapMode.READ_WRITE, start, bufferSize);
            }
            mem.putLong(counter);
            counter++;
            if (counter > noOfMessage) {
                break;
            }
        }
        fc.close();
        long endT = System.currentTimeMillis();
        long tot = endT - startT;
        System.out.println(String.format("No Of Message %s , Time(ms) %s ", noOfMessage, tot));
    }

    /**
     * @param args
     * @throws IOException
     * @throws FileNotFoundException
     * @throws InterruptedException
     */
    public static void main2(String[] args) throws FileNotFoundException, IOException, InterruptedException {

        FileChannel fc = new RandomAccessFile(new File("c:/tmp/mapped.txt"), "rw").getChannel();

        long bufferSize = 8 * 1000;
        MappedByteBuffer mem = fc.map(FileChannel.MapMode.READ_ONLY, 0, bufferSize);
        long oldSize = fc.size();

        long currentPos = 0;
        long xx = currentPos;

        long startTime = System.currentTimeMillis();
        long lastValue = -1;
        for (;;) {

            while (mem.hasRemaining()) {
                lastValue = mem.getLong();
                currentPos += 8;
            }
            if (currentPos < oldSize) {

                xx = xx + mem.position();
                mem = fc.map(FileChannel.MapMode.READ_ONLY, xx, bufferSize);
                continue;
            } else {
                long end = System.currentTimeMillis();
                long tot = end - startTime;
                System.out.println(String.format("Last Value Read %s , Time(ms) %s ", lastValue, tot));
                System.out.println("Waiting for message");
                while (true) {
                    long newSize = fc.size();
                    if (newSize > oldSize) {
                        oldSize = newSize;
                        xx = xx + mem.position();
                        mem = fc.map(FileChannel.MapMode.READ_ONLY, xx, oldSize - xx);
                        System.out.println("Got some data");
                        break;
                    }
                }
            }
        }
    }

}
