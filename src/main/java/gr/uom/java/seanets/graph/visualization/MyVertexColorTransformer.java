package gr.uom.java.seanets.graph.visualization;

import gr.uom.java.seanets.graph.MyNode;

import java.awt.Color;
import java.awt.Paint;

import org.apache.commons.collections15.Transformer;

public class MyVertexColorTransformer implements Transformer<MyNode, Paint>{

	private Color nodeColor = new Color(47,113,145);

	public Paint transform(MyNode node) {
		int alpha = (int)(255 - 6000 * node.getPageRank() ); 
		if(alpha < 50) alpha = 50;
		return new Color(nodeColor.getRed(), nodeColor.getGreen(), nodeColor.getBlue(), alpha);
	}

}
