package gr.uom.java.seanets.graph;

import java.util.Comparator;

public class NodePackagePAComparator implements Comparator<MyNode>{

	@Override
	public int compare(MyNode n1, MyNode n2) {
		if(n1.getPackagePreferentialAttachment() >= n2.getPackagePreferentialAttachment())
			return 1;
		else 
			return 0;
	}

}
