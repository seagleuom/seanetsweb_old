package gr.uom.java.seanets.graph.visualization;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.transform.shape.GraphicsDecorator;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyNode;

public class MyVertexRenderer implements Renderer.Vertex<MyNode, MyEdge> {
	
	private Color vertexColor;
	private float alpha;  // transparency , diafaneia
	
	public MyVertexRenderer(float alpha){
		vertexColor = new Color(1f, 0, 0 , alpha);
		this.alpha = alpha;
	}
	
	@Override 
	public void paintVertex(RenderContext<MyNode, MyEdge> rc, Layout<MyNode, MyEdge> layout, MyNode node) {
		GraphicsDecorator graphicsContext = rc.getGraphicsContext();
		Point2D center = layout.transform(node);
		double width = 10000 * node.getPageRank();
		double height  = 10000 * node.getPageRank();
		if(node.getPageRank() < 0.002){
			width = 20000 * node.getPageRank();
			height  = 20000 * node.getPageRank();
		}
		else{
			vertexColor = new Color(1f, 0f, 0f , 0.65f);
		}
		Ellipse2D circle = new Ellipse2D.Double(center.getX()-15, center.getY()-15, width, height);
		
		graphicsContext.setPaint(vertexColor);
		graphicsContext.fill(circle);
	}
}