package gr.uom.java.seanets.graph.visualization;

import gr.uom.java.seanets.graph.MyNode;
import org.apache.commons.collections15.Transformer;

public class MyVertexLabelTransformer implements Transformer<MyNode, String>{

	@Override
	public String transform(MyNode node) {
		return node.getSimpleName();
	}


}
