package gr.uom.java.seanets.graph;

import java.io.Serializable;

public class MyPackage implements Comparable<MyPackage>, Serializable{
	
	/**
	 *  First version of the class:
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	
	/*
	 * Wikipedia: http://en.wikipedia.org/wiki/Software_package_metrics
	 * Afferent Couplings (Ca): The number of other packages that depend upon classes within the package is an indicator of the package's responsibility.
	 */
	private double afferentCoupling;
	
	/*
	 * Wikipedia: http://en.wikipedia.org/wiki/Software_package_metrics
	 * Efferent Couplings (Ce): The number of other packages that the classes in the package depend upon is an indicator of the package's independence.
	 */
	private double efferentCoupling;
	
	public MyPackage(String n) {
		name = n;
		afferentCoupling = 0.0;
		efferentCoupling = 0.0;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getAfferentCoupling() {
		return afferentCoupling;
	}

	public void setAfferentCoupling(double afferentCoupling) {
		this.afferentCoupling = afferentCoupling;
	}

	public double getEfferentCoupling() {
		return efferentCoupling;
	}

	public void setEfferentCoupling(double efferentCoupling) {
		this.efferentCoupling = efferentCoupling;
	}

	public boolean equals(Object p2){
		if(p2 instanceof MyPackage){
			MyPackage mp2 = (MyPackage)p2;
			return this.name.equals(mp2.name);
		}
		else if(p2 instanceof String){
			return this.name.equals(p2);
		}
		else{
			return false;
		}
	}

	public int hashCode(){
		return name.hashCode();
	}
	
	public String toString(){
		return "Package "+getName();
	}

	@Override
	public int compareTo(MyPackage p2) {
		return this.name.compareTo(p2.getName());
	}
}
