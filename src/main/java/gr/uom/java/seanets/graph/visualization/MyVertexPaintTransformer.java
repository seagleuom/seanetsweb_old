package gr.uom.java.seanets.graph.visualization;

import java.awt.Color;
import java.awt.Paint;

import gr.uom.java.seanets.graph.MyNode;

import org.apache.commons.collections15.Transformer;

public class MyVertexPaintTransformer implements Transformer<MyNode, Paint>{

	@Override
	public Paint transform(MyNode node) {
		if(node.getAge() == 0){
			return Color.GREEN;
		}
		else{
			return Color.RED;
		}
	}

}
