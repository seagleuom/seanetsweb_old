package gr.uom.java.seanets.graph.visualization;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;

import gr.uom.java.seanets.graph.MyNode;

import org.apache.commons.collections15.Transformer;

public class MyVertexSizeTransformer implements Transformer<MyNode, Shape>{
	
	public Shape transform(MyNode node){
		double width = 10000 * node.getPageRank();
		double height  = 10000 * node.getPageRank();
		if(node.getPageRank() < 0.002){
			width = 20000 * node.getPageRank();
			height  = 20000 * node.getPageRank();
		}
		Ellipse2D circle = new Ellipse2D.Double(-(width/2.0), -(height/2.0), width, height);
	
		//	return AffineTransform.getScaleInstance(1, 1).createTransformedShape(circle);

		return circle;
	}

}
