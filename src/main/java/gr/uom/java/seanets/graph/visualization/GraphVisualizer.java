package gr.uom.java.seanets.graph.visualization;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Queue;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.collections15.functors.ConstantTransformer;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;

import edu.uci.ics.jung.algorithms.layout.*;
import edu.uci.ics.jung.algorithms.layout.util.Relaxer;
import edu.uci.ics.jung.algorithms.layout.util.VisRunner;
import edu.uci.ics.jung.algorithms.util.IterativeContext;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.ObservableGraph;
import edu.uci.ics.jung.graph.event.GraphEvent;
import edu.uci.ics.jung.graph.event.GraphEventListener;
import edu.uci.ics.jung.graph.util.Graphs;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.layout.LayoutTransition;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;
import edu.uci.ics.jung.visualization.util.Animator;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.event.GraphEventLogger;
import gr.uom.java.seanets.graph.event.MyGraphEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GraphVisualizer {

    private Graph<MyNode, MyEdge> graph;
    private JFrame frame;
    private JPanel commandPanel;
    private JButton saveImageButton;
    private JCheckBox showLabelsCheck;
    private VisualizationViewer<MyNode, MyEdge> visualizationViewer;
    private FRLayout<MyNode, MyEdge> layout;
    private DefaultModalGraphMouse<MyNode, MyEdge> graphMouse;
    private ArrayList<Double> verticesPageRanks;

    private int eventCounter;

    private Dimension screenSize;
    private Dimension graphSize;
    private transient final static Logger logger = Logger.getLogger(GraphVisualizer.class.getName());
    //	public static final int EDGE_LENGTH = 100;

    public GraphVisualizer(MyGraph graph) {
        this.frame = new JFrame(graph + " ");
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        graphSize = new Dimension(screenSize.width - 50, screenSize.height - 50);
        graphMouse = new DefaultModalGraphMouse<MyNode, MyEdge>();

        eventCounter = 0;

        commandPanelInitialization();
        observableInitialization(graph);
        layOutInitialization();
        visualizationViewerInitialization();

        frame.getContentPane().setFont(new Font("Serif", Font.PLAIN, 12));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(screenSize);
        frame.setVisible(true);

    }

    private void commandPanelInitialization() {
        commandPanel = new JPanel();
        showLabelsCheck = new JCheckBox("Display Node Labels");
        showLabelsCheck.addActionListener(new CheckboxListener());
        showLabelsCheck.setSelected(false);
        saveImageButton = new JButton("Save Graph as JPEG");
        saveImageButton.addActionListener(new SaveImageButtonListener());
        commandPanel.add(showLabelsCheck);
        commandPanel.add(saveImageButton);
        frame.add(commandPanel, BorderLayout.SOUTH);
    }

    private void layOutInitialization() {
        layout = new FRLayout<MyNode, MyEdge>(graph);
        layout.setRepulsionMultiplier(2.75);
        layout.setAttractionMultiplier(0.75);
        layout.setMaxIterations(2000);
        layout.setSize(screenSize); // sets the initial size of the space
        relaxerHandler();
    }

    private void observableInitialization(MyGraph graph) {
        Graph<MyNode, MyEdge> aGraph = Graphs.<MyNode, MyEdge>synchronizedDirectedGraph(graph);
        ObservableGraph<MyNode, MyEdge> observableGraph = new ObservableGraph<MyNode, MyEdge>(aGraph);
        observableGraph.addGraphEventListener(new GraphEventListener<MyNode, MyEdge>() {
            public void handleGraphEvent(GraphEvent<MyNode, MyEdge> evt) {
                //	System.err.println("got "+evt);
            }
        });
        this.graph = observableGraph;
    }

    private void visualizationViewerInitialization() {
        StaticLayout<MyNode, MyEdge> staticLayout = new StaticLayout<MyNode, MyEdge>(graph, layout);
        visualizationViewer = new VisualizationViewer<MyNode, MyEdge>(staticLayout, graphSize);
        visualizationViewer.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);
        visualizationViewer.setGraphMouse(graphMouse);
        graphMouse.setMode(Mode.PICKING);

        visualizationViewer.getRenderContext().setVertexFillPaintTransformer(new MyVertexPaintTransformer());
        visualizationViewer.getRenderContext().setVertexShapeTransformer(new MyVertexSizeTransformer());
        visualizationViewer.getRenderContext().setVertexFillPaintTransformer(new MyVertexColorTransformer());
        visualizationViewer.setVertexToolTipTransformer(new ToStringLabeller<MyNode>());

        visualizationViewer.getRenderContext().setEdgeDrawPaintTransformer(new MyEdgeColorTransformer());
        visualizationViewer.getRenderContext().setEdgeShapeTransformer(new EdgeShape.QuadCurve<MyNode, MyEdge>());
        visualizationViewer.setForeground(Color.white);

        addComponentListenerToViewer();
        frame.add(visualizationViewer, BorderLayout.CENTER);
    }

    private void relaxerHandler() {
        Relaxer relaxer = new VisRunner((IterativeContext) layout);
        relaxer.stop();
        relaxer.prerelax();
    }

    private void addComponentListenerToViewer() {
        visualizationViewer.addComponentListener(new ComponentAdapter() {
            /**
             * @see
             * java.awt.event.ComponentAdapter#componentResized(java.awt.event.ComponentEvent)
             */
            @Override
            public void componentResized(ComponentEvent arg0) {
                super.componentResized(arg0);
                System.err.println("resized");
                layout.setSize(arg0.getComponent().getSize());
            }
        });
    }

    public void updateGraphViewer() {

        try {

            //vv.getRenderContext().getMultiLayerTransformer().setToIdentity();
            visualizationViewer.repaint();
        } catch (Exception e) {
            logger.log(Level.SEVERE, null,e);
        }
    }

    private void updateLayout() {
        layout.initialize();
        relaxerHandler();
        StaticLayout<MyNode, MyEdge> staticLayout = new StaticLayout<MyNode, MyEdge>(graph, layout);
        LayoutTransition<MyNode, MyEdge> layoutTransition
                = new LayoutTransition<MyNode, MyEdge>(visualizationViewer,
                        visualizationViewer.getGraphLayout(),
                        staticLayout);
        Animator animator = new Animator(layoutTransition);
        animator.start();
        visualizationViewer.repaint();
    }

    public void visualizeModifications(IProgressMonitor monitor) throws InterruptedException {
        Queue<MyGraphEvent> graphEvents = GraphEventLogger.getEvents();
        while (!graphEvents.isEmpty()) {
            if (monitor != null && monitor.isCanceled()) {
                throw new OperationCanceledException();
            }
            if (monitor != null) {
                monitor.subTask("Creating graph visualizations");
            }
            visualizationViewer.getRenderContext().getPickedEdgeState().clear();
            graphEvents.poll().reproduce(graph, visualizationViewer);
            updateEventCounter();

            Thread.sleep(250);
            if (monitor != null) {
                monitor.worked(1);
            }
        }
        visualizationViewer.getRenderContext().getPickedVertexState().clear();
        visualizationViewer.getRenderContext().getPickedEdgeState().clear();
    }

    private void updateEventCounter() {
        eventCounter++;
        visualizationViewer.repaint();

        if ((eventCounter % 10) == 0) {
            updateLayout();
        }
    }

    public void setVisible() {
        frame.pack();
        frame.setVisible(true);
    }

    public void writeJPEGImage(File file) {
        int width = visualizationViewer.getWidth();
        int height = visualizationViewer.getHeight();

        BufferedImage bi = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = bi.createGraphics();
        visualizationViewer.paint(graphics);
        graphics.dispose();
        try {
            ImageIO.write(bi, "jpeg", file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CheckboxListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == showLabelsCheck) {
                if (showLabelsCheck.isSelected()) {
                    visualizationViewer.getRenderContext().setVertexLabelTransformer(new MyVertexLabelTransformer());
                } else {
                    visualizationViewer.getRenderContext().setVertexLabelTransformer(new ConstantTransformer(null));
                }
            }

            visualizationViewer.repaint();
        }

    }

    private class SaveImageButtonListener implements ActionListener {

        private JFileChooser fileChooser;

        public SaveImageButtonListener() {
            fileChooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter(
                    "JPG Images only", "jpg", "jpeg", "JPEG", "JPG");
            fileChooser.setFileFilter(filter);
        }

        @Override
        public void actionPerformed(ActionEvent arg0) {
            fileChooser.setDialogTitle("Specify file name");
            int userSelection = fileChooser.showSaveDialog(frame);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = new File(fileChooser.getSelectedFile() + ".jpeg");

                writeJPEGImage(fileToSave);
            }
        }

    }

}
