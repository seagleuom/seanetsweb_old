package gr.uom.java.seanets.graph;
import gr.uom.java.seanets.statistics.MyLineChart;
import gr.uom.java.seanets.statistics.MyScatterPlotChart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Utilities {

	public class NumberComparator implements Comparator<Number>{
		@Override
		public int compare(Number n1, Number n2) {
			if(n1.doubleValue() > n2.doubleValue())
				return 1;
			else if(n1.doubleValue() < n2.doubleValue())
				return -1;
			else
				return 0;
		}
	}

	public static double [][] transposeMatrix(double [][] m){
		int r = m.length;
		int c = m[r-1].length;
		double [][] t = new double[c][r];
		for(int i = 0; i < r; ++i){
			for(int j = 0; j < c; ++j){
				t[j][i] = m[i][j];
			}
		}
		return t;
	}

	public static double getMedian(List<Double> list){
		double median = 0;
		int dataSize = list.size();
		if((dataSize % 2 ) == 0){
			int index1 = dataSize/2;
			int index2 = index1 - 1;
			median = ( list.get(index2) + list.get(index1) ) / 2.0;
		}
		else{
			int index = (int)Math.ceil(dataSize / 2.0) -1 ;
			median = list.get(index);
		}
		return median;
	}

	public static double getUpperQuartile(List<Double> list){
		double upperQuartile = 0;
		int dataSize = list.size();
		if((dataSize % 2 ) == 0){
			int index = (int) Math.ceil(dataSize * 0.75);
			upperQuartile = list.get(index - 1);
		}
		else{
			int index1 = (int)Math.ceil(dataSize * 0.75);
			int index2 = index1 - 1;
			upperQuartile = ( list.get(index2) + list.get(index1) ) / 2.0;
		}
		return upperQuartile;
	}

	public static double getLowerQuartile(List<Double> list){
		double upperQuartile = 0;
		int dataSize = list.size();
		if((dataSize % 2 ) == 0){
			int index = (int) Math.ceil(dataSize * 0.25);
			upperQuartile = list.get(index - 1);
		}
		else{
			int index1 = (int)Math.ceil(dataSize * 0.25);
			int index2 = index1 - 1;
			upperQuartile = ( list.get(index2) + list.get(index1) ) / 2.0;
		}
		return upperQuartile;
	}

	public static List<Double> cutOffUpperExtremeValues(List<Double> orderedList){
		List<Double> listWithNoOutliers = new ArrayList<Double>();
		double extremeValueLimit = getUpperExtremeValueLimit(orderedList);
		for(int i=0; i< orderedList.size(); i++) {
			double number = orderedList.get(i);
			if(number <= extremeValueLimit){
				listWithNoOutliers.add(number);
			}
		}
		return listWithNoOutliers;
	}

	private static double getUpperExtremeValueLimit(List<Double> orderedList) {
		double upperQuartile = getUpperQuartile(orderedList);
		double lowerQuartile = getLowerQuartile(orderedList);
		double interQuartileRange = upperQuartile - lowerQuartile;
		double extremeValueLimit = upperQuartile + interQuartileRange * 3.0;
		return extremeValueLimit;
	}

	public static Map<Integer, Double> cutOffUpperExtremeValues(Map<Integer, Double> map){
		Map<Integer, Double> mapWithNoUpperOutlierValues = new TreeMap<Integer, Double>();
		List<Double> listWithOutliers = new ArrayList<Double>();
		listWithOutliers.addAll(map.values());
		Collections.sort(listWithOutliers);
		double extremeValueLimit = getUpperExtremeValueLimit(listWithOutliers);

		for(Integer key : map.keySet()) {
			double value = map.get(key);
			if(value <= extremeValueLimit){
				mapWithNoUpperOutlierValues.put(key, value);
			}
		}
		return mapWithNoUpperOutlierValues;
	}

	public static void createChartFromMap(	int size, Map<Integer,ArrayList<Integer>> dataMap,
			String title,String axisX, String axisY) {
		if(!dataMap.isEmpty()) {
			int i;
			double[][] data = new double[2][dataMap.keySet().size()];
			i=0;
			//	System.out.println(axisX+"\tNew Edges");
			for(Integer aNum : dataMap.keySet()){
				ArrayList<Integer> list = dataMap.get(aNum);
				data[0][i] = aNum;
				double sum = 0;
				for(Integer num : list){
					sum += num;
				}
				data[1][i] = sum;
				//		System.out.println(aNum+"\t"+sum);
				i++;
			}
			MyScatterPlotChart chart2 = new MyScatterPlotChart(title,axisX,axisY,true);
			chart2.addData("data 1", data);
			chart2.prepareChart();
		}
	}

	public static void createChartFromMap2(Map<Integer,Double> dataMap,
			String title,String axisX, String axisY) {
		if(!dataMap.isEmpty()) {
			int i;
			double[][] data = new double[2][dataMap.keySet().size()];
			i=0;

			for(Integer aNumber : dataMap.keySet()){
				data[0][i] = aNumber * 1.0;
				data[1][i] = dataMap.get(aNumber);
				i++;
				//	System.out.println(aNumber+"\t"+dataMap.get(aNumber));
			}
			MyScatterPlotChart chart2 = new MyScatterPlotChart(title,axisX,axisY,true);
			chart2.addData("data 1",data);
			chart2.prepareChart();
		}
	}

	public static void createChartFromMap3(Map<Integer,Integer> dataMap, String title, String dataName,
			String axisX, String axisY) {
		if(!dataMap.isEmpty()) {
			int i;
			double[][] data = new double[2][dataMap.keySet().size()];
			i=0;
			for(Integer aNumber : dataMap.keySet()){
				data[0][i] = aNumber * 1.0;
				data[1][i] = dataMap.get(aNumber)*1.0;
				i++;
				System.out.println(aNumber+"\t"+dataMap.get(aNumber));
			}
			MyScatterPlotChart chart2 = new MyScatterPlotChart(title,axisX,axisY,false);
			chart2.addData(dataName,data);
		}
	}

	public static void createChartFromMap4(ArrayList<Map.Entry<String,Integer>> dataMap, String title, String dataName,
			String axisX, String axisY) {
		if(!dataMap.isEmpty()) {
			MyLineChart chart2 = new MyLineChart(title,axisX,axisY);
			for(Map.Entry<String, Integer> entry : dataMap){
				String aLabel = entry.getKey();
				int value = entry.getValue();
				chart2.addValue(value, dataName , aLabel);
			}
			chart2.displayChart();
		}
	}

    public static double[][] convertIntVSDoubleMapToDataset(Map<Integer, Double> intVSDoubleMap, boolean trimExtremeValues) {
        Map<Integer, Double> intVSDoublePositiveValuesOnlyMap = new TreeMap<Integer, Double>();
        int i = 0;
        boolean dataEmpty = true;
        for (Integer key : intVSDoubleMap.keySet()) {
            double value = intVSDoubleMap.get(key);
            if (key > 0 && value > 0) {
                intVSDoublePositiveValuesOnlyMap.put(key, value);
            }
        }
        double[][] data;
        Map<Integer, Double> map;
        if (trimExtremeValues) {
            map = Utilities.cutOffUpperExtremeValues(intVSDoublePositiveValuesOnlyMap);
        } else {
            map = intVSDoublePositiveValuesOnlyMap;
        }
        int dataSize = map.keySet().size();
        data = new double[2][dataSize];
        for (Integer key : map.keySet()) {
            double value = map.get(key);
            data[0][i] = key;
            data[1][i] = value;
            i++;
            dataEmpty = false;
        }
        if (!dataEmpty) {
            return data;
        } else {
            return null;
        }
    }
/*
	public static void createReportGraphs(Map<Version, MyGraph> projectGraphEvolution) {

					Map<Integer, Integer> packageSizeToIncomingEdges = new LinkedHashMap<Integer, Integer>();
			Map<Integer, Integer> packageSizeToOutgoingEdges = new LinkedHashMap<Integer, Integer>();
			Map<Integer, Integer> packageSizeToIntraEdges = new LinkedHashMap<Integer, Integer>();

			Map<String, Integer> newIncomingEdgesPerPackage = new LinkedHashMap<String, Integer>();

			for(ProjectVersion currentVersion : projectGraphEvolution.keySet()){
				MyGraph myGraph = projectGraphEvolution.get(currentVersion);
				for(MyPackage myPack : myGraph.getPackageMap().keySet()) {
					if(newIncomingEdgesPerPackage.containsKey(myPack.getName())){
						int prevEdges = newIncomingEdgesPerPackage.get(myPack.getName());
						int currentEdges = myPack.getInterPackageIncomingEdgesCounter();
						int newEdges = currentEdges - prevEdges;
						newIncomingEdgesPerPackage.put(myPack.getName(), prevEdges + newEdges);

						int packSize = myGraph.getPackageMap().get(myPack).size();
						updateIntegerIntegerMap(packageSizeToIncomingEdges,	newEdges, packSize);
					}
					else{
						newIncomingEdgesPerPackage.put(myPack.getName(), myPack.getInterPackageIncomingEdgesCounter());

						int packSize = myGraph.getPackageMap().get(myPack).size();
						updateIntegerIntegerMap(packageSizeToIncomingEdges,	myPack.getInterPackageIncomingEdgesCounter(), packSize);
					}
				}
			}
			System.out.println("Package Name\tClasses\tNew InterPackage Incoming Edges");
			for(String pack : newIncomingEdgesPerPackage.keySet()) {
				System.out.println(pack+"\t"+newIncomingEdgesPerPackage.get(pack));
			}

			Map<String, Integer> newOutgoingEdgesPerPackage = new LinkedHashMap<String, Integer>();
			for(ProjectVersion currentVersion : projectGraphEvolution.keySet()){
				MyGraph myGraph = projectGraphEvolution.get(currentVersion);
				for(MyPackage myPack : myGraph.getPackageMap().keySet()) {
					if(newOutgoingEdgesPerPackage.containsKey(myPack.getName())){
						int prevEdges = newOutgoingEdgesPerPackage.get(myPack);
						int currentEdges = myPack.getInterPackageOutgoingEdgesCounter();
						int newEdges = currentEdges - prevEdges;
						newOutgoingEdgesPerPackage.put(myPack.getName(), prevEdges + (currentEdges - prevEdges));

						int packSize = myGraph.getPackageMap().get(myPack).size();
						updateIntegerIntegerMap(packageSizeToOutgoingEdges,	newEdges, packSize);
					}
					else{
						newOutgoingEdgesPerPackage.put(myPack.getName(), myPack.getInterPackageOutgoingEdgesCounter());

						int packSize = myGraph.getPackageMap().get(myPack).size();
						updateIntegerIntegerMap(packageSizeToOutgoingEdges,	myPack.getInterPackageOutgoingEdgesCounter(), packSize);
					}
				}
			}

			Map<String, Integer> newIntraEdgesPerPackage = new LinkedHashMap<String, Integer>();
			for(ProjectVersion currentVersion : projectGraphEvolution.keySet()){
				MyGraph myGraph = projectGraphEvolution.get(currentVersion);
				for(MyPackage myPack : myGraph.getPackageMap().keySet()) {
					if(newIntraEdgesPerPackage.containsKey(myPack.getName())){
						int prevEdges = newIntraEdgesPerPackage.get(myPack);
						int currentEdges = myPack.getIntraPackageEdgesCounter();
						int newEdges = currentEdges - prevEdges;
						newIntraEdgesPerPackage.put(myPack.getName(), prevEdges + (currentEdges - prevEdges));

						int packSize = myGraph.getPackageMap().get(myPack).size();
						updateIntegerIntegerMap(packageSizeToIntraEdges,	newEdges, packSize);
					}
					else{
						newIntraEdgesPerPackage.put(myPack.getName(), myPack.getIntraPackageEdgesCounter());

						int packSize = myGraph.getPackageMap().get(myPack).size();
						updateIntegerIntegerMap(packageSizeToIntraEdges,	myPack.getIntraPackageEdgesCounter(), packSize);
					}
				}
			}

			System.out.println("Package Name\tNew InterPackage Incoming Edges");
			for(String pack : newIncomingEdgesPerPackage.keySet()) {
				System.out.println(pack+"\t"+newIncomingEdgesPerPackage.get(pack));
			}
			System.out.println("Package Name\tNew InterPackage Outgoing Edges");
			for(String pack : newOutgoingEdgesPerPackage.keySet()) {
				System.out.println(pack+"\t"+newOutgoingEdgesPerPackage.get(pack));
			}
			System.out.println("Package Name\tNew Intra Package Edges");
			for(String pack : newIntraEdgesPerPackage.keySet()) {
				System.out.println(pack+"\t"+newIntraEdgesPerPackage.get(pack));
			}

			System.out.println("Package Size in #Classes\tNew InterPackage Incoming Edges");
			for(int packSize : packageSizeToIncomingEdges.keySet()) {
				System.out.println(packSize+"\t"+packageSizeToIncomingEdges.get(packSize));
			}
			System.out.println("Package Size in #Classes\tNew InterPackage Outgoing Edges");
			for(int packSize : packageSizeToOutgoingEdges.keySet()) {
				System.out.println(packSize+"\t"+packageSizeToOutgoingEdges.get(packSize));
			}
			System.out.println("Package Size in #Classes\tNew IntraPackage Edges");
			for(int packSize : packageSizeToIntraEdges.keySet()) {
				System.out.println(packSize+"\t"+packageSizeToIntraEdges.get(packSize));
			}

	for(ProjectVersion currentVersion : projectGraphEvolution.keySet()){
				MyGraph myGraph = projectGraphEvolution.get(currentVersion);
				for(MyPackage myPack : myGraph.getPackageMap().keySet()) {
					if(myPack.getName().equalsIgnoreCase(name)){
						System.out.print(currentVersion+"\t");
						myPack.printPercentages();
					}
				}
			}
			createChartFromMap3(ageToNewOutgoingEdgeCountMap,
					ProjectEvolution.projectName,
					"New outgoing Edges Count",
					" Node Age","Count of new out Edges");
			createChartFromMap3(ageToNewIncomingEdgeCountMap,
					ProjectEvolution.projectName,
					"New incoming Edge Count",
					" Node Age","Count of new in Edges");


			//PRINT GRAPHS FOR THE LAST VERSION:

			graph.printDegreeVSCcountMap();
				System.out.println();

			EvolutionSimulation.createCumulativeDistributionFromIntegerIntegerMap(existingNodeOutDegreeToNewOutgoingEdgesMap, 
					"Existing node out degree to new Edges count","Out Degree","Cumul. Freq");

			EvolutionSimulation.createCumulativeDistributionFromIntegerIntegerMap(getEdgesFromNewToExistingFrequencyMap(),
					"Number of new edges from NEW to EXISTING nodes","New Edge Number","Cumul. Freq");

			EvolutionSimulation.createCumulativeDistributionFromIntegerIntegerMap(getExistingNodeNewEdgeNumberToFrequencyMap(),
					"Number of new edges from EXISTING nodes", "New Edge Number", "Cumul. Freq");

			EvolutionSimulation.createCumulativeDistributionFromIntegerIntegerMap(getEdgesFromNewToNewFrequencyMap(),
								"Number of new edges from NEW to NEW nodes","New Edge Number","Cumul. Freq");

			EvolutionSimulation.createCumulativeDistributionFromIntegerIntegerMap(ageToNewIncomingEdgeCountMap,
								"Existing node age to new incoming edges", "Node Age", "Cumul. Percentage of incoming edges (target nodes)");

			graph.printHopsFrequenciesMap();
			firstGraph.printCumulativeHopsPercentageMap();

			graph.printCumulativeHopsPercentageMap();
			graph.printDegreeVSClusteringCoefficientMap();

			System.out.println("Existing node age to new outgoing Edges count\n Age\tNew outgoing Edges\n");
			for(Integer age : ageToNewOutgoingEdgeCountMap.keySet()) {
				System.out.println(age+"\t"+ageToNewOutgoingEdgeCountMap.get(age));
			}

	}
*/

	public static void updateIntegerIntegerMap(Map<Integer, Integer> map, int key, int value) {
		if(map.containsKey(key)){
			int previousCounter = map.get(key);
			map.put(key, previousCounter + value);
		}
		else{
			map.put(key, value);
		}
	}

	public static void printIntegerIntegerMap(Map<Integer, Integer> aMap, String key, String value){
		System.out.println(key+"\t"+value);
		for(Integer k : aMap.keySet()) {
			System.out.println(k+"\t"+aMap.get(k));
		}
	}

	public static void printIntegerDoubleMap(Map<Integer, Double> aMap, String key, String value){
		System.out.println(key+"\t"+value);
		for(Integer k : aMap.keySet()) {
			System.out.println(k+"\t"+aMap.get(k));
		}
	}

}
