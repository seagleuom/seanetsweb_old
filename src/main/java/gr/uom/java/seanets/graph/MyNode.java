package gr.uom.java.seanets.graph;

import java.io.Serializable;
import java.util.ArrayList;

public class MyNode implements Comparable<MyNode>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String name;
    private MyPackage _package;
    private double clusteringCoefficient;
    private double betweenessCentrality;
    private double globalPreferentialAttachment;
    private double packagePreferentialAttachment;
    private int age;
    private boolean _abstract;
    private boolean _interface;
    private MyNode parent;
    private double pageRank;
    private double eigenvectorCentrality;

    public MyNode(String name, MyPackage _pack, MyGraph graph) {
        super();
        this.name = name;
        this._package = _pack;
        this.age = 0;
        this.clusteringCoefficient = 0;
        this.betweenessCentrality = 0;
        this.eigenvectorCentrality = 0.0;
        this.globalPreferentialAttachment = 0;
        this.packagePreferentialAttachment = 0;
        this._abstract = false;
        this._interface = false;
        this.parent = null;
        this.pageRank = 0.0;
    }

    public MyNode(String name) {
        super();
        this.name = name;
        this.age = 0;
        this.clusteringCoefficient = 0;
        this.betweenessCentrality = 0;
        this.eigenvectorCentrality = 0.0;
        this.globalPreferentialAttachment = 0;
        this.packagePreferentialAttachment = 0;
        this._abstract = false;
        this._interface = false;
        this.parent = null;
        this.pageRank = 0.0;
    }

    public MyNode clone(MyGraph graph) {
        MyNode newNode = new MyNode(new String(name), new MyPackage(_package.getName()), graph);
        newNode.age = new Integer(age);
        if (parent != null) {
            newNode.parent = new MyNode(new String(parent.getName()), new MyPackage(parent.getPackage().getName()), graph);
        }
        return newNode;
    }

    public double getGlobalPreferentialAttachment() {
        return globalPreferentialAttachment;
    }

    public void setGlobalPreferentialAttachment(double globalPreferentialAttachment) {
        this.globalPreferentialAttachment = globalPreferentialAttachment;
    }

    public double getPackagePreferentialAttachment() {
        return packagePreferentialAttachment;
    }

    public void setPackagePreferentialAttachment(
            double packagePreferentialAttachment) {
        this.packagePreferentialAttachment = packagePreferentialAttachment;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void increaseAgeByOneVersion() {
        age++;
    }

    public double getClusteringCoefficient() {
        return clusteringCoefficient;
    }

    public void setClusteringCoefficient(double clusteringCoefficient) {
        this.clusteringCoefficient = clusteringCoefficient;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getSimpleName() {
        return name.substring(name.lastIndexOf(".") + 1, name.length());
    }

    public double getBetweenessCentrality() {
        return betweenessCentrality;
    }

    public void setBetweennessCentrality(double betweenessCentrality) {
        this.betweenessCentrality = betweenessCentrality;
    }

    public boolean isAbstractOrInterface() {
        return _abstract || this._interface;
    }

    public boolean isAbstract() {
        return _abstract;
    }

    public void setAbstract(boolean _abstract) {
        this._abstract = _abstract;
    }

    public boolean isInterface() {
        return _interface;
    }

    public void setInterface(boolean _interface) {
        this._interface = _interface;
    }

    public MyNode getParent() {
        return parent;
    }

    public void setParent(MyNode parent) {
        this.parent = parent;
    }

    public void getAnscestors(ArrayList<MyNode> anscestors) {
        if (parent != null) {
            anscestors.add(parent);
            parent.getAnscestors(anscestors);
        }
    }

    public boolean isSubclass() {
        if (parent != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(MyNode o) {
        return this.name.compareTo(o.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MyNode) {
            MyNode node = (MyNode) o;
            return this.getName().equals(node.getName());
        } else if (o instanceof Integer) {
            int num = (Integer) o;
            return this.getName().equals("" + num);
        } else if (o instanceof String) {
            return this.getName().equals(o);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = name.hashCode();
        hash = (hash % 17) + name.hashCode();
        return hash;
    }

    public MyPackage getPackage() {
        return _package;
    }

    public void setPackage(MyPackage _package) {
        this._package = _package;
    }

    public double getPageRank() {
        return pageRank;
    }

    public void setPageRank(double pageRank) {
        this.pageRank = pageRank;
    }

    public double getEigenvectorCentrality() {
        return eigenvectorCentrality;
    }

    public void setEigenvectorCentrality(double vertexScore) {
        this.eigenvectorCentrality = vertexScore;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
