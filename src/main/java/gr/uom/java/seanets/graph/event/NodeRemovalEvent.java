package gr.uom.java.seanets.graph.event;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;


public class NodeRemovalEvent extends MyGraphEvent{

	private MyNode nodeRemoved;
	
	public NodeRemovalEvent(MyNode node){
		nodeRemoved = node;
	}
	
	@Override
	public void reproduce(Graph<MyNode, MyEdge> graph, VisualizationViewer<MyNode, MyEdge> visualizationViewer) {
		graph.removeVertex(nodeRemoved);
	}
	

}
