package gr.uom.java.seanets.graph;

import edu.uci.ics.jung.algorithms.metrics.Metrics;
import edu.uci.ics.jung.algorithms.scoring.BetweennessCentrality;
import edu.uci.ics.jung.algorithms.scoring.EigenvectorCentrality;
import edu.uci.ics.jung.algorithms.scoring.PageRank;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.algorithms.shortestpath.Distance;
import edu.uci.ics.jung.algorithms.shortestpath.DistanceStatistics;
import edu.uci.ics.jung.algorithms.shortestpath.UnweightedShortestPath;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.seanets.graph.event.EdgeRemovalEvent;
import gr.uom.java.seanets.graph.event.GraphEventLogger;
import gr.uom.java.seanets.gui.SelectedProjectName;
import gr.uom.java.seanets.history.PowerFittingFunction;
import gr.uom.java.seanets.db.persistence.Version;
import gr.uom.java.seanets.simulation.MyRandomGenerator;
import gr.uom.java.seanets.statistics.FittingFunction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;

public class MyGraph extends DirectedSparseGraph<MyNode, MyEdge> implements Serializable {

    private static final long serialVersionUID = 6585948028229436597L;
    private Map<Integer, List<List<MyEdge>>> pathLengthToPathMap;
    private Map<Integer, Integer> hopsFrequencies = new TreeMap<Integer, Integer>();
    private Map<Integer, Double> pathLengthCumulativeFrequencies = new TreeMap<Integer, Double>();
    private Map<Integer, Double> degreeToClusteringCoefMap = new TreeMap<Integer, Double>();
    private Map<Integer, Double> inDegreeToClusteringCoefMap = new TreeMap<Integer, Double>();
    private Map<Integer, Double> outDegreeToClusteringCoefMap = new TreeMap<Integer, Double>();
    private Map<Integer, Integer> inDegreeToCountMap = new TreeMap<Integer, Integer>();
    private Map<Integer, Integer> outDegreeToCountMap = new TreeMap<Integer, Integer>();
    private ArrayList<Double> globalPreferentialAttachmentOrderedSet = new ArrayList<Double>();
    private Map<Integer, Integer> existingNodeInDegreeToNewEdgesAttractedFrequencyMap = new TreeMap<Integer, Integer>();
    private Double[] cumulativePreferentialAttachment;
    private double diameter;
    private Version projectVersion;
    private double totalInDegree;
    private double clusteringCoefficientGlobal;
    private String projectName;

    private transient Distance<MyNode> distanceMeasure;
    private transient final static Logger logger = Logger.getLogger(MyGraph.class.getName());

    public MyGraph(Version pv) {
        diameter = 0;
        clusteringCoefficientGlobal = 0;
        projectVersion = pv;
    }

    public MyGraph() {
        diameter = 0;
        clusteringCoefficientGlobal = 0;
        projectVersion = new Version(Calendar.getInstance().getTime());
    }

    public MyGraph(File graphEdges) {
        FileInputStream fileIn;
        try {
            fileIn = new FileInputStream(graphEdges);
            Scanner scanner = new Scanner(fileIn);
            while (scanner.hasNext()) {
                String edge = scanner.nextLine();
                StringTokenizer st = new StringTokenizer(edge);
                MyNode sourceNode = new MyNode(st.nextToken());
                MyNode targetNode = new MyNode(st.nextToken());
                this.addVertex(sourceNode);
                this.addVertex(targetNode);
                this.addEdge(new MyEdge(sourceNode, targetNode, ""), sourceNode, targetNode, EdgeType.DIRECTED);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        diameter = 0;
        clusteringCoefficientGlobal = 0;
        projectVersion = new Version();

        calculateDiameterAndShortestPaths();
        calculateClusteringCoefficient();
        calculateGlobalPreferentialAttachment();
        calculateDegreeDistribution();
        getInDegreeVSClusteringCoefficientMapString();

    }

    public MyGraph cloneGraph() {
        MyGraph newGraph = new MyGraph();
        Map<MyNode, MyNode> m = new HashMap<MyNode, MyNode>();
        for (MyNode oldNode : this.getVertices()) {
            MyNode newNode = oldNode.clone(newGraph);
            newGraph.addVertex(newNode);
            m.put(oldNode, newNode);
        }
        for (MyEdge oldEdge : this.getEdges()) {
            MyNode newNode1 = m.get(oldEdge.getSourceNode());
            MyNode newNode2 = m.get(oldEdge.getTargetNode());
            try {
                MyEdge newEdge = new MyEdge(newNode1, newNode2, oldEdge.getFriendship());
                newGraph.addEdge(newEdge, newNode1, newNode2, EdgeType.DIRECTED);
                newEdge.setVersionAdded(oldEdge.getVersionAdded());
                newEdge.setDirection(oldEdge.getDirection());
            } catch (NullPointerException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        newGraph.diameter = diameter;
        newGraph.projectVersion = projectVersion;
        return newGraph;
    }

    public void initializeDijskstraDistance() {
        distanceMeasure = new UnweightedShortestPath<MyNode, MyEdge>(this);
    }

    public void calculatePageRank() {
        PageRank<MyNode, MyEdge> pageRank = new PageRank<MyNode, MyEdge>(this, 0.15);
        pageRank.evaluate();

        for (MyNode node : getVertices()) {
            node.setPageRank(pageRank.getVertexScore(node));
        }
    }

    /*
     * Calculation according to wikipedia
     * http://en.wikipedia.org/wiki/Clustering_coefficient
     */
    public final void calculateClusteringCoefficient() {
        Map<MyNode, Double> ccMap = Metrics.clusteringCoefficients(this);
        for (MyNode node : getVertices()) {
            double clusteringCoefficient = ccMap.get(node);
            node.setClusteringCoefficient(clusteringCoefficient);
            this.clusteringCoefficientGlobal += clusteringCoefficient;

            int inDegree = inDegree(node);
            int outDegree = outDegree(node);
            int totalDegree = inDegree + outDegree;

            if (!degreeToClusteringCoefMap.containsKey(totalDegree)) {
                degreeToClusteringCoefMap.put(totalDegree, clusteringCoefficient);
            } else {
                double oldCC = degreeToClusteringCoefMap.get(totalDegree);
                degreeToClusteringCoefMap.put(totalDegree, (oldCC + clusteringCoefficient) / 2.0);
            }

            if (!inDegreeToClusteringCoefMap.containsKey(inDegree)) {
                inDegreeToClusteringCoefMap.put(inDegree, clusteringCoefficient);
            } else {
                double oldCC = inDegreeToClusteringCoefMap.get(inDegree);
                inDegreeToClusteringCoefMap.put(inDegree, (oldCC + clusteringCoefficient) / 2.0);
            }

            if (!outDegreeToClusteringCoefMap.containsKey(outDegree)) {
                outDegreeToClusteringCoefMap.put(outDegree, clusteringCoefficient);
            } else {
                double oldCC = outDegreeToClusteringCoefMap.get(outDegree);
                outDegreeToClusteringCoefMap.put(outDegree, (oldCC + clusteringCoefficient) / 2.0);
            }
            //	System.out.println(node+" CC = "+clusteringCoefficient);
        }
        this.clusteringCoefficientGlobal = this.clusteringCoefficientGlobal / getVertexCount();
        this.calculateAfferentPackageCoupling();
        this.calculateEfferentPackageCoupling();
    }

    public double[][] getTotalDegreeVSClusteringCoefficientDataset(boolean trimExtremeValues) {
        return Utilities.convertIntVSDoubleMapToDataset(this.degreeToClusteringCoefMap, trimExtremeValues);
    }

    public double[][] getInDegreeVSClusteringCoefficientDataset(boolean trimExtremeValues) {
        return Utilities.convertIntVSDoubleMapToDataset(this.inDegreeToClusteringCoefMap, trimExtremeValues);
    }

    public double[][] getOutDegreeVSClusteringCoefficientDataset(boolean trimExtremeValues) {
        return Utilities.convertIntVSDoubleMapToDataset(this.outDegreeToClusteringCoefMap, trimExtremeValues);
    }

    public double[][] getInDegreeVSOutDegreeDataset() {
        double[][] data = new double[2][getVertexCount()];
        int i = 0;
        for (MyNode node : getVertices()) {
            data[0][i] = inDegree(node);
            data[1][i] = outDegree(node);
            i++;
        }
        return data;
    }

    public double[][] getInDegreeVSCountDataset() {
        return convertIntVSIntMapToDataset(this.inDegreeToCountMap);
    }

    public double[][] getOutDegreeVSCountDataset() {
        return convertIntVSIntMapToDataset(this.outDegreeToCountMap);
    }

    public static double[][] convertIntVSIntMapToDataset(Map<Integer, Integer> intIntMap) {
        int dataSize = intIntMap.keySet().size();
        double[][] data = new double[2][dataSize];
        int i = 0;
        boolean dataEmpty = true;
        for (Integer degree : intIntMap.keySet()) {
            double count = intIntMap.get(degree);
            if (degree > 0 && count > 0) {
                data[0][i] = degree;
                data[1][i] = count;
                i++;
                dataEmpty = false;
            }
        }
        if (!dataEmpty) {
            return data;
        } else {
            return null;
        }
    }

    public double getAlpha() {
        double[][] dataset = getInDegreeVSCountDataset();
        if (dataset == null) {
            System.err.println("Exponent is zero due to a null dataset. please check the in Degree vs count dataset");
            return 0.0;
        }
        double[][] data = Utilities.transposeMatrix(dataset);
        PowerFittingFunction function = new PowerFittingFunction(org.jfree.data.statistics.Regression.getPowerRegression(data));
        return function.getB();
    }

    public double[][] getHopsFrequenciesDataset() {
        int dataSize = this.hopsFrequencies.keySet().size();
        double[][] data = new double[2][dataSize];
        int i = 0;
        boolean dataEmpty = true;
        for (Integer pathLength : this.hopsFrequencies.keySet()) {
            double freq = hopsFrequencies.get(pathLength);
            if (pathLength > 0 && freq > 0) {
                data[0][i] = pathLength;
                data[1][i] = freq;
                i++;
                dataEmpty = false;
            }
        }
        if (!dataEmpty) {
            return (data);
        } else {
            return null;
        }
    }

    public double[][] getCumulativePercentageOfClassesThatAreHopsApart() {
        int dataSize = this.hopsFrequencies.keySet().size();
        Map<Integer, Double> hopsCumulativeFrequencies = new HashMap<Integer, Double>();
        double[][] data = new double[2][dataSize];
        double sum = 0.0;
        for (Integer i : hopsFrequencies.keySet()) {
            sum += hopsFrequencies.get(i);
        }
        // Calculate frequency %
        for (Integer i : hopsFrequencies.keySet()) {
            hopsCumulativeFrequencies.put(i, hopsFrequencies.get(i) / sum);
        }
        // Calculate cumulative frequency %
        double previousFrequency = 0, freq = 0;
        int k = 0;
        //We want the numbers to be in ascending order:
        TreeSet<Integer> keySet = new TreeSet<Integer>(hopsFrequencies.keySet());
        for (Integer i : keySet) {
            freq = hopsCumulativeFrequencies.get(i) + previousFrequency;
            hopsCumulativeFrequencies.put(i, freq);
            //	System.out.println(i*1.0+"\t"+freq);
            previousFrequency = freq;
            data[0][k] = i;
            data[1][k] = freq;
            k++;
        }
        return data;
    }

    public final String getInDegreeVSClusteringCoefficientMapString() {
        StringBuilder sb = new StringBuilder();
        for (Integer degree : this.inDegreeToClusteringCoefMap.keySet()) {
            sb.append(degree).append(" \t ").append(inDegreeToClusteringCoefMap.get(degree)).append("\n");
        }
        return sb.toString();
    }

    public String getDegreeVSCcountMapString() {
        StringBuffer sb = new StringBuffer();
        Map<Integer, Integer> tempMap = new TreeMap<Integer, Integer>(inDegreeToCountMap);
        for (Integer degree : tempMap.keySet()) {
            sb.append("" + degree + " \t " + tempMap.get(degree) + "\n");
        }

        return sb.toString();
    }

    public String getHopsFrequenciesMapString() {
        StringBuffer sb = new StringBuffer();
        for (Integer pathLength : hopsFrequencies.keySet()) {
            sb.append(pathLength + " \t " + hopsFrequencies.get(pathLength) + "\n");
        }
        return sb.toString();
    }

    public String getCumulativeHopsPercentageMapString() {
        StringBuffer sb = new StringBuffer();
        double[][] data = getCumulativePercentageOfClassesThatAreHopsApart();
        int dataSize = this.hopsFrequencies.keySet().size();
        for (int i = 0; i < dataSize; i++) {
            sb.append("" + data[0][i] + " \t " + data[1][i] + "\n");
        }
        return sb.toString();
    }

    public void printInDegreeVSClusteringCoefficientMap() {
        StringBuilder sb = new StringBuilder();
        sb.append("Project : ").append(SelectedProjectName.getInstance().getSelectedProjectName()).append(" version: ").append(this.projectVersion).append("\n---------------------------\n");
        sb.append("In Degree\tClustering Coefficient\n");
        sb.append(getInDegreeVSClusteringCoefficientMapString());
        logger.log(Level.FINE, sb.toString());
    }

    public void printInDegreeVSCcountMap() {
        StringBuilder sb = new StringBuilder();
        sb.append("Project : ").append(SelectedProjectName.getInstance().getSelectedProjectName()).append("\n---------------------------\n");
        sb.append("In Degree\tCount\n");
        sb.append(getDegreeVSCcountMapString());
        logger.log(Level.FINE, sb.toString());
    }

    public void printHopsFrequenciesMap() {
        StringBuilder sb = new StringBuilder();
        sb.append("Project : ").append(SelectedProjectName.getInstance().getSelectedProjectName()).append("\n---------------------------\n");
        sb.append("Path Length\tFrequency\n");
        sb.append(getHopsFrequenciesMapString());
        logger.log(Level.FINE, sb.toString());
    }

    public void printCumulativeHopsPercentageMap() {
        StringBuilder sb = new StringBuilder();
        sb.append("Project : ").append(SelectedProjectName.getInstance().getSelectedProjectName()).append(" version : ").append(projectVersion).append("\n---------------------------\n");
        sb.append("Path Length\tCumulative Percentage\n");
        sb.append(getCumulativeHopsPercentageMapString());
        logger.log(Level.FINE, sb.toString());
    }

    public Collection<MyNode> getNeighboorhood(MyNode aNode) {
        return this.getNeighbors(aNode);
    }

    public Collection<MyNode> getNeighboorhood(MyNode aNode, int desiredNeighboorhoodAge) {
        Collection<MyNode> neighboors = getNeighboorhood(aNode);
        Collection<MyNode> properNeighboors = new HashSet<MyNode>();
        for (MyNode node : neighboors) {
            if (node.getAge() == desiredNeighboorhoodAge) {
                properNeighboors.add(node);
            }
        }
        if (properNeighboors.size() > 0) {
            return properNeighboors;
        } else {
            return null;
        }
    }

    public Collection<MyNode> getOutNeighboorhood(MyNode aNode) {
        Collection<MyNode> outNeighbors = this.getSuccessors(aNode);
        if (outNeighbors == null) {
            return new ArrayList<>();
        } else {
            return outNeighbors;
        }
    }

    public Collection<MyNode> getInNeighboorhood(MyNode aNode) {
        Collection<MyNode> inNeighbors = this.getPredecessors(aNode);
        if (inNeighbors == null) {
            return new ArrayList<>();
        } else {
            return inNeighbors;
        }
    }

    public Collection<MyPackage> getOutPackageNeighboorhood(MyNode aNode) {
        Collection<MyNode> outNeighboors = getOutNeighboorhood(aNode);
        Collection<MyPackage> outPackages = new HashSet<>();
        for (MyNode node : outNeighboors) {
            outPackages.add(node.getPackage());
        }
        return outPackages;
    }

    public Collection<MyPackage> getInPackageNeighboorhood(MyNode aNode) {
        Collection<MyNode> inNeighboors = getInNeighboorhood(aNode);
        Collection<MyPackage> outPackages = new HashSet<MyPackage>();
        for (MyNode node : inNeighboors) {
            outPackages.add(node.getPackage());
        }
        return outPackages;
    }

    public Collection<MyEdge> incomingEdgesOf(MyNode aNode) {
        return getInEdges(aNode);
    }

    public Collection<MyEdge> outgoingEdgesOf(MyNode aNode) {
        return getOutEdges(aNode);
    }

    public boolean doesEdgeExceedsMaxHopDistanceNew11(MyNode sourceNode, MyNode destinationNode, double maxHopDistance) {
        MyEdge myEdge = new MyEdge(sourceNode, destinationNode, "sim_Dummy");
        addEdge(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED);
        double newDiameter = DistanceStatistics.diameter(this, distanceMeasure, true);
        removeEdge(myEdge);
        if (newDiameter > maxHopDistance + 2) {
            return true;
        } else {
            return false;
        }
    }

    public boolean doesEdgeExceedsMaxHopDistanceNew(MyNode sourceNode, MyNode destinationNode, double maxHopDistance) {
        int maxLengthOfAllShortestPathsThatStartFromDestinationNode = 0;
        int maxLengthOfAllShortestPathsThatEndToSourceNode = 0;
        ArrayList<Integer> shortestPathLengthsFromDestNode = new ArrayList<Integer>();
        ArrayList<Integer> shortestPathLengthsToSourceNode = new ArrayList<Integer>();
        if (distanceMeasure == null) {
            distanceMeasure = new UnweightedShortestPath<MyNode, MyEdge>(this);
        }
        //	System.out.println("----###################--------- MAX HOP DISTANCE CHECKING...");
        for (MyNode node : getVertices()) {
            Collection<MyNode> neighborsOfNode = getNeighboorhood(node);
            if (!node.equals(sourceNode) && !node.equals(destinationNode) && !neighborsOfNode.contains(sourceNode) && !neighborsOfNode.contains(destinationNode)) {
                try {
                    Number distanceFromFromDestinationNodeToThisNode = distanceMeasure.getDistance(destinationNode, node);
                    Number distanceFromThisNodeToSourceNode = distanceMeasure.getDistance(node, sourceNode);
                    if (distanceFromFromDestinationNodeToThisNode != null) {
                        shortestPathLengthsFromDestNode.add(distanceFromFromDestinationNodeToThisNode.intValue());
                    }
                    if (distanceFromThisNodeToSourceNode != null) {
                        shortestPathLengthsToSourceNode.add(distanceFromThisNodeToSourceNode.intValue());
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, null, e);
                }
            }
        }
        if (shortestPathLengthsFromDestNode.size() > 0) {
            maxLengthOfAllShortestPathsThatStartFromDestinationNode = Collections.max(shortestPathLengthsFromDestNode);
        }
        if (shortestPathLengthsToSourceNode.size() > 0) {
            maxLengthOfAllShortestPathsThatEndToSourceNode = Collections.max(shortestPathLengthsToSourceNode);
        }

        int totalLengthOfTheNewPath = maxLengthOfAllShortestPathsThatEndToSourceNode + maxLengthOfAllShortestPathsThatStartFromDestinationNode + 1;
        //	System.out.println("----###################--------- MAX HOP DISTANCE CHECKING.....DONE!!!!!!!!!!!");
        if (totalLengthOfTheNewPath > maxHopDistance) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Returns the shortest path length between these two nodes, or -1 if no path exists. 
     */
    public int shortestPathLengthBetween(MyNode sourceNode, MyNode targetNode) {
        if (distanceMeasure == null) {
            distanceMeasure = new UnweightedShortestPath<MyNode, MyEdge>(this);
        }
        Number distance = distanceMeasure.getDistance(sourceNode, targetNode);
        if (distance == null) {
            return 0;
        } else {
            return distance.intValue();
        }
    }

    public final void calculateDiameterAndShortestPaths() {

        hopsFrequencies = new HashMap<>();
        pathLengthToPathMap = new HashMap<>();
        pathLengthCumulativeFrequencies = new HashMap<>();

        if (distanceMeasure == null) {
            distanceMeasure = new DijkstraShortestPath<>(this, true);
        }
        for (MyNode sourceNode : getVertices()) {
            for (MyNode targetNode : getVertices()) {
                if (!sourceNode.equals(targetNode)) {
                    Number pathLength = distanceMeasure.getDistance(sourceNode, targetNode);

                    if (pathLength != null && pathLength.intValue() > 0) {

                        if (hopsFrequencies.containsKey(pathLength.intValue())) {
                            hopsFrequencies.put(pathLength.intValue(), hopsFrequencies.get(pathLength.intValue()) + 1);
                        } else {
                            hopsFrequencies.put(pathLength.intValue(), 1);
                        }
                    }
                }
            }
        }
        diameter = DistanceStatistics.diameter(this, distanceMeasure, true);
        double sum = 0;

        for (Integer i : hopsFrequencies.keySet()) {
            sum += hopsFrequencies.get(i);
        }
        // Calculate frequency %
        for (int i = 1; i <= diameter; i++) {
            if (hopsFrequencies.containsKey(i)) {
                pathLengthCumulativeFrequencies.put(i, hopsFrequencies.get(i) / sum);
            } else {
                pathLengthCumulativeFrequencies.put(i, 0.0);
            }
        }
        // Calculate cumulative frequency %
        for (int i = 2; i <= diameter; i++) {
            pathLengthCumulativeFrequencies.put(i, pathLengthCumulativeFrequencies.get(i) + pathLengthCumulativeFrequencies.get(i - 1));
        }

        for (int i = 1; i <= diameter; i++) {
            //	System.out.println(i+"\t\t "+pathLengthFrequencies.get(i)+"\t\t"+pathLengthCumulativeFrequencies.get(i));
        }
    }

    public void printPathsOfLength(int pathLength) {

        List<List<MyEdge>> paths = pathLengthToPathMap.get(pathLength);
        if (paths != null) {
            logger.log(Level.FINE, "paths of length {0} for {1} {2}", new Object[]{pathLength, SelectedProjectName.getInstance().getSelectedProjectName(), projectVersion});
            for (List<MyEdge> path : paths) {
                MyEdge lastEdge = new MyEdge();
                for (MyEdge edge : path) {
                    logger.log(Level.FINE, null, edge.getSourceNode() + " -> ");
                    lastEdge = edge;
                }
                logger.log(Level.FINE, null, lastEdge.getTargetNode());
            }
        }
    }

    /*
     * Calculation using the jung library
     */
    public void calculateCentralities() {
        BetweennessCentrality<MyNode, MyEdge> bc = new BetweennessCentrality<MyNode, MyEdge>(this);
        EigenvectorCentrality<MyNode, MyEdge> egvc = new EigenvectorCentrality<MyNode, MyEdge>(this);

        for (MyNode node : getVertices()) {
            node.setBetweennessCentrality(bc.getVertexScore(node));
            node.setEigenvectorCentrality(egvc.getVertexScore(node));
        }
    }

    public MyNode getNode(String name) {
        for (MyNode node : this.getVertices()) {
            if (node.getName().equals(name)) {
                return node;
            }
        }
        return null;
    }

    public final void calculateGlobalPreferentialAttachment() {
        totalInDegree = calculateTotalInDegree(null);
        for (MyNode node : getVertices()) {
            int nodeDegree = this.inDegree(node);
            double pa = (double) nodeDegree / (double) totalInDegree;
            node.setGlobalPreferentialAttachment(pa);
            globalPreferentialAttachmentOrderedSet.add(pa);
        }
        Collections.sort(globalPreferentialAttachmentOrderedSet);
        cumulativePreferentialAttachment = new Double[globalPreferentialAttachmentOrderedSet.size()];
        cumulativePreferentialAttachment[0] = globalPreferentialAttachmentOrderedSet.get(0);
        for (int i = 1; i < globalPreferentialAttachmentOrderedSet.size(); i++) {
            cumulativePreferentialAttachment[i] = cumulativePreferentialAttachment[i - 1] + globalPreferentialAttachmentOrderedSet.get(i);
        }

    }

    public void calculatePackagePreferentialAttachment(MyPackage aPackage) {
        if (aPackage != null) {
            MyNode node1 = null;
            int totalPackageInDegree = calculateTotalInDegree(aPackage);
            try {
                for (MyNode node : getNodesOfPackage(aPackage)) {
                    node1 = node;
                    int nodeDegree = this.inDegree(node);
                    double pa = (double) nodeDegree / (double) totalPackageInDegree;
                    node.setPackagePreferentialAttachment(pa);
                }
            } catch (Exception e) {
                System.err.println("Error in calculation of PA for package: " + aPackage.getName() + ", "
                        + "Node: " + node1);
                e.printStackTrace();
            }
        }
    }

    private int calculateTotalInDegree(MyPackage aPackage) {
        int totalPackageInDegree = 0;
        if (aPackage != null) {
            try {
                for (MyNode node : getNodesOfPackage(aPackage)) {
                    int inDegree = inDegree(node);
                    totalPackageInDegree += inDegree;
                }
            } catch (Exception e) {
                System.err.println("no nodes for package: " + aPackage.getName());
            }
        } else {
            for (MyNode node : getVertices()) {
                totalPackageInDegree += inDegree(node);
            }
        }
        return totalPackageInDegree;
    }

    private ArrayList<Double> getPackagePreferentialAttachmentOrderedSet(MyPackage aPackage) {
        ArrayList<Double> packagePreferentialAttachmentOrderedSet = new ArrayList<Double>();
        if (aPackage != null) {
            ArrayList<MyNode> nodesOfPackage = getNodesOfPackage(aPackage);
            if (nodesOfPackage != null) {
                for (MyNode node : getNodesOfPackage(aPackage)) {
                    double PA = node.getPackagePreferentialAttachment();
                    if (PA > 0) {
                        packagePreferentialAttachmentOrderedSet.add(PA);
                    }
                }
            }
        }
        Collections.sort(packagePreferentialAttachmentOrderedSet);
        return packagePreferentialAttachmentOrderedSet;
    }

    /*
     * 
     */
    public ArrayList<MyNode> classesAccordingToPackagePreferentialAttachment(MyPackage aPackage, int limitLoops) {
        ArrayList<MyNode> nodeList = new ArrayList<MyNode>();
        int counter = 0;
        do {
            double r = MyRandomGenerator.getNextDoubleRandom();
            double targetPA = 0;
            //	System.out.print("PA-"+counter+"|");
            if (aPackage == null) {
                int setSize = globalPreferentialAttachmentOrderedSet.size();
                double maxPA = globalPreferentialAttachmentOrderedSet.get(setSize - 1);
                double tolerance = 0.1 * maxPA;
                for (int i = 1; i < cumulativePreferentialAttachment.length; i++) {
                    if (r < cumulativePreferentialAttachment[i] && r > cumulativePreferentialAttachment[i - 1]) {
                        targetPA = cumulativePreferentialAttachment[i] - cumulativePreferentialAttachment[i - 1];
                        break;
                    }
                }
                for (MyNode aNode : getVertices()) {
                    if (Math.abs(aNode.getGlobalPreferentialAttachment() - targetPA) < tolerance) {
                        nodeList.add(aNode);
                    }
                }
            } else {
                ArrayList<MyNode> packageNodes = getNodesOfPackage(aPackage);
                ArrayList<Double> preferentialAttachmentOrderedSet = getPackagePreferentialAttachmentOrderedSet(aPackage);
                if (packageNodes != null && packageNodes.size() > 1 && preferentialAttachmentOrderedSet.size() > 0) {
                    Double[] cumulativePreferentialAttachment = new Double[preferentialAttachmentOrderedSet.size()];
                    cumulativePreferentialAttachment[0] = preferentialAttachmentOrderedSet.get(0);
                    int setSize = preferentialAttachmentOrderedSet.size();
                    for (int i = 1; i < setSize; i++) {
                        cumulativePreferentialAttachment[i] = cumulativePreferentialAttachment[i - 1] + preferentialAttachmentOrderedSet.get(i);
                    }
                    double r1 = MyRandomGenerator.getNextDoubleRandom(1.0);
                    double maxPA = preferentialAttachmentOrderedSet.get(setSize - 1);
                    double tolerance = 0.1 * maxPA;

                    if (packageNodes.size() == 2) {
                        nodeList.addAll(packageNodes);
                    } else {
                        for (int i = 1; i < cumulativePreferentialAttachment.length; i++) {
                            if (r1 < cumulativePreferentialAttachment[i] && r1 > cumulativePreferentialAttachment[i - 1]) {
                                targetPA = cumulativePreferentialAttachment[i] - cumulativePreferentialAttachment[i - 1];
                                break;
                            }
                        }
                        for (MyNode aNode : packageNodes) {
                            double pa = aNode.getPackagePreferentialAttachment();
                            if ((Math.abs(targetPA - pa) < tolerance)) {
                                nodeList.add(aNode);
                            }
                        }
                    }
                } else if (packageNodes != null && packageNodes.size() == 1) {
                    return packageNodes;
                } else {
                    //		System.err.println("no nodes or nodes without incoming connections for package: "+aPackage.getName());
                    return null;
                }
            }
            counter++;
        } while (counter < limitLoops && nodeList.size() == 0);
        //	System.out.print("|");
        return nodeList;
    }

    /*
     * This method first lists in decreasing order the classes that conform to the desired age as its sampled from the age distribution
     * then it tries to find a class with age closest to the desired that also has the desired PA
     */
    public MyNode classSelectorAccordingToAgeAndPreferentialAttachment(MyPackage aPackage, boolean considerAge, int loopLimit, int ageRelaxation, FittingFunction ageDistribution) {
        MyNode node = null;
        if (aPackage != null && getNodesOfPackage(aPackage) != null) {
            calculatePackagePreferentialAttachment(aPackage);
        }
        if (!considerAge) {
            ArrayList<MyNode> nodeList = classesAccordingToPackagePreferentialAttachment(aPackage, loopLimit);
            int innerLoopCounter = 0;
            while (nodeList == null || nodeList.size() == 0) {
                nodeList = classesAccordingToPackagePreferentialAttachment(aPackage, loopLimit / 2);
                innerLoopCounter++;
                if (innerLoopCounter >= loopLimit) {
                    return null;
                }
            }
            int size = nodeList.size();
            node = nodeList.get(MyRandomGenerator.getNextIntRandom(size));
        } else {
            ArrayList<MyNode> nodesWithProperAge = getNodesWithProperAge(aPackage, loopLimit / 2, ageRelaxation, ageDistribution);
            if (nodesWithProperAge == null) {
                return null;
            }
            ArrayList<MyNode> nodeList = classesAccordingToPackagePreferentialAttachment(aPackage, loopLimit);
            int loopCounter = 0;
            int innerLoopCounter = 0;
            while (!nodesWithProperAge.contains(node)) {
                while (nodeList == null) {
                    nodeList = classesAccordingToPackagePreferentialAttachment(aPackage, loopLimit / 2);
                    innerLoopCounter++;
                    if (innerLoopCounter >= loopLimit) {
                        return null;
                    }
                }
                int size = nodeList.size();
                if (size > 0) {
                    node = nodeList.get(MyRandomGenerator.getNextIntRandom(size));
                } else {
                    return null;
                }
                loopCounter++;
                if (loopCounter >= loopLimit) {
                    return null;
                }
            }
        }

        return node;
    }

    /*
     * Triangle-closing models
     * Leskovec: Microscopic Evolution of Networks, pages 5,6
     */
    public MyNode classSelectorAccordingTo_2Hops_TriangleClosingModel(ArrayList<MyNode> nodes, int desiredAge) {
        ArrayList<MyNode> nodeList;
        if (nodes == null) {
            nodeList = new ArrayList<MyNode>(getVertices());
        } else {
            nodeList = nodes;
        }
        if (nodeList.size() == 1) {
            return nodeList.get(0);
        } else {
            int randomNum = MyRandomGenerator.getNextIntRandom(nodeList.size());
            MyNode randomNode = nodeList.get(randomNum);
            MyNode[] nn = new MyNode[1];
            Collection<MyNode> friendsOfRandomNode;
            if (desiredAge != -99) {
                friendsOfRandomNode = getNeighboorhood(randomNode, desiredAge);
            } else {
                friendsOfRandomNode = getNeighboorhood(randomNode);
            }

            if (friendsOfRandomNode == null) {
                return randomNode;
            } else {
                if (friendsOfRandomNode.size() == 1) {
                    return friendsOfRandomNode.toArray(nn)[0];
                } else {
                    int randomNum2 = MyRandomGenerator.getNextIntRandom(friendsOfRandomNode.size());
                    return friendsOfRandomNode.toArray(nn)[randomNum2];
                }
            }
        }
    }

    private ArrayList<MyNode> getNodesWithProperAge(MyPackage aPackage, int loopLimit, int ageRelaxation, FittingFunction ageDistribution) {
        ArrayList<MyNode> nodesWithProperAge = new ArrayList<>();
        int counter = 0;
        do {
            if (counter == loopLimit) {
                return null;
            }
            int anAge = ageDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
            anAge = (anAge == 0) ? 1 : anAge;
            if (aPackage != null) {
                try {
                    ArrayList<MyNode> packageNodes = getNodesOfPackage(aPackage);
                    for (MyNode aPackageNode : packageNodes) {
                        if (aPackageNode.getAge() == anAge || Math.abs(aPackageNode.getAge() - anAge) <= ageRelaxation) {
                            nodesWithProperAge.add(aPackageNode);
                        }
                    }
                } catch (Exception e) {
                    System.err.println("problem with package age selection " + aPackage.getName());
                    return null;
                }
            } else {
                for (MyNode aNode : getVertices()) {
                    if (aNode.getAge() == anAge || Math.abs(aNode.getAge() - anAge) <= ageRelaxation) {
                        nodesWithProperAge.add(aNode);
                    }
                }
            }
            counter++;
        } while (nodesWithProperAge.isEmpty());
        return nodesWithProperAge;
    }

    public void calculateDegreeDistribution() {
        calculateInDegreeDistribution();
        calculateOutDegreeDistribution();
    }

    public void calculateInDegreeDistribution() {
        inDegreeToCountMap = new TreeMap<Integer, Integer>();
        for (MyNode aNode : getVertices()) {
            int inDegree = inDegree(aNode);
            if (inDegree > 0) {
                if (!inDegreeToCountMap.containsKey(inDegree)) {
                    inDegreeToCountMap.put(inDegree, 1);
                } else {
                    int oldCount = inDegreeToCountMap.get(inDegree);
                    inDegreeToCountMap.put(inDegree, oldCount + 1);
                }
            }
        }

    }

    public void calculateOutDegreeDistribution() {
        outDegreeToCountMap = new TreeMap<Integer, Integer>();
        for (MyNode aNode : getVertices()) {
            int outDegree = outDegree(aNode);
            if (outDegree > 0) {
                if (!outDegreeToCountMap.containsKey(outDegree)) {
                    outDegreeToCountMap.put(outDegree, 1);
                } else {
                    int oldCount = outDegreeToCountMap.get(outDegree);
                    outDegreeToCountMap.put(outDegree, oldCount + 1);
                }
            }
        }

    }

    public double getDiameter() {
        return new Double(diameter).doubleValue();
    }

    public ArrayList<MyNode> getNodesWithOutDegree(int outDegree) {
        ArrayList<MyNode> nodes = new ArrayList<MyNode>();
        for (MyNode node : getVertices()) {
            if (outDegree == outDegree(node)) {
                nodes.add(node);
            }
        }
        return nodes;
    }

    public ArrayList<MyNode> getNodesWithInDegree(int inDegree) {
        ArrayList<MyNode> nodes = new ArrayList<MyNode>();
        for (MyNode node : getVertices()) {
            if (inDegree == inDegree(node)) {
                nodes.add(node);
            }
        }
        return nodes;
    }

    public MyNode getARandomNodeWithOutDegree(int outDegree) {
        Map<Integer, ArrayList<MyNode>> outDegreeOrderedMap = getOutDegreeOrderedMap();
        ArrayList<MyNode> list = outDegreeOrderedMap.get(outDegree);
        if (list != null) {
            int size = list.size();
            int randomInt = MyRandomGenerator.getNextIntRandom(size);
            return list.get(randomInt);
        } else {
            return null;
        }
    }

    public MyNode getARandomNodeWithInDegree(int inDegree) {
        Map<Integer, ArrayList<MyNode>> inDegreeOrderedMap = getInDegreeOrderedMap();
        ArrayList<MyNode> list = inDegreeOrderedMap.get(inDegree);
        if (list != null) {
            int size = list.size();
            int randomInt = MyRandomGenerator.getNextIntRandom(size);
            return list.get(randomInt);
        } else {
            return null;
        }
    }

    public Map<Integer, ArrayList<MyNode>> getOutDegreeOrderedMap() {
        Map<Integer, ArrayList<MyNode>> outDegreeOrderedMap = new TreeMap<Integer, ArrayList<MyNode>>();
        for (MyNode node : getVertices()) {
            int outDegree = outDegree(node);
            if (!outDegreeOrderedMap.containsKey(outDegree)) {
                ArrayList<MyNode> list = new ArrayList<MyNode>();
                list.add(node);
                outDegreeOrderedMap.put(outDegree, list);
            } else {
                outDegreeOrderedMap.get(outDegree).add(node);
            }
        }
        return outDegreeOrderedMap;
    }

    public Map<Integer, ArrayList<MyNode>> getInDegreeOrderedMap() {
        Map<Integer, ArrayList<MyNode>> inDegreeOrderedMap = new TreeMap<Integer, ArrayList<MyNode>>();
        for (MyNode node : getVertices()) {
            int inDegree = inDegree(node);
            if (!inDegreeOrderedMap.containsKey(inDegree)) {
                ArrayList<MyNode> list = new ArrayList<MyNode>();
                list.add(node);
                inDegreeOrderedMap.put(inDegree, list);
            } else {
                inDegreeOrderedMap.get(inDegree).add(node);
            }
        }
        return inDegreeOrderedMap;
    }

    public MyNode getARandomNode() {
        int classSize = getVertexCount();
        int randomInt = MyRandomGenerator.getNextIntRandom(classSize);
        MyNode[] ar = new MyNode[1];
        return getVertices().toArray(ar)[randomInt];
    }

    public MyNode getARandomNode(MyPackage aPackage) {
        MyNode[] ar = new MyNode[1];
        Map<MyPackage, ArrayList<MyNode>> packageMap = getPackageMap();
        if (packageMap.containsKey(aPackage)) {
            MyNode[] aPackageNodes = packageMap.get(aPackage).toArray(ar);
            int nodesSize = aPackageNodes.length;
            int randomInt = MyRandomGenerator.getNextIntRandom(nodesSize);
            return aPackageNodes[randomInt];
        }
        return null;
    }

    public Version getProjectVersion() {
        return this.projectVersion;
    }

    public void setProjectVersion(Version pv) {
        this.projectVersion = pv;
    }

    public double getClusteringCoefficient() {
        DecimalFormat twoDForm = new DecimalFormat("#.######");
        return Double.valueOf(twoDForm.format(clusteringCoefficientGlobal));
    }

    public void updateNodeAges(MyGraph prevGraph) {
        for (MyNode node : getVertices()) {
            try {
                MyNode prevNode = prevGraph.getNode(node.getName());
                if (prevNode != null) {
                    node.setAge(prevNode.getAge() + 1);
                }
            } catch (NullPointerException e) {
                logger.log(Level.SEVERE, "Previous graph does not exist. Please check. Current version = " + this.getProjectVersion(), e);
            }
        }
    }

    public double getDensity() {
        double edges = this.getEdges().size();
        double nodes = this.getVertexCount();
        DecimalFormat twoDForm = new DecimalFormat("#.######");
        return Double.valueOf(twoDForm.format(edges / (nodes * (nodes - 1))));
    }

    public void printGraphBasicProperties() {
        System.out.format("%1$-15s \t %2$11s \t%3$11s  \t%4$7s \t%5$5s \t%6$5s \t%7$6s \n", "Version", "Clust.Coeff", "Density", "Diameter", "nodes", "edges", "alpha");
        System.out.format("%1$-15s \t%2$10.9f \t%3$10.9f \t%4$3.0f \t%5$5d \t%6$5d \t%7$6.5f \n", projectVersion, getClusteringCoefficient(), getDensity(), getDiameter(), getVertexCount(), getEdges().size(), getAlpha());
    }

    public String getGraphBasicProperties() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%1$-15s \t %2$11s \t %3$11s  \t%4$7s \t%5$5s \t%6$5s \t%7$6s \n", "Version", "Clust.Coeff", "Density", "Diameter", "nodes", "edges", "alpha"));
        sb.append(String.format("\"%1$-15s \t%2$10.9f \t%3$10.9f \t%4$3.0f \t%5$5d \t%6$5d \t%7$6.5f \n",
                projectVersion, getClusteringCoefficient(), getDensity(), diameter, getVertexCount(), getEdges().size(), getAlpha()));
        return sb.toString();
    }

    public void printGraphMetrics() {
        logger.log(Level.FINE, null, "=========Graph Metrics ====== VERSION: " + projectVersion);
        this.printHopsFrequenciesMap();
        this.printCumulativeHopsPercentageMap();
        this.printInDegreeVSCcountMap();
        this.printInDegreeVSClusteringCoefficientMap();
    }

    public void appendGraphMetrics(StringBuilder sb) {
        sb.append("=========Graph Metrics ====== VERSION: ").append(projectVersion).append("\n");
        sb.append("Hops\tCount").append("\n");
        sb.append(this.getHopsFrequenciesMapString()).append("\n");
        sb.append("Hops\tCumul. %").append("\n");
        sb.append(this.getCumulativeHopsPercentageMapString()).append("\n");
        sb.append("In Degree\tCount").append("\n");
        sb.append(this.getDegreeVSCcountMapString()).append("\n");
        sb.append("In Degree\tCC").append("\n");
        sb.append(this.getInDegreeVSClusteringCoefficientMapString()).append("\n");
    }

    public void printFullGraphDescription() {
        logger.log(Level.FINE, null, "Version\tClust.Coeff\tDiameter\tnodes\tedges");
        logger.log(Level.FINE, null, projectVersion + "\t" + getClusteringCoefficient() + "\t" + diameter + "\t" + this.getVertexCount() + "\t" + this.getEdges().size());
        for (MyEdge edge : this.getEdges()) {
            logger.log(Level.FINE, null, "" + edge.toString());
        }
    }

    public Map<Integer, Integer> getHopsFrequencies() {
        return hopsFrequencies;
    }

    public Set<MyNode> getVerticesOfAge(int age) {
        Set<MyNode> mySet = new TreeSet<MyNode>();
        for (MyNode node : getVertices()) {
            if (node.getAge() == age) {
                mySet.add(node);
            }
        }
        return mySet;
    }

    public List<MyNode> getExistingNodes() {
        List<MyNode> mySet = new ArrayList<MyNode>();
        for (MyNode node : getVertices()) {
            if (node.getAge() > 0) {
                mySet.add(node);
            }
        }
        return mySet;
    }

    public Map<String, Integer> getClassesAndOutDegrees() {
        Map<String, Integer> outDegreeOrderedMap = new TreeMap<String, Integer>();
        Map<Integer, ArrayList<MyNode>> outDegreeMap = getOutDegreeOrderedMap();
        for (Integer i : outDegreeMap.keySet()) {
            ArrayList<MyNode> nodeList = outDegreeMap.get(i);
            for (MyNode node : nodeList) {
                outDegreeOrderedMap.put(node.getName(), i);
            }
        }
        return outDegreeOrderedMap;
    }

    public Map<String, Integer> getClassesAndInDegrees() {
        Map<String, Integer> inDegreeOrderedMap = new TreeMap<String, Integer>();
        Map<Integer, ArrayList<MyNode>> inDegreeMap = getInDegreeOrderedMap();
        for (Integer i : inDegreeMap.keySet()) {
            ArrayList<MyNode> nodeList = inDegreeMap.get(i);
            for (MyNode node : nodeList) {
                inDegreeOrderedMap.put(node.getName(), i);
            }
        }
        return inDegreeOrderedMap;
    }

    public Map<MyPackage, ArrayList<MyNode>> getPackageMap() {
        Map<MyPackage, ArrayList<MyNode>> packageMap = new TreeMap<MyPackage, ArrayList<MyNode>>();
        for (MyNode node : getVertices()) {
            MyPackage nodePackage = node.getPackage();
            if (packageMap.containsKey(nodePackage)) {
                packageMap.get(nodePackage).add(node);
            } else {
                ArrayList<MyNode> nodeList = new ArrayList<MyNode>();
                nodeList.add(node);
                packageMap.put(nodePackage, nodeList);
            }
        }
        return packageMap;
    }

    public ArrayList<MyNode> getNodesOfPackage(MyPackage aPackage) {
        return getPackageMap().get(aPackage);
    }

    public String getFriendships() {
        StringBuilder sb = new StringBuilder();
        for (MyEdge edge : getEdges()) {
            sb.append(edge.getSourceNode().getName()).append("\t").append(edge.getTargetNode().getName());
            sb.append("\n");
        }
        return sb.toString();
    }

    public String getClassLevelMetrics(IProgressMonitor monitor) {
        calculateInDegreeDistribution();
        calculateClusteringCoefficient();
        calculateCentralities();
        StringBuilder sb = new StringBuilder();
        String t = "\t";
        sb.append("Class Name" + t + "In Degree" + t + "Out Degree" + t + "Clustering Coefficient" + t + "Betweeness Centrality" + t + "Page Rank" + t + "Eigenvector Centrality" + t + "Is Abstract?" + t + "Is Interface?");
        sb.append("\n");
        for (MyNode node : getVertices()) {
            sb.append(node.getName() + t);
            sb.append(inDegree(node) + t);
            sb.append(outDegree(node) + t);
            sb.append(node.getClusteringCoefficient() + t);
            sb.append(node.getBetweenessCentrality() + t);
            sb.append(node.getPageRank() + t);
            sb.append(node.getEigenvectorCentrality() + t);
            sb.append(node.isAbstract() + t);
            sb.append(node.isInterface() + t);
            sb.append("\n");
        }
        return sb.toString();
    }

    public Map<Integer, Integer> getExistingNodeInDegreeToNewEdgesAttractedFrequencyMap() {
        return existingNodeInDegreeToNewEdgesAttractedFrequencyMap;
    }

    public void setExistingNodeInDegreeToNewEdgesAttractedFrequencyMap(
            Map<Integer, Integer> existingNodeInDegreeToNewEdgesAttractedFrequencyMap) {
        this.existingNodeInDegreeToNewEdgesAttractedFrequencyMap = existingNodeInDegreeToNewEdgesAttractedFrequencyMap;
    }

    public Map<Integer, Integer> getInDegreeToCountMap() {
        return inDegreeToCountMap;
    }

    public void calculateAfferentPackageCoupling() {
        Map<MyPackage, ArrayList<MyNode>> packageMap = getPackageMap();
        for (MyPackage aPackage : packageMap.keySet()) {
            Set<MyPackage> packagesThatReferenceThisPackage = new TreeSet<MyPackage>();
            for (MyNode node : packageMap.get(aPackage)) {
                packagesThatReferenceThisPackage.addAll(getInPackageNeighboorhood(node));
            }
            aPackage.setAfferentCoupling(packagesThatReferenceThisPackage.size());
        }
    }

    public void calculateEfferentPackageCoupling() {
        Map<MyPackage, ArrayList<MyNode>> packageMap = getPackageMap();
        for (MyPackage aPackage : packageMap.keySet()) {
            Set<MyPackage> packagesThatAreReferencedFromThisPackage = new TreeSet<MyPackage>();
            for (MyNode node : packageMap.get(aPackage)) {
                packagesThatAreReferencedFromThisPackage.addAll(getOutPackageNeighboorhood(node));
            }
            aPackage.setEfferentCoupling(packagesThatAreReferencedFromThisPackage.size());
        }
    }

    public void calculateMetrics() {
        this.calculateClusteringCoefficient();
        this.calculateAfferentPackageCoupling();
        this.calculateEfferentPackageCoupling();
        this.calculateDegreeDistribution();
    }

    public MyPackage getPackage(String name) {
        Map<MyPackage, ArrayList<MyNode>> packageMap = getPackageMap();
        for (MyPackage aPack : packageMap.keySet()) {
            if (aPack.getName().equals(name)) {
                return aPack;
            }
        }
        return null;
    }

    public String toString() {
        return projectName + "_" + this.projectVersion;
    }

    public boolean removeEdge(MyEdge edge, GraphEventLogger eventLogger) {
        boolean b = super.removeEdge(edge);
        GraphEventLogger.logEvent(new EdgeRemovalEvent(edge));
        return b;
    }

    public ArrayList<MyNode> getRandomNodes(int number) {
        ArrayList<MyNode> randomNodes = new ArrayList<MyNode>();
        if (number >= this.vertices.size()) {
            return new ArrayList<MyNode>(this.vertices.keySet());
        }
        for (int i = 0; i < number; i++) {
            randomNodes.add(this.getARandomNode());
        }
        return randomNodes;
    }

    public void setProjectName(String name) {
        this.projectName = name;
    }

    public String getProjectName() {
        return projectName;
    }

    public void printNodes() {
        logger.log(Level.INFO, "---------NODES-------ver " + this.getProjectVersion());
        for (MyNode node : this.getVertices()) {
            logger.log(Level.INFO, node.hashCode() + "\t" + node + " age : " + node.getAge());
        }
        logger.log(Level.INFO, "-------------------------");
    }

    public void printEdges() {
        logger.log(Level.INFO, "---------EDGES-------ver " + this.getProjectVersion());
        for (MyEdge edge : this.getEdges()) {
            logger.log(Level.INFO, edge + "");
        }
        logger.log(Level.INFO, "-------------------------");
    }

}
