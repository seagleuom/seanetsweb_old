package gr.uom.java.seanets.graph.event;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyNode;


public abstract class MyGraphEvent {
	
	public abstract void reproduce(Graph<MyNode, MyEdge> graph, VisualizationViewer<MyNode, MyEdge> visualizationViewer);
	
}
