package gr.uom.java.seanets.graph.event;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class GraphEventLogger {
	
	private static Queue<MyGraphEvent> graphEvents;
	
	public GraphEventLogger(){
		graphEvents = new ConcurrentLinkedQueue<MyGraphEvent>();
	}
	
	public static void logEvent(MyGraphEvent e){
		graphEvents.add(e);
	}
	
	public static Queue<MyGraphEvent> getEvents(){
		return graphEvents;
	}
	
}
