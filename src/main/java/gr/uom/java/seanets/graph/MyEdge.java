package gr.uom.java.seanets.graph;

import gr.uom.java.seanets.db.persistence.Version;

import java.io.Serializable;

public class MyEdge implements Serializable {

    private static final long serialVersionUID = 1L;
    private String from;
    private String to;

    private MyNode sourceNode;
    private MyNode targetNode;
    private String direction;
    private String friendship;

    private Version versionAdded;

    public MyEdge() {
        from = "" + Math.random();
        to = "" + Math.random();
    }

    public MyEdge(String from, String to, String friendship) {
        this.from = from;
        this.to = to;
        this.direction = "->";
        this.friendship = friendship;
    }

    public MyEdge(MyNode from, MyNode to, String friendship) {
        this.sourceNode = from;
        this.targetNode = to;
        this.from = from.getName();
        this.to = to.getName();
        this.friendship = friendship;
    }

    public MyEdge(String from, String to) {
        this.from = from;
        this.to = to;
        this.direction = "->";
    }

    public void setFriendship(String friendship) {
        this.friendship = friendship;
    }

    public String getFriendship() {
        return friendship;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public MyNode getSourceNode() {
        return sourceNode;
    }

    public void setSourceNode(MyNode sourceNode) {
        this.from = sourceNode.getName();
        this.sourceNode = sourceNode;
    }

    public MyNode getTargetNode() {
        return targetNode;
    }

    public void setTargetNode(MyNode targetNode) {
        this.to = targetNode.getName();
        this.targetNode = targetNode;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Version getVersionAdded() {
        return versionAdded;
    }

    public void setVersionAdded(Version versionAdded) {
        this.versionAdded = versionAdded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof MyEdge) {
            MyEdge edge = (MyEdge) o;
            return ((this.from.equals(edge.from))
                    && (this.to.equals(edge.to)));
            //&&(this.direction.equals(edge.direction))
        }
        return false;
    }

    public boolean equals(MyEdge edge) {
        return ((this.from.equals(edge.from)) && (this.to.equals(edge.to)));
    }

    public boolean equals2(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof MyEdge) {
            MyEdge edge = (MyEdge) o;
            return ((this.from.equals(edge.from)) && (this.to.equals(edge.to)));
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (friendship != null) {
            sb.append(from + " > " + this.friendship + " > " + to + "  ");
        } else {
            sb.append(from + " -> " + to + "  ");
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return ((from.hashCode() * -31) % 17 + (to.hashCode() * -7) % 17) * 31;
        //	return (from.hashCode() + to.hashCode() ) * -31;
    }

}
