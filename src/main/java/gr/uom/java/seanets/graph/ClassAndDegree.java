package gr.uom.java.seanets.graph;

public class ClassAndDegree implements Comparable<ClassAndDegree>{
	
	private String className;
	private int degree;
	
	
	public ClassAndDegree(String className, int degree) {
		super();
		this.className = className;
		this.degree = degree;
	}
	
	public String getClassName() {
		return className;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
	
	public int getDegree() {
		return degree;
	}
	
	public void setDegree(int degree) {
		this.degree = degree;
	}


	@Override
	public int compareTo(ClassAndDegree o) {
		if(this.degree < o.degree)
			return 1;
		else if(this.degree > o.degree)
			return -1;
		else
			return 0;
	}
	

}
