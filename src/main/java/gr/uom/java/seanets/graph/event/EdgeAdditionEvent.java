package gr.uom.java.seanets.graph.event;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;

public class EdgeAdditionEvent extends MyGraphEvent{
	
	private MyEdge edgeAdded;
	private MyNode sourceNode, destinationNode;
	private EdgeType edgeType;
	
	public EdgeAdditionEvent(MyEdge edge, MyNode source, MyNode target, EdgeType type){
		edgeAdded = edge;
		sourceNode = source;
		destinationNode = target;
		edgeType = type;
	}

	@Override
	public void reproduce(Graph<MyNode, MyEdge> graph, VisualizationViewer<MyNode, MyEdge> visualizationViewer) {
		graph.addEdge(edgeAdded, sourceNode, destinationNode, edgeType);
		visualizationViewer.getRenderContext().getPickedEdgeState().pick(edgeAdded, true);
		visualizationViewer.getRenderContext().getPickedVertexState().pick(sourceNode, true);
		visualizationViewer.getRenderContext().getPickedVertexState().pick(destinationNode, true);
	}

}
