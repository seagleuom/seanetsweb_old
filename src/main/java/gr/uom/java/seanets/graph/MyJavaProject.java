package gr.uom.java.seanets.graph;

import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.seanets.db.persistence.DBGraph;
import gr.uom.java.seanets.db.persistence.DBNode;
import gr.uom.java.seanets.db.persistence.Version;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

public class MyJavaProject implements Serializable {

    private static final long serialVersionUID = 1L;
    private MyGraph projectGraph;
    private Version projectVersion;
    private Set<String> javaFilePaths;
    private String sourceFolderForThisRevision;
    private String projectName;

    public MyJavaProject(Version version, String projectName) {
        projectVersion = version;
        this.projectName = projectName;
        javaFilePaths = new HashSet<String>();
    }

    public String getProjectName() {
        return projectName;
    }

    public Set<String> getJavaFilePaths() {
        return javaFilePaths;
    }

    public void addJavaFilePath(String path) {
        javaFilePaths.add(path);
    }

    public MyGraph getProjectGraph() {
        return projectGraph;
    }

    public Version getProjectVersion() {
        return projectVersion;
    }

    public String getSourceFolder() {
        return sourceFolderForThisRevision;
    }

    public void setSourceFolderForThisRevision(String path) {
        this.sourceFolderForThisRevision = path;
    }

    public DBGraph createGraph(SystemObject systemObject, ProjectEvolutionAnalysis projectEvolutionAnalysis) {
        Map<String, Set<String>> accessedClassesMap = new LinkedHashMap<String, Set<String>>();
        Map<String, Set<MyEdge>> accessedEdgesMap = new LinkedHashMap<String, Set<MyEdge>>();

        createProjectGraphNodes(systemObject);
        createProjectGraphEdges(systemObject, accessedClassesMap, accessedEdgesMap);

        projectEvolutionAnalysis.getProjectGraphEvolution().put(projectVersion, projectGraph);
        projectEvolutionAnalysis.performGraphCalculations(projectVersion, null, accessedClassesMap, accessedEdgesMap, projectGraph);
        projectEvolutionAnalysis.addGraphInEvolutionHistory(projectVersion, projectGraph);

        DBGraph dbGraph = new DBGraph(projectVersion, projectGraph);
        for (MyNode node : projectGraph.getVertices()) {
            DBNode dbNode = new DBNode(node, dbGraph);
            dbGraph.getdBNodesCollection().add(dbNode);
        }
        projectEvolutionAnalysis.putDBGraphForAVersion(projectVersion, dbGraph);
        return dbGraph;
    }

    public void createProjectGraphNodes(SystemObject systemObject) {
        projectGraph = systemObject.getSystemGraphNoEdges();
        projectGraph.setProjectName(projectName);
        projectGraph.setProjectVersion(projectVersion);
    }

    public void createProjectGraphEdges(SystemObject systemObject, Map<String, Set<String>> accessedClassesMap,
            Map<String, Set<MyEdge>> accessedEdgesMap) {
        if (projectGraph != null) {
            ListIterator<ClassObject> classObjects = systemObject.getClassListIterator();
            Collection<MyNode> classNames = projectGraph.getVertices();
            while (classObjects.hasNext()) {
                Set<String> accessedClasses = new LinkedHashSet<String>();
                Set<MyEdge> accessedEdges = new LinkedHashSet<MyEdge>();
                ClassObject class1 = classObjects.next();
                for (MyNode node2 : classNames) {
                    if (!class1.getName().equals(node2.getName())) {
                        if (class1.isFriend(node2.getName())) {
                            accessedClasses.add(node2.getName());
                            MyNode node1 = projectGraph.getNode(class1.getName());
                            MyEdge myEdge = new MyEdge(node1, node2, "");
                            projectGraph.addEdge(myEdge, node1, node2, EdgeType.DIRECTED);
                            myEdge.setFriendship(class1.isFriend2(node2.getName()));
                            myEdge.setVersionAdded(projectVersion);
                            accessedEdges.add(myEdge);
                        }
                    }
                }
                accessedClassesMap.put(class1.getName(), accessedClasses);
                accessedEdgesMap.put(class1.getName(), accessedEdges);
            }
        }
    }

    @Override
    public String toString() {
        return projectName + "_" + this.projectVersion.toString();
    }

}