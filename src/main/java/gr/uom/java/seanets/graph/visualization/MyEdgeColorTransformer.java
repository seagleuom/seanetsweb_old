package gr.uom.java.seanets.graph.visualization;

import gr.uom.java.seanets.graph.MyEdge;

import java.awt.Color;
import java.awt.Paint;

import org.apache.commons.collections15.Transformer;

public class MyEdgeColorTransformer implements Transformer<MyEdge, Paint>{
	
	@Override
	public Paint transform(MyEdge e) {
		return Color.gray;
	}

}
