package gr.uom.java.seanets.graph.event;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;


public class NodeAdditionEvent extends MyGraphEvent{

	private MyNode nodeAdded;
	
	public NodeAdditionEvent(MyNode node){
		nodeAdded = node;
	}
	
	@Override
	public void reproduce(Graph<MyNode, MyEdge> graph, VisualizationViewer<MyNode, MyEdge> visualizationViewer) {
		graph.addVertex(nodeAdded);
		visualizationViewer.getRenderContext().getPickedVertexState().pick(nodeAdded, true);
	}
	

}
