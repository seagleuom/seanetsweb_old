package gr.uom.java.seanets.simulation;

import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.MyPackage;
import gr.uom.java.seanets.graph.event.EdgeAdditionEvent;
import gr.uom.java.seanets.graph.event.GraphEventLogger;
import gr.uom.java.seanets.graph.event.NodeAdditionEvent;
import gr.uom.java.seanets.graph.visualization.GraphVisualizer;
import gr.uom.java.seanets.history.NodeBehavior;
import gr.uom.java.seanets.history.NodeBehaviorHistory;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.statistics.FittingFunction;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewClassInserterFullModel {

    private ProjectEvolutionAnalysis projectEvolutionAnalysis;
    private NodeBehaviorHistory newNodeBehaviors;

    private FittingFunction edgesNumberThatMustCreatedFromNewNodesInTotal;
    private FittingFunction edgesNumberThatANewNodeMustCreate;
    private FittingFunction nodeAgeToNewIncomingEdgesDistribution;

    private final static Logger logger = Logger.getLogger(NewClassInserterFullModel.class.getName());

    public NewClassInserterFullModel(ProjectEvolutionAnalysis pa, DistributionManager dm) {
        projectEvolutionAnalysis = pa;
        newNodeBehaviors = projectEvolutionAnalysis.getNewNodeBehaviors();
        edgesNumberThatMustCreatedFromNewNodesInTotal = dm.getEdgesNumberThatMustCreatedFromNewNodesInTotal();
        edgesNumberThatANewNodeMustCreate = dm.getEdgesNumberThatANewNodeMustCreate();
        nodeAgeToNewIncomingEdgesDistribution = dm.getNodeAgeToNewIncomingEdgesDistribution();
    }

    private MyNode prepareANewNode(MyGraph simulatedGraph, Set<MyPackage> packagesThisNodeAttachedTo) {
        NodeBehavior nodeBehavior = newNodeBehaviors.getARandomNodeBehavior();
        packagesThisNodeAttachedTo.addAll(nodeBehavior.getPackagesAttached());
        MyPackage newNodePackage = nodeBehavior.getNode().getPackage();
        String newClassName = newNodePackage.getName() + ".NewClass_" + EvolutionSimulator.NEW_CLASS_COUNTER;
        EvolutionSimulator.NEW_CLASS_COUNTER++;
        MyNode newNode = new MyNode(newClassName, newNodePackage, simulatedGraph);
        simulatedGraph.addVertex(newNode);
        GraphEventLogger.logEvent(new NodeAdditionEvent(newNode));
        Set<MyPackage> graphPackages = simulatedGraph.getPackageMap().keySet();
        if (!graphPackages.containsAll(packagesThisNodeAttachedTo)) {
            packagesThisNodeAttachedTo.retainAll(graphPackages);
        }
        return newNode;
    }

    public void insertNewNodes(
            GraphVisualizer visualizer, 
            double A, 
            MyGraph simulatedGraph, 
            Reporter reporter, 
            double diameter,
            boolean considerNodeAge,
            boolean preserveHopDistance) {
        /*We should prevent the new edge from creating huge chains of classes (many hops apart). Thus we set a limit. The new chain that would be
         * created from the new edge, must be no longer than the distance of the 90% of the class pairs of the final version of the project.
         */
        /* A: separator: with prob A new edges are attached on randomly selected nodes
         * with prob 1-A new edges attached according to Preferential Attachment prob.
         */
        simulatedGraph.calculateGlobalPreferentialAttachment();
        logger.log(Level.INFO, "Simulation: inserting new Classes.");

        int howManyClassesWillBeInserted = projectEvolutionAnalysis.getAverageNumberOfNewClassesInAllVersions();
        int howManyEdgesWillBeCreated = edgesNumberThatMustCreatedFromNewNodesInTotal.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
        if (howManyEdgesWillBeCreated < howManyClassesWillBeInserted) {
            howManyEdgesWillBeCreated = howManyClassesWillBeInserted;
        }

        for (int i = 0; i < howManyClassesWillBeInserted; i++) {
            MyNode destinationNode = null;
            double randomNum = MyRandomGenerator.getNextDoubleRandom();
            Set<MyPackage> packagesThisNodeAttachedTo = new TreeSet<MyPackage>();
            MyNode newNode = prepareANewNode(simulatedGraph, packagesThisNodeAttachedTo);
            ArrayList<MyNode> destinationNodes = new ArrayList<MyNode>();
            int howManyEdgesMustBeCreatedByThisNode = edgesNumberThatANewNodeMustCreate.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
            if (howManyEdgesMustBeCreatedByThisNode == 0) {
                howManyEdgesMustBeCreatedByThisNode = 1;
            }
            boolean foundProperDestinationNode = true;
            int ageRelaxation = 0;
            int internalLoopCounter = 0;
            int bigCounter = 0;
            int edgesCreatedSoFar = 0;
            MyPackage[] dummy = new MyPackage[1];
            while (destinationNodes.size() < packagesThisNodeAttachedTo.size()) {
                MyPackage[] aPackageArray = packagesThisNodeAttachedTo.toArray(dummy);
                MyPackage aPackage = aPackageArray[MyRandomGenerator.getNextIntRandom(aPackageArray.length)];
                if (randomNum < A) {
                    destinationNode = simulatedGraph.getARandomNode(aPackage);
                } else {
                    destinationNode = simulatedGraph.classSelectorAccordingToAgeAndPreferentialAttachment(aPackage, considerNodeAge, EvolutionSimulator.loopLimit, ageRelaxation, nodeAgeToNewIncomingEdgesDistribution);
                }
                int loopCounter = 0;
                boolean hopBreak = false;
                while (destinationNode == null
                        || (simulatedGraph.findEdge(newNode, destinationNode) != null)
                        || (destinationNode.getAge() == 0)
                        || (destinationNode.equals(newNode))
                        || (preserveHopDistance && (hopBreak = simulatedGraph.doesEdgeExceedsMaxHopDistanceNew(newNode, destinationNode, diameter)))) {
                    if (destinationNode == null) {
                        reporter.increaseEdgesRejectedDueToNullDestination();
                    } else if (hopBreak) {
                        reporter.increaseEdgesRejectedDueToPathLength();
                    }
                    if (randomNum < A) {
                        destinationNode = simulatedGraph.getARandomNode(aPackage);
                    } else {
                        destinationNode = simulatedGraph.classSelectorAccordingToAgeAndPreferentialAttachment(aPackage, considerNodeAge, EvolutionSimulator.loopLimit, ageRelaxation, nodeAgeToNewIncomingEdgesDistribution);
                    }
                    logger.log(Level.FINE, "Checking dest node {0}", destinationNode);
                    loopCounter++;
                    ageRelaxation++;
                    if (loopCounter > EvolutionSimulator.loopLimit) {
                        foundProperDestinationNode = false;
                        logger.log(Level.FINE, "\nLoop Counter break");
                        break;
                    }
                }
                if (foundProperDestinationNode) {
                    destinationNodes.add(destinationNode);
                    if (destinationNodes.size() >= howManyEdgesMustBeCreatedByThisNode) {
                        break;
                    }
                } else {
                    MyRandomGenerator.resetRandomGenerator();
                    internalLoopCounter++;
                    if (internalLoopCounter > EvolutionSimulator.loopLimit) {
                        break;
                    }
                    packagesThisNodeAttachedTo = new TreeSet<MyPackage>();
                    EvolutionSimulator.NEW_CLASS_COUNTER--;
                    simulatedGraph.removeVertex(newNode);
                    newNode = prepareANewNode(simulatedGraph, packagesThisNodeAttachedTo);
                    //destinationNodes = new ArrayList<MyNode>();
                    foundProperDestinationNode = true;
                    howManyEdgesMustBeCreatedByThisNode = edgesNumberThatANewNodeMustCreate.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
                    logger.log(Level.FINE, "dest node search failed, changing to another node behavior");
                }
                bigCounter++;
                if (bigCounter > EvolutionSimulator.bigCounterLimit) {
                    logger.log(Level.INFO, "Total break!");
                    break;
                }
            }
            for (MyNode destNode : destinationNodes) {
                try {
                    MyEdge myEdge = new MyEdge(newNode, destNode, "sim_New2Ex");
                    myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
                    simulatedGraph.addEdge(myEdge, newNode, destNode, EdgeType.DIRECTED);
                    GraphEventLogger.logEvent(new EdgeAdditionEvent(myEdge, newNode, destNode, EdgeType.DIRECTED));
                    logger.log(Level.FINE, "NEW EDGE FROM NEW NODE: {0} - {1}-> {2}", new Object[]{newNode, simulatedGraph.getProjectVersion(), destNode});
                    reporter.increaseEdgesFromNewClasses();
                    edgesCreatedSoFar++;
                    if (edgesCreatedSoFar >= howManyEdgesWillBeCreated) {
                        break;
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Problem while trying to add a new edge from a new class to an existing class",e);
                }

            }
        }//for
    }

}
