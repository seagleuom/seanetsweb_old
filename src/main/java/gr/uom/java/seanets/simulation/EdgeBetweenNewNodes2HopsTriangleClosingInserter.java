package gr.uom.java.seanets.simulation;

import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.event.EdgeAdditionEvent;
import gr.uom.java.seanets.graph.event.GraphEventLogger;
import gr.uom.java.seanets.graph.visualization.GraphVisualizer;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;

public class EdgeBetweenNewNodes2HopsTriangleClosingInserter extends EdgeBetweenNewNodesAbstractInserter {

    private final static Logger logger = Logger.getLogger(EdgeBetweenNewNodes2HopsTriangleClosingInserter.class.getName());

    public EdgeBetweenNewNodes2HopsTriangleClosingInserter(DistributionManager dm) {
        super(dm);

    }

    public void createEdgesBetweenNewClassesWith_2Hops_TriangleClosingModel(GraphVisualizer visualizer, double A,
            MyGraph simulatedGraph,
            Reporter reporter,
            double diameter,
            boolean preserveDomainRules,
            boolean preserveHopDistance) {

        /*We should prevent the new edge from creating huge chains of classes (many hops apart). Thus we set a limit. The new chain that would be
         * created from the new edge, must be no longer than the distance of the 90% of the class pairs of the final version of the project.
         */
        ArrayList<MyNode> newNodeList = new ArrayList<MyNode>();
        newNodeList.addAll(simulatedGraph.getVerticesOfAge(0));

        logger.log(Level.INFO, "--------------------------\nSimulation: creating edges between NEW classes");

        if (newNodeList.size() > 0) {
            int howManyEdgesWillbeCreated = newEdgeNumberDistributionOfNewNodes.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
            int edgesCreatedSoFar = 0;
            logger.log(Level.INFO, "will create {0} edges ", howManyEdgesWillbeCreated);

            while (edgesCreatedSoFar < howManyEdgesWillbeCreated) {
                MyNode sourceNode, destinationNode;
                int howManyEdgesWillbeCreatedFromSourceNode = 1;
                if (newEdgeNumberDistributionOfANewToOtherNewNodes != null) {
                    howManyEdgesWillbeCreatedFromSourceNode = newEdgeNumberDistributionOfANewToOtherNewNodes.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
                }
                if (MyRandomGenerator.getNextDoubleRandom() < A) {
                    //	logger.log(Level.INFO, "trying to create a random edge between new nodes");
                    sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
                    while (preserveDomainRules && sourceNode.isAbstractOrInterface()) {
                        sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
                    }
                    for (int i = 0; i < howManyEdgesWillbeCreatedFromSourceNode; i++) {
                        destinationNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
                        int counter = 0;
                        while (destinationNode.equals(sourceNode) || (simulatedGraph.findEdge(sourceNode, destinationNode) != null)) {
                            counter++;
                            destinationNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
                            if ((counter % newNodeList.size()) == 0) {
                                sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
                                if (counter > EvolutionSimulator.loopLimit) {
                                    break;
                                }
                            }
                        }
                        try {
                            MyEdge myEdge = new MyEdge(sourceNode, destinationNode, "sim_New2New");
                            myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
                            simulatedGraph.addEdge(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED);
                            GraphEventLogger.logEvent(new EdgeAdditionEvent(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED));

                            logger.log(Level.FINE, "NEW EDGE BTWN NEW NODES (r): {0} - {1}-> {2}", new Object[]{sourceNode, simulatedGraph.getProjectVersion(), destinationNode});

                            edgesCreatedSoFar++;
                            reporter.increaseEdgesBetweenNewClasses();
                            if (edgesCreatedSoFar >= howManyEdgesWillbeCreated) {
                                break;
                            }
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "Error while trying to create edge between random new nodes");
                        }
                    }
                } else {
                    sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
                    while (preserveDomainRules && sourceNode.isAbstractOrInterface()) {
                        sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
                    }
                    int edgesCreatedFromThisSourceNode = 0;
                    while (edgesCreatedFromThisSourceNode < howManyEdgesWillbeCreatedFromSourceNode) {
                        destinationNode = simulatedGraph.classSelectorAccordingTo_2Hops_TriangleClosingModel(newNodeList, 0);
                        int loopCounter = 0;
                        boolean shouldBreak = false, hopBreak = false;
                        while (destinationNode == null
                                || destinationNode.equals(sourceNode)
                                || destinationNode.getAge() > 0
                                || (simulatedGraph.findEdge(sourceNode, destinationNode) != null)
                                || (preserveDomainRules && sourceNode.isAbstractOrInterface())
                                || (preserveHopDistance && (hopBreak = simulatedGraph.doesEdgeExceedsMaxHopDistanceNew(sourceNode, destinationNode, diameter)))) {
                            if (destinationNode == null || sourceNode.getAge() == 0) {
                                reporter.increaseEdgesRejectedDueToNullDestination();
                            } else if (sourceNode.equals(destinationNode)) {
                                reporter.increaseEdgesRejectedDueToSameSourceDestination();
                            } else if (preserveDomainRules && sourceNode.isAbstractOrInterface()) {
                                reporter.increaseEdgesRejectedDueToDomainRules();
                            } else if (hopBreak) {
                                reporter.increaseEdgesRejectedDueToPathLength();
                            }
                            destinationNode = simulatedGraph.classSelectorAccordingTo_2Hops_TriangleClosingModel(newNodeList, 0);
                            loopCounter++;
                            if (loopCounter > EvolutionSimulator.loopLimit) {
                                //	shouldBreak = true;
                                break;
                            }
                            if (shouldBreak) {
                                break;
                            }
                        }
                        try {
                            MyEdge myEdge = new MyEdge(sourceNode, destinationNode, "sim_New2New");
                            myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
                            simulatedGraph.addEdge(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED);
                            GraphEventLogger.logEvent(new EdgeAdditionEvent(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED));

                            logger.log(Level.FINE, "NEW EDGE BTWN NEW NODES: {0} - {1}-> {2} dest Node In degree {3}", new Object[]{sourceNode, simulatedGraph.getProjectVersion(), destinationNode, simulatedGraph.inDegree(destinationNode)});

                            edgesCreatedSoFar++;
                            reporter.increaseEdgesBetweenNewClasses();

                            if (edgesCreatedSoFar >= howManyEdgesWillbeCreated || edgesCreatedFromThisSourceNode >= howManyEdgesWillbeCreatedFromSourceNode) {
                                break;
                            }
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "Error while trying to create edge between new nodes");
                        }
                    }
                }
            }
            logger.log(Level.INFO, "Edges Created: {0}", edgesCreatedSoFar);
        }
    }

}
