package gr.uom.java.seanets.simulation;

import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.statistics.DistributionFactory;
import gr.uom.java.seanets.statistics.FittingFunction;

import java.util.ArrayList;

public class EdgesBetweenRandomNewNodesInserter extends EdgeBetweenNewNodesAbstractInserter{

	public EdgesBetweenRandomNewNodesInserter(DistributionManager dm){
		super(dm);
	}
	
	public void createEdgesBetweenNewClasses(double A, MyGraph simulatedGraph, 
			Reporter reporter, boolean preserveDomainRules,
			boolean preserveHopDistance
			) {
		/*We should prevent the new edge from creating huge chains of classes (many hops apart). Thus we set a limit. The new chain that would be
		 * created from the new edge, must be no longer than the distance of the 90% of the class pairs of the final version of the project.
		 */

		ArrayList<MyNode> newNodeList = new ArrayList<MyNode>();
		newNodeList.addAll(simulatedGraph.getVerticesOfAge(0));

		System.out.println("--------------------------\nSimulation: creating edges between NEW classes");
		if ( newNodeList.size() > 0 ) {
			int howManyEdgesWillbeCreated = newEdgeNumberDistributionOfNewNodes.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
			int edgesCreatedSoFar = 0;
			System.out.println("will create "+howManyEdgesWillbeCreated+" edges ");
			while(edgesCreatedSoFar < howManyEdgesWillbeCreated){
				MyNode sourceNode, destinationNode;
				int howManyEdgesWillbeCreatedFromSourceNode = newEdgeNumberDistributionOfANewToOtherNewNodes.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
				if(MyRandomGenerator.getNextDoubleRandom() < A) {
					//	System.out.println("trying to create a random edge between new nodes");
					sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
					while(preserveDomainRules && sourceNode.isAbstractOrInterface() ) {
						sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
					}
					for(int i=0; i< howManyEdgesWillbeCreatedFromSourceNode; i++) {
						destinationNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
						int counter=0;
						while(destinationNode.equals(sourceNode) || (simulatedGraph.findEdge(sourceNode, destinationNode) != null)) {
							counter++;
							destinationNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
							if((counter % newNodeList.size()) == 0){
								sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
								if(counter > EvolutionSimulator.loopLimit) break;
							}
						}
						try{
							MyEdge myEdge = new MyEdge(sourceNode, destinationNode,"simulated");
							myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
							simulatedGraph.addEdge(myEdge,sourceNode, destinationNode,EdgeType.DIRECTED);
							//		System.out.println("NEW EDGE BTWN NEW NODES (r): "+sourceNode+" - "+simulatedGraph.getProjectVersion()+"-> "+destinationNode);
							edgesCreatedSoFar++;
							reporter.increaseEdgesBetweenNewClasses();
							if(edgesCreatedSoFar >= howManyEdgesWillbeCreated){
								break;
							}
						}
						catch(Exception e) {
							System.err.println("Error while trying to create edge between random new nodes");
						}
					}
				}
				else{
					sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
					while(preserveDomainRules && sourceNode.isAbstractOrInterface() ) {
						sourceNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
					}
					int edgesCreatedFromThisSourceNode = 0;
					while(edgesCreatedFromThisSourceNode < howManyEdgesWillbeCreatedFromSourceNode)  {
						destinationNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
						int loopCounter = 0;
						boolean shouldBreak = false;
						while(destinationNode.equals(sourceNode)|| 
								(simulatedGraph.findEdge(sourceNode, destinationNode) != null)
								|| (preserveDomainRules && sourceNode.isAbstractOrInterface())
								){
							destinationNode = newNodeList.get(MyRandomGenerator.getNextIntRandom(newNodeList.size()));
							loopCounter++;
							if(loopCounter == EvolutionSimulator.loopLimit) {
								shouldBreak = true;
								break;
							}
							//	System.out.print("/");
						}
						if(shouldBreak) break;
						try{
							MyEdge myEdge = new MyEdge(sourceNode, destinationNode,"simulated");
							myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
							simulatedGraph.addEdge(myEdge,sourceNode, destinationNode,EdgeType.DIRECTED);
							System.out.println("NEW EDGE BTWN NEW NODES: "+sourceNode+" - "+simulatedGraph.getProjectVersion()+"-> "+destinationNode+" dest Node In degree "+simulatedGraph.inDegree(destinationNode));
							edgesCreatedSoFar++;	
							reporter.increaseEdgesBetweenNewClasses();
						//	simulatedGraph.calculateDiameterAndShortestPaths(null);
							if(edgesCreatedSoFar >= howManyEdgesWillbeCreated || edgesCreatedFromThisSourceNode >= howManyEdgesWillbeCreatedFromSourceNode){
								break;
							}
						}
						catch(Exception e) {
							System.err.println("Error while trying to create edge between new nodes");
						}
					}
				}
			}
			System.out.println("Edges Created: "+edgesCreatedSoFar);
		}
	}


}
