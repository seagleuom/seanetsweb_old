package gr.uom.java.seanets.simulation;

import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.MyPackage;
import gr.uom.java.seanets.graph.event.EdgeAdditionEvent;
import gr.uom.java.seanets.graph.event.GraphEventLogger;
import gr.uom.java.seanets.graph.visualization.GraphVisualizer;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;

public class EdgeFromExistingWithPAInserter extends EdgeFromExistingNodesAbstractInserter {

    private final static Logger logger = Logger.getLogger(EdgeFromExistingWithPAInserter.class.getName());

    public EdgeFromExistingWithPAInserter(ProjectEvolutionAnalysis pa, DistributionManager dm) {
        super(pa, dm);
    }

    public void createEdgesFromExistingClassesWithPackageBehaviorDuplicationModel(
            GraphVisualizer visualizer,
            double A,
            MyGraph simulatedGraph,
            Reporter reporter,
            double diameter,
            boolean preserveDomainRules,
            boolean considerSourceNodeAge,
            boolean considerTargetNodeAge,
            boolean preserveHopDistance) {


        /* A: separator: with prob A new edges are beginning from randomly selected nodes
         * with prob 1-A new edges are beginning from nodes according to Inverse Preferential Attachment prob.
         */
        if (newEdgeNumberDistributionOfExistingNodes != null) {

            double random = MyRandomGenerator.getNextDoubleRandom();
            int howManyEdgesWillBeCreated = newEdgeNumberDistributionOfExistingNodes.getXValueForaY(random);
            logger.log(Level.INFO, "--------------------------\nSimulation: creating edges From existing classes \n Will create " + howManyEdgesWillBeCreated + " edges, random = " + random);
            int edgesCreated = 0;
            while (edgesCreated < howManyEdgesWillBeCreated) {
                MyNode sourceNode = null;
                MyNode destinationNode = null;
                int ageRelaxation = 0;
                int bigCounter = 0;
                boolean foundProperDestinationNode = true;
                ArrayList<MyNode> destinationNodes = new ArrayList<MyNode>();
                ArrayList<MyPackage> packagesThisNodeAttachedTo = new ArrayList<MyPackage>();
                sourceNode = prepareAnExistingNode(simulatedGraph, packagesThisNodeAttachedTo, considerSourceNodeAge);
                while (destinationNodes.size() < packagesThisNodeAttachedTo.size()) {
                    MyPackage aPackage = null;
                    double randomNum = MyRandomGenerator.getNextDoubleRandom();
                    int hopsThatShouldBeSpanned = getHopsToSpan(simulatedGraph);
                    if (bigCounter < EvolutionSimulator.bigCounterLimit) {
                        aPackage = packagesThisNodeAttachedTo.get(MyRandomGenerator.getNextIntRandom(packagesThisNodeAttachedTo.size()));
                        if (randomNum < A) {
                            destinationNode = simulatedGraph.getARandomNode(aPackage);
                        } else {
                            destinationNode = simulatedGraph.classSelectorAccordingToAgeAndPreferentialAttachment(aPackage, considerTargetNodeAge, EvolutionSimulator.loopLimit, ageRelaxation, nodeAgeToNewIncomingEdgesDistribution);
                        }
                        int loopCounter = 0;
                        while (destinationNode == null
                                || sourceNode.getAge() == 0
                                || sourceNode.equals(destinationNode)
                                || (simulatedGraph.findEdge(sourceNode, destinationNode) != null)
                                || (preserveDomainRules && sourceNode.isAbstractOrInterface())
                                || (preserveHopDistance && simulatedGraph.doesEdgeExceedsMaxHopDistanceNew(sourceNode, destinationNode, diameter))) {
                            if (destinationNode == null || sourceNode.getAge() == 0) {
                                reporter.increaseEdgesRejectedDueToNullDestination();
                            } else if (sourceNode.equals(destinationNode)) {
                                reporter.increaseEdgesRejectedDueToSameSourceDestination();
                            } else if (preserveDomainRules && sourceNode.isAbstractOrInterface()) {
                                reporter.increaseEdgesRejectedDueToDomainRules();
                            }
                            if (randomNum < A) {
                                destinationNode = simulatedGraph.getARandomNode(aPackage);
                            } else {
                                destinationNode = simulatedGraph.classSelectorAccordingToAgeAndPreferentialAttachment(aPackage, considerTargetNodeAge, EvolutionSimulator.loopLimit, ageRelaxation, nodeAgeToNewIncomingEdgesDistribution);
                            }
                            //	logger.log(Level.INFO, "destination node selected");
                            loopCounter++;
                            if (loopCounter > EvolutionSimulator.loopLimit) {
                                foundProperDestinationNode = false;
                                break;
                            }
                            randomNum = MyRandomGenerator.getNextDoubleRandom();
                        }
                        if (foundProperDestinationNode) {
                            destinationNodes.add(destinationNode);
                        } else {
                            logger.log(Level.INFO, "changing source node and selecting new behavior");
                            packagesThisNodeAttachedTo = new ArrayList<MyPackage>();
                            sourceNode = prepareAnExistingNode(simulatedGraph, packagesThisNodeAttachedTo, considerSourceNodeAge);
                            //destinationNodes = new ArrayList<MyNode>();
                            foundProperDestinationNode = true;
                            bigCounter++;
                        }
                    } else {
                        bigCounter = -1;
                        break;
                    }
                }
                if (bigCounter == -1) {
                    logger.log(Level.INFO, "\n  Aborted  \n");
                    break;
                }
                for (MyNode destNode : destinationNodes) {
                    try {
                        MyEdge myEdge = new MyEdge(sourceNode, destinationNode, "sim_FromEx");
                        myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
                        simulatedGraph.addEdge(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED);
                        GraphEventLogger.logEvent(new EdgeAdditionEvent(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED));
                        logger.log(Level.INFO, "NEW EDGE From Existing Node: {0} - {1}-> {2} dest Node In degree {3}", new Object[]{sourceNode, simulatedGraph.getProjectVersion(), destNode, simulatedGraph.inDegree(destNode)});
                        edgesCreated++;
                        reporter.increaseEdgesFromExistingClasses();
                    } catch (Exception e) {
                        System.err.println("\nProblem while trying to add a new edge between existing classes");
                    }
                }
            }
            logger.log(Level.INFO, "Edges Created: {0}", edgesCreated);
        } else {
            logger.log(Level.SEVERE, "Not enough data to simulate edges between existing nodes");
        }
    }

    public void createEdgesFromExistingClassesWithPA_Model_NoPackages(
            GraphVisualizer visualizer, double A, 
            MyGraph simulatedGraph,
            Reporter reporter, 
            double diameter,
            boolean preserveDomainRules,
            boolean considerSourceNodeAge,
            boolean considerTargetNodeAge,
            boolean preserveHopDistance
            ) {

        /* A: separator: with prob A new edges are beginning from randomly selected nodes
         * with prob 1-A new edges are beginning from nodes according to Inverse Preferential Attachment prob.
         */

        if (newEdgeNumberDistributionOfExistingNodes != null) {
            double random = MyRandomGenerator.getNextDoubleRandom();
            int howManyEdgesWillBeCreated = newEdgeNumberDistributionOfExistingNodes.getXValueForaY(random);
            logger.log(Level.INFO, "--------------------------\nSimulation: creating edges From existing classes \n Will create {0} edges, random = {1}", new Object[]{howManyEdgesWillBeCreated, random});
            int edgesCreated = 0;
            while (edgesCreated < howManyEdgesWillBeCreated) {
                MyNode sourceNode = null;
                MyNode destinationNode = null;
                int ageRelaxation = 0;
                int bigCounter = 0;
                boolean foundProperDestinationNode = true;
                ArrayList<MyNode> destinationNodes = new ArrayList<MyNode>();
                ArrayList<MyPackage> packagesThisNodeAttachedTo = new ArrayList<MyPackage>();
                sourceNode = prepareAnExistingNode(simulatedGraph, packagesThisNodeAttachedTo, considerSourceNodeAge);
                int howManyEdgesWillBeCreatedFromThisNode = newEdgeNumberDistributionOfExistingNodes.getXValueForaY(random);
                if (howManyEdgesWillBeCreatedFromThisNode == 0) {
                    howManyEdgesWillBeCreatedFromThisNode = 2;
                }
                logger.log(Level.INFO, "Selected source node {0}. Will create {1} nodes", new Object[]{sourceNode, howManyEdgesWillBeCreatedFromThisNode});
                while (destinationNodes.size() < howManyEdgesWillBeCreatedFromThisNode) {
                    MyPackage aPackage = null;
                    double randomNum = MyRandomGenerator.getNextDoubleRandom();

                    if (bigCounter < EvolutionSimulator.bigCounterLimit) {
                        if (randomNum < A) {
                            destinationNode = simulatedGraph.getARandomNode();
                        } else {
                            destinationNode = simulatedGraph.classSelectorAccordingToAgeAndPreferentialAttachment(null, considerTargetNodeAge, EvolutionSimulator.loopLimit, ageRelaxation, nodeAgeToNewIncomingEdgesDistribution);
                        }
                        int loopCounter = 0;
                        boolean hopBreak = false;
                        while (destinationNode == null
                                || sourceNode.getAge() == 0
                                || sourceNode.equals(destinationNode)
                                || (simulatedGraph.findEdge(sourceNode, destinationNode) != null)
                                || (preserveDomainRules && sourceNode.isAbstractOrInterface())
                                || (preserveHopDistance && (hopBreak = simulatedGraph.doesEdgeExceedsMaxHopDistanceNew(sourceNode, destinationNode, diameter)))) {
                            if (destinationNode == null || sourceNode.getAge() == 0) {
                                reporter.increaseEdgesRejectedDueToNullDestination();
                            } else if (sourceNode.equals(destinationNode)) {
                                reporter.increaseEdgesRejectedDueToSameSourceDestination();
                            } else if (preserveDomainRules && sourceNode.isAbstractOrInterface()) {
                                reporter.increaseEdgesRejectedDueToDomainRules();
                            } else if (hopBreak) {
                                reporter.increaseEdgesRejectedDueToPathLength();
                            }
                            if (randomNum < A) {
                                destinationNode = simulatedGraph.getARandomNode();
                            } else {
                                destinationNode = simulatedGraph.classSelectorAccordingToAgeAndPreferentialAttachment(null, considerTargetNodeAge, EvolutionSimulator.loopLimit, ageRelaxation, nodeAgeToNewIncomingEdgesDistribution);
                            }
                            //	logger.log(Level.INFO, "destination node selected");
                            loopCounter++;
                            ageRelaxation++;
                            if (loopCounter > EvolutionSimulator.loopLimit) {
                                foundProperDestinationNode = false;
                                break;
                            }
                            randomNum = MyRandomGenerator.getNextDoubleRandom();
                        }
                        if (foundProperDestinationNode) {
                            destinationNodes.add(destinationNode);
                        } else {
                            logger.log(Level.INFO, "changing source node and selecting new behavior");
                            packagesThisNodeAttachedTo = new ArrayList<MyPackage>();
                            sourceNode = prepareAnExistingNode(simulatedGraph, packagesThisNodeAttachedTo, considerSourceNodeAge);
                            foundProperDestinationNode = true;
                            bigCounter++;
                        }
                    } else {
                        bigCounter = -1;
                        break;
                    }
                }
                if (bigCounter == -1) {
                    logger.log(Level.INFO, "\n  Aborted  \n");
                    break;
                }
                for (MyNode destNode : destinationNodes) {
                    try {
                        MyEdge myEdge = new MyEdge(sourceNode, destinationNode, "sim_FromEx");
                        myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
                        simulatedGraph.addEdge(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED);
                        GraphEventLogger.logEvent(new EdgeAdditionEvent(myEdge, sourceNode, destinationNode, EdgeType.DIRECTED));
                        logger.log(Level.FINE, "NEW EDGE From Existing Node: {0} - {1}-> {2} dest Node In degree {3}", new Object[]{sourceNode, simulatedGraph.getProjectVersion(), destNode, simulatedGraph.inDegree(destNode)});
                        edgesCreated++;
                        reporter.increaseEdgesFromExistingClasses();
                    } catch (Exception e) {
                        System.err.println("\nProblem while trying to add a new edge between existing classes");
                    }
                }
            }
            logger.log(Level.INFO, "Edges Created: {0}", edgesCreated);
        } else {
            logger.log(Level.SEVERE, "Not enough data to simulate edges between existing nodes");
        }
    }
}
