package gr.uom.java.seanets.simulation;

import java.util.ArrayList;

import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.MyPackage;
import gr.uom.java.seanets.history.NodeBehavior;
import gr.uom.java.seanets.history.NodeBehaviorHistory;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.statistics.FittingFunction;

public abstract class EdgeFromExistingNodesAbstractInserter {
	
	private NodeBehaviorHistory existingNodeBehaviors;
	protected ProjectEvolutionAnalysis projectEvolutionAnalysis;
	
	protected FittingFunction hopsThatNewEdgeSpannedDistribution;
	protected FittingFunction ageOfSourceNodeThatCreatesOutgoingEdgeDistribution;
	protected FittingFunction newEdgeNumberDistributionOfExistingNodes;
	protected FittingFunction nodeAgeToNewIncomingEdgesDistribution;

	
	protected EdgeFromExistingNodesAbstractInserter(ProjectEvolutionAnalysis pa, DistributionManager dm){
		projectEvolutionAnalysis = pa;
		existingNodeBehaviors = projectEvolutionAnalysis.getExistingNodeBehaviors();
		hopsThatNewEdgeSpannedDistribution = dm.getHopsThatNewEdgeSpannedDistribution();
		newEdgeNumberDistributionOfExistingNodes = dm.getNewEdgeNumberDistributionOfExistingNodes();
		nodeAgeToNewIncomingEdgesDistribution = dm.getNodeAgeToNewIncomingEdgesDistribution();
		ageOfSourceNodeThatCreatesOutgoingEdgeDistribution = dm.getAgeOfSourceNodeThatCreatesOutgoingEdgeDistribution();
	}
	
	protected MyNode prepareAnExistingNode(MyGraph aGraph, ArrayList<MyPackage> packagesThisNodeAttachedTo, boolean considerNodeAge){

		NodeBehavior nodeBehavior = existingNodeBehaviors.getANodeBehavior(ageOfSourceNodeThatCreatesOutgoingEdgeDistribution, 0, considerNodeAge);
		if(nodeBehavior.getNode() != null) {
			packagesThisNodeAttachedTo.addAll(nodeBehavior.getPackagesAttached());
			while(!aGraph.containsVertex(nodeBehavior.getNode())){
				nodeBehavior = existingNodeBehaviors.getANodeBehavior(ageOfSourceNodeThatCreatesOutgoingEdgeDistribution, 0, considerNodeAge);
				packagesThisNodeAttachedTo.addAll(nodeBehavior.getPackagesAttached());
			}
			//	System.out.println("Node Behavior Selected to Duplicate:\n"+nodeBehavior);
			return nodeBehavior.getNode();
		}
		else
			return null;
	}
	
	protected int getHopsToSpan(MyGraph simulatedGraph) {
		if(hopsThatNewEdgeSpannedDistribution != null) {
			int hopsThatShouldBeSpanned = hopsThatNewEdgeSpannedDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
			while(hopsThatShouldBeSpanned == 1 ){
				hopsThatShouldBeSpanned = hopsThatNewEdgeSpannedDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
			}
			return hopsThatShouldBeSpanned;
		}
		else{
			// case where the hopsThatNewEdgeSpannedDistribution is null.
			//This means that the project did not have enough data to generate the distribution
			//So we consider that every new edge should be between not previously connected nodes (previous distance = 0) 
			return 0; 
		}
	}



}
