package gr.uom.java.seanets.simulation;

import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.MyPackage;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.statistics.DistributionFactory;
import gr.uom.java.seanets.statistics.FittingFunction;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewClassInserterPrefAttachOnly {
	
	private final ProjectEvolutionAnalysis projectEvolutionAnalysis;
	private final FittingFunction edgesNumberThatANewNodeMustCreate;
	private final int loopLimit;
	private final static Logger logger = Logger.getLogger(NewClassInserterPrefAttachOnly.class.getName());
	
	public NewClassInserterPrefAttachOnly(ProjectEvolutionAnalysis pa, DistributionManager distributionManager){
		projectEvolutionAnalysis = pa;
		edgesNumberThatANewNodeMustCreate = distributionManager.getNewEdgeNumberDistributionOfNewNodes();
		loopLimit = EvolutionSimulator.loopLimit;
	}
	
	public void insertNewClassesBarabasiClassicPA(double A,MyGraph simulatedGraph, Reporter reporter0) {
		simulatedGraph.calculateGlobalPreferentialAttachment();
		logger.log(Level.INFO, "--------------------------\n" +"Simulation: inserting new Classes Barabasi Classic PA");
		//	int howManyClassesWillBeInserted = newNodesNumberThatMustBeInserted.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
		int howManyClassesWillBeInserted = projectEvolutionAnalysis.getAverageNumberOfNewClassesInAllVersions();
		if(howManyClassesWillBeInserted > 0) {
			for(int i=0; i< howManyClassesWillBeInserted; i++) {
				MyNode destinationNode = null;
				ArrayList<MyNode> destinationNodes = new ArrayList<MyNode>();
				int howManyEdgesMustBeCreatedByThisNode = edgesNumberThatANewNodeMustCreate.getIntValue(MyRandomGenerator.getNextDoubleRandom());
				int edgesCreatedSoFar = 0;
				while(destinationNodes.size() < howManyEdgesMustBeCreatedByThisNode) {
					boolean foundProperDestinationNode = true;
					double randomNum = MyRandomGenerator.getNextDoubleRandom();
					MyPackage aPackage = null;
					if(randomNum < A){
						destinationNode = simulatedGraph.getARandomNode();
					}
					else{
						destinationNode = simulatedGraph.classSelectorAccordingToAgeAndPreferentialAttachment(aPackage, false, loopLimit, 0, null);
					}
					int loopCounter = 0;
					while(destinationNode == null || destinationNodes.contains(destinationNode)){
						if(randomNum < A){
							destinationNode = simulatedGraph.getARandomNode();
						}
						else{
							destinationNode = simulatedGraph.classSelectorAccordingToAgeAndPreferentialAttachment(aPackage, false, loopLimit, 0, null);					}
						loopCounter++;
						if(loopCounter > loopLimit) {
							foundProperDestinationNode = false;
							break;
						}
					}
					if(foundProperDestinationNode) {
						destinationNodes.add(destinationNode);
						if(destinationNodes.size() > howManyEdgesMustBeCreatedByThisNode){
							break;
						}
					}
				}
				if(destinationNodes.size() > 0) {
					String newClassName = "NewClass_"+EvolutionSimulator.NEW_CLASS_COUNTER;	EvolutionSimulator.NEW_CLASS_COUNTER++;
					MyNode newNode = new MyNode(newClassName, new MyPackage("newPackage"), simulatedGraph);
					simulatedGraph.addVertex(newNode);
					for(MyNode destNode : destinationNodes) {
						try{
							MyEdge myEdge = new MyEdge(newNode, destNode,"simulated");
							myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
							simulatedGraph.addEdge(myEdge,newNode, destNode,EdgeType.DIRECTED);	
							logger.log(Level.FINE, "New Class Edge: {0} -> {1}", new Object[]{newNode, destNode});
							edgesCreatedSoFar ++;
							reporter0.increaseEdgesFromNewClasses();
							if(edgesCreatedSoFar >= howManyEdgesMustBeCreatedByThisNode){
								break;
							}
						}catch(Exception e){
							logger.log(Level.SEVERE, "Problem while trying to add a new edge from a new class to an existing class");
						}
					}
				}
			}//for
		}//if
	}


}
