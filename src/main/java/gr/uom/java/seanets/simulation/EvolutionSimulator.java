package gr.uom.java.seanets.simulation;

import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.event.GraphEventLogger;
import gr.uom.java.seanets.graph.visualization.GraphVisualizer;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.db.persistence.Version;
import gr.uom.java.seanets.statistics.MyScatterPlotChart;
import gr.uom.java.seanets.statistics.MyScatterPlotChartWithPowerFitting;
import gr.uom.java.seanets.util.SEANetsProperties;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
@LocalBean
public class EvolutionSimulator {

    private ProjectEvolutionAnalysis projectEvolutionAnalysis;
    private Map<Version, MyGraph> projectGraphSimulationOfEvolution0, projectGraphSimulationOfEvolution1;
    private Map<Version, MyGraph> projectGraphSimulationOfEvolution2, projectGraphSimulationOfEvolution3, projectGraphSimulationOfEvolution4, projectGraphSimulationOfEvolution5;
    private SimulationResults simulationResults;
    private MyScatterPlotChartWithPowerFitting chartInDegreeVScc;
    private MyScatterPlotChartWithPowerFitting chartInDegreeVScount, chartOutDegreeVScount;
    private MyScatterPlotChart chartHopsOverTime, chartHopsOverTimeCumul;
    private Map<Version, MyGraph> projectGraphEvolution;
    private MyScatterPlotChart chartInDegreeVSOutDegree;

    private final static Logger logger = Logger.getLogger(EvolutionSimulator.class.getName());

    private MyGraph finalGraph = null;
    private MyGraph baseGraph = null;
    private MyGraph lastGraph0 = null;

    private MyGraph lastGraph5 = null;

    private Reporter reporter0 = new Reporter();
    private Reporter reporter5 = new Reporter();

    public static final int loopLimit = 30;
    public static final int bigCounterLimit = 30;
    public static int NEW_CLASS_COUNTER = 0;

    private GraphVisualizer visualizer;

    private DistributionManager distributionManager;
    private NewClassInserterFullModel newNodeInserter;
    private NewClassInserterPrefAttachOnly newNodeInserterPAOnly;
    private EdgeFromExistingWithPAInserter edgesFromExistingNodes_PA_ModelInserter;
    private EdgeBetweenNewNodes2HopsTriangleClosingInserter edgesBetweenNewNodes_2Hop_TriangleClosingModelInserter;
    private EdgeDeleter edgeDeleter;
    private StringBuilder resultHeader;

    private String projectName;

    public EvolutionSimulator() {
    }

    public void initializeSimulator(ProjectEvolutionAnalysis pev, String projectName) {
        this.projectName = projectName;
        projectEvolutionAnalysis = pev;
        distributionManager = new DistributionManager(projectEvolutionAnalysis, projectName);

        newNodeInserter = new NewClassInserterFullModel(projectEvolutionAnalysis, distributionManager);
        newNodeInserterPAOnly = new NewClassInserterPrefAttachOnly(projectEvolutionAnalysis, distributionManager);
        edgesFromExistingNodes_PA_ModelInserter = new EdgeFromExistingWithPAInserter(projectEvolutionAnalysis, distributionManager);
        edgesBetweenNewNodes_2Hop_TriangleClosingModelInserter = new EdgeBetweenNewNodes2HopsTriangleClosingInserter(distributionManager);
        edgeDeleter = new EdgeDeleter(distributionManager);

        projectGraphSimulationOfEvolution0 = new LinkedHashMap<Version, MyGraph>();
        projectGraphSimulationOfEvolution1 = new LinkedHashMap<Version, MyGraph>();
        projectGraphSimulationOfEvolution2 = new LinkedHashMap<Version, MyGraph>();
        projectGraphSimulationOfEvolution3 = new LinkedHashMap<Version, MyGraph>();
        projectGraphSimulationOfEvolution4 = new LinkedHashMap<Version, MyGraph>();
        projectGraphSimulationOfEvolution5 = new LinkedHashMap<Version, MyGraph>();

        projectGraphEvolution = projectEvolutionAnalysis.getProjectGraphEvolution();

        chartInDegreeVScc = new MyScatterPlotChartWithPowerFitting(projectName, "In Degree", "Clustering Coefficient");
        chartInDegreeVScount = new MyScatterPlotChartWithPowerFitting(projectName, "In Degree", "Count");
        chartOutDegreeVScount = new MyScatterPlotChartWithPowerFitting(projectName, "Out Degree", "Count");
        chartHopsOverTime = new MyScatterPlotChart(projectName, "Distance in hops", "Count of class pairs", true);
        chartHopsOverTimeCumul = new MyScatterPlotChart(projectName, "Distance in hops", "Cumul. Count of class pairs", true);
        chartInDegreeVSOutDegree = new MyScatterPlotChart(projectName, "In Degree", "Out Degree", false);

        int versions = projectGraphEvolution.keySet().size();
        double estimationCutOff = SEANetsProperties.getInstance().getCutOffValue();
        int versionToStartSimulation = (int) Math.floor(versions  *  estimationCutOff) + 1;
        int counter = 1;

        for (Version projectVersion : projectGraphEvolution.keySet()) {
            if (counter < versionToStartSimulation) {
                projectGraphSimulationOfEvolution0.put(projectVersion, projectGraphEvolution.get(projectVersion));
                projectGraphSimulationOfEvolution1.put(projectVersion, projectGraphEvolution.get(projectVersion));
                projectGraphSimulationOfEvolution2.put(projectVersion, projectGraphEvolution.get(projectVersion));
                projectGraphSimulationOfEvolution3.put(projectVersion, projectGraphEvolution.get(projectVersion));
                projectGraphSimulationOfEvolution4.put(projectVersion, projectGraphEvolution.get(projectVersion));
                projectGraphSimulationOfEvolution5.put(projectVersion, projectGraphEvolution.get(projectVersion));
                baseGraph = projectGraphEvolution.get(projectVersion);
            } else {
                projectGraphSimulationOfEvolution0.put(projectVersion, null);
                projectGraphSimulationOfEvolution1.put(projectVersion, null);
                projectGraphSimulationOfEvolution2.put(projectVersion, null);
                projectGraphSimulationOfEvolution3.put(projectVersion, null);
                projectGraphSimulationOfEvolution4.put(projectVersion, null);
                projectGraphSimulationOfEvolution5.put(projectVersion, null);
            }
            counter++;
            finalGraph = projectGraphEvolution.get(projectVersion);
        }
        //	visualizer = new GraphVisualizer(baseGraph);
        new MyRandomGenerator(finalGraph.getVertexCount());
        new GraphEventLogger();

        String baseGraphDescr = baseGraph + "_BaseGraph_REAL";
        String finalGraphDescr = finalGraph + "_FINAL_REAL";

        chartInDegreeVScc.addData(baseGraphDescr, baseGraph.getInDegreeVSClusteringCoefficientDataset(false));
        chartInDegreeVScc.addData(finalGraphDescr, finalGraph.getInDegreeVSClusteringCoefficientDataset(false));

        chartInDegreeVScount.addData(baseGraphDescr, baseGraph.getInDegreeVSCountDataset());
        chartInDegreeVScount.addData(finalGraphDescr, finalGraph.getInDegreeVSCountDataset());

        chartOutDegreeVScount.addData(baseGraphDescr, baseGraph.getOutDegreeVSCountDataset());
        chartOutDegreeVScount.addData(finalGraphDescr, finalGraph.getOutDegreeVSCountDataset());

        chartInDegreeVSOutDegree.addData(baseGraphDescr, baseGraph.getInDegreeVSOutDegreeDataset());
        chartInDegreeVSOutDegree.addData(finalGraphDescr, finalGraph.getInDegreeVSOutDegreeDataset());

        chartHopsOverTime.addData(baseGraphDescr, baseGraph.getHopsFrequenciesDataset());
        chartHopsOverTime.addData(finalGraphDescr, finalGraph.getHopsFrequenciesDataset());

        chartHopsOverTimeCumul.addData(baseGraphDescr, baseGraph.getCumulativePercentageOfClassesThatAreHopsApart());
        chartHopsOverTimeCumul.addData(finalGraphDescr, finalGraph.getCumulativePercentageOfClassesThatAreHopsApart());

    }

    public void simulationOfEvolution(double A, IProgressMonitor monitor,
            boolean applyDomainSpecificRules,
            boolean applyAgeRules,
            boolean preserveHopDistance) {

        Version previousVersion = null;
        resultHeader = new StringBuilder("Simulating the evolution of project ").append(projectName).append("\n");
        resultHeader.append("=================================================================").append("\n");
        resultHeader.append("Starting simulation, \nRandomness = ").append(A).append("\n");
        resultHeader.append("Cut off percentage = ").append(SEANetsProperties.getInstance().getCutOffValue()).append("\n");
        resultHeader.append("Version to Start simulation: ").append(baseGraph.getProjectVersion()).append("\n");
        resultHeader.append("Domain Rules Preservation: ").append(applyDomainSpecificRules).append("\n");
        resultHeader.append("Preserve Hop distance history ").append(preserveHopDistance).append("\n");
        resultHeader.append("=================================================================").append("\n");

        
        for (Version projectVersion : projectGraphSimulationOfEvolution1.keySet()) {
            if (monitor != null && monitor.isCanceled()) {
                throw new OperationCanceledException();
            }
            if (projectGraphSimulationOfEvolution1.get(projectVersion) == null) {
                
                logger.log(Level.INFO, resultHeader.toString());
                logger.log(Level.INFO, "=================================================================");
                logger.log(Level.INFO, "Simulating version {0}", projectVersion);
                logger.log(Level.INFO, "=================================================================");

                /*
                 * STEP 0: 
                 * new edges from new nodes only with Barabasi Classic PA
                 */
                MyGraph simulatedGraph0 = new MyGraph();
                MyGraph previousGraph0 = projectGraphSimulationOfEvolution0.get(previousVersion);
                simulatedGraph0 = previousGraph0.cloneGraph();
                simulatedGraph0.updateNodeAges(previousGraph0);
                Version newProjectVersion = projectVersion.clone();
                newProjectVersion.appendToVersionName("_sim_PA");
                simulatedGraph0.setProjectVersion(newProjectVersion);
                logger.log(Level.INFO, "---------STEP 0-------\nVersion: {0}", simulatedGraph0.getProjectVersion());

                newNodeInserterPAOnly.insertNewClassesBarabasiClassicPA(A, simulatedGraph0, reporter0);
                
                simulatedGraph0.calculateMetrics();
                lastGraph0 = simulatedGraph0;
                projectGraphSimulationOfEvolution0.put(projectVersion, simulatedGraph0);

                resetCountersGeneratorsAndLists();

                MyGraph simulatedGraph5 = new MyGraph();
                MyGraph previousGraph5 = projectGraphSimulationOfEvolution5.get(previousVersion);

                simulatedGraph5 = previousGraph5.cloneGraph();
                Version newProjectVersionFull = projectVersion.clone();
                newProjectVersionFull.appendToVersionName("_Sim_Full");
                simulatedGraph5.setProjectVersion(newProjectVersionFull);
                logger.log(Level.INFO, "---------STEP 5-------\nVersion: {0}", simulatedGraph5.getProjectVersion());
                simulatedGraph5.updateNodeAges(previousGraph5);

                simulatedGraph5.initializeDijskstraDistance();
                this.newNodeInserter.insertNewNodes(visualizer, A, simulatedGraph5, reporter5, previousGraph5.getDiameter(), applyAgeRules, preserveHopDistance);
                this.edgesFromExistingNodes_PA_ModelInserter.
                        createEdgesFromExistingClassesWithPackageBehaviorDuplicationModel(visualizer, A, 
                                simulatedGraph5, 
                                reporter5, 
                                previousGraph5.getDiameter(), 
                                applyDomainSpecificRules,
                                applyAgeRules,
                                applyAgeRules,
                                preserveHopDistance);
                
                this.edgesBetweenNewNodes_2Hop_TriangleClosingModelInserter.
                        createEdgesBetweenNewClassesWith_2Hops_TriangleClosingModel(visualizer, 
                                A, 
                                simulatedGraph5, 
                                reporter5, 
                                previousGraph5.getDiameter(),
                                applyDomainSpecificRules,
                                preserveHopDistance);
                
            //    this.edgeDeleter.deleteEdges(visualizer,A, simulatedGraph5, reporter5);
                simulatedGraph5.calculateMetrics();
                lastGraph5 = simulatedGraph5;
                projectGraphSimulationOfEvolution5.put(projectVersion, simulatedGraph5);
            }
            previousVersion = projectVersion;
            if (monitor != null) {
                monitor.worked(1);
            }
        }

        /*		logger.log(Level.INFO, "----Top ten classes with Barabasi Classic Only:-----");
         Reporter.printTopTenInDegreeClasses(finalGraph, lastGraph0);
         Reporter.printTopTenOutDegreeClasses(finalGraph, lastGraph0);
		
         logger.log(Level.INFO, "----Top ten classes with Simulation 5-----");
         Reporter.printTopTenInDegreeClasses(finalGraph, lastGraph5);
         Reporter.printTopTenOutDegreeClasses(finalGraph, lastGraph5);
         */
        
        reporter0.printReport("sim0");
        reporter5.printReport("sim5");

        lastGraph0.calculateDiameterAndShortestPaths();
        lastGraph5.calculateDiameterAndShortestPaths();

        baseGraph.calculateMetrics();
        lastGraph0.calculateMetrics();
        finalGraph.calculateMetrics();
        lastGraph5.calculateMetrics();

        baseGraph.printGraphBasicProperties();
        finalGraph.printGraphBasicProperties();
        lastGraph0.printGraphBasicProperties();
        lastGraph5.printGraphBasicProperties();

        String lastGraphDescr = "" + lastGraph0.getProjectVersion();
        chartHopsOverTime.addData(lastGraphDescr, lastGraph0.getHopsFrequenciesDataset());
        chartHopsOverTimeCumul.addData(lastGraphDescr, lastGraph0.getCumulativePercentageOfClassesThatAreHopsApart());
        chartInDegreeVScc.addData(lastGraphDescr, lastGraph0.getInDegreeVSClusteringCoefficientDataset(false));
        chartInDegreeVScount.addData(lastGraphDescr, lastGraph0.getInDegreeVSCountDataset());
        chartOutDegreeVScount.addData(lastGraphDescr, lastGraph0.getOutDegreeVSCountDataset());
        chartInDegreeVSOutDegree.addData(lastGraphDescr, lastGraph0.getInDegreeVSOutDegreeDataset());

        String lastGraphFULLDescr = "" + lastGraph5.getProjectVersion();
        chartInDegreeVScc.addData(lastGraphFULLDescr, lastGraph5.getInDegreeVSClusteringCoefficientDataset(false));
        chartInDegreeVScount.addData(lastGraphFULLDescr, lastGraph5.getInDegreeVSCountDataset());
        chartOutDegreeVScount.addData(lastGraphFULLDescr, lastGraph5.getOutDegreeVSCountDataset());
        chartInDegreeVSOutDegree.addData(lastGraphFULLDescr, lastGraph5.getInDegreeVSOutDegreeDataset());
        chartHopsOverTime.addData(lastGraphFULLDescr, lastGraph5.getHopsFrequenciesDataset());
        chartHopsOverTimeCumul.addData(lastGraphFULLDescr, lastGraph5.getCumulativePercentageOfClassesThatAreHopsApart());

        simulationResults = new SimulationResults(baseGraph, finalGraph, lastGraph0, lastGraph5);
        simulationResults.setResultsHeader(resultHeader.toString());
        
        simulationResults.setProjectGraphEvolution(projectGraphSimulationOfEvolution5);
        simulationResults.setProjectGraphPASimulationEvolution(projectGraphSimulationOfEvolution0);

        simulationResults.saveSimulationResultsLocally(projectName);

        simulationResults.printGraphDistributionMetrics();

        simulationResults.printGraphDistributionMetricsToFile(projectName);
        if (monitor != null) {
            monitor.done();
        }
    }

    private void resetCountersGeneratorsAndLists() {
        MyRandomGenerator.resetRandomGenerator();
    }

    private void testRandomGenerator() {
        logger.log(Level.INFO, "Generating Randoms in [0,1]");
        long limit = 1000000;
        double mean = 0;
        for (long i = 0; i < limit; i++) {
            double random = MyRandomGenerator.getNextDoubleRandom();
            mean += random;
        }
        logger.log(Level.INFO, "Mean = " + (mean / limit));

        logger.log(Level.INFO, "Generating integer Randoms in [0, 149]");
        double mean2 = 0;
        for (long i = 0; i < limit; i++) {
            double random = MyRandomGenerator.getNextIntRandom(150);
            mean2 += random;
        }
        logger.log(Level.INFO, "Mean = " + (mean2 / limit));
    }

    public SimulationResults getSimulationResults() {
        return simulationResults;
    }

    public GraphVisualizer getVisualizer() {
        return visualizer;
    }

}
