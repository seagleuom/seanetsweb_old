package gr.uom.java.seanets.simulation;

import java.util.List;

import edu.uci.ics.jung.graph.Graph;

import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.event.GraphEventLogger;
import gr.uom.java.seanets.graph.event.NodeRemovalEvent;
import gr.uom.java.seanets.statistics.FittingFunction;
import gr.uom.java.seanets.graph.event.EdgeRemovalEvent;
import gr.uom.java.seanets.graph.visualization.GraphVisualizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EdgeDeleter {

    private final FittingFunction deletedEdgesNumberDistribution;
    private final FittingFunction deletedEdgesSourceNodeOutDegreeDistribution;
    private final FittingFunction deletedEdgesTargetNodeInDegreeDistribution;
    private final static Logger logger = Logger.getLogger(EdgeDeleter.class.getName());

    public EdgeDeleter(DistributionManager dm) {
        deletedEdgesNumberDistribution = dm.getDeletedEdgesNumberDistribution();
        deletedEdgesSourceNodeOutDegreeDistribution = dm.getDeletedEdgesSourceNodeOutDegreeDistribution();
        deletedEdgesTargetNodeInDegreeDistribution = dm.getDeletedEdgesTargetNodeInDegreeDistribution();
    }

    private void checkIfNodesShouldBeDeleted(MyGraph graph, MyNode sourceNode, MyNode destinationNode) {
        if (graph.getNeighboorhood(sourceNode) == null) {
            graph.removeVertex(sourceNode);
            GraphEventLogger.logEvent(new NodeRemovalEvent(sourceNode));
        }
        if (graph.getNeighboorhood(destinationNode) == null) {
            graph.removeVertex(destinationNode);
            GraphEventLogger.logEvent(new NodeRemovalEvent(destinationNode));
        }
    }

    public void deleteEdges(GraphVisualizer visualizer, double A, MyGraph simulatedGraph, Reporter reporter) {
        logger.log(Level.INFO, "--------------------------\nSimulation: Deleting edges between existing classes");
        List<MyNode> existingNodes = simulatedGraph.getExistingNodes();
        int howManyEdgesWillbeDeleted = 0;
        if (deletedEdgesNumberDistribution != null) {
            howManyEdgesWillbeDeleted = deletedEdgesNumberDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
            logger.log(Level.INFO, "will delete {0} edges ", howManyEdgesWillbeDeleted);
            int edgesDeletedSoFar = 0;
            while (edgesDeletedSoFar < howManyEdgesWillbeDeleted) {
                MyNode sourceNode, destinationNode;
                if (MyRandomGenerator.getNextDoubleRandom() < A) {
                    sourceNode = existingNodes.get(MyRandomGenerator.getNextIntRandom(existingNodes.size()));
                    destinationNode = existingNodes.get(MyRandomGenerator.getNextIntRandom(existingNodes.size()));
                    int counter = 0;
                    while (sourceNode == null || destinationNode == null || simulatedGraph.outDegree(sourceNode) == 0
                            || simulatedGraph.inDegree(destinationNode) == 0
                            || destinationNode.equals(sourceNode)
                            || simulatedGraph.findEdge(sourceNode, destinationNode) == null) {
                        counter++;
                        sourceNode = existingNodes.get(MyRandomGenerator.getNextIntRandom(existingNodes.size()));
                        destinationNode = existingNodes.get(MyRandomGenerator.getNextIntRandom(existingNodes.size()));
                        if (counter > EvolutionSimulator.loopLimit) {
                            break;
                        }
                    }
                    try {
                        MyEdge edgeToBeRemoved = simulatedGraph.findEdge(sourceNode, destinationNode);
                        simulatedGraph.removeEdge(edgeToBeRemoved);
                        logger.log(Level.FINE, "EDGE DELETED (r): " + sourceNode + " - " + simulatedGraph.getProjectVersion() + "-> " + destinationNode);
                        checkIfNodesShouldBeDeleted(simulatedGraph, sourceNode, destinationNode);
                        GraphEventLogger.logEvent(new EdgeRemovalEvent(edgeToBeRemoved));

                        edgesDeletedSoFar++;
                        reporter.increaseDeletedEdges();
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, "Error while trying to delete edge between random nodes");
                    }
                } else {
                    int sourceOutDegree = deletedEdgesSourceNodeOutDegreeDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
                    int destinationInDegree = deletedEdgesTargetNodeInDegreeDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
                    sourceNode = simulatedGraph.getARandomNodeWithOutDegree(sourceOutDegree);
                    destinationNode = simulatedGraph.getARandomNodeWithInDegree(destinationInDegree);

                    int loopCounter = 0;
                    while (sourceNode == null || destinationNode == null || simulatedGraph.outDegree(sourceNode) == 0
                            || simulatedGraph.inDegree(destinationNode) == 0
                            || destinationNode.equals(sourceNode)
                            || simulatedGraph.findEdge(sourceNode, destinationNode) == null) {
                        sourceOutDegree = deletedEdgesSourceNodeOutDegreeDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
                        destinationInDegree = deletedEdgesTargetNodeInDegreeDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
                        sourceNode = simulatedGraph.getARandomNodeWithOutDegree(sourceOutDegree);
                        destinationNode = simulatedGraph.getARandomNodeWithInDegree(destinationInDegree);
                        loopCounter++;
                        if (loopCounter > EvolutionSimulator.loopLimit * 100) {
                            break;
                        }
                    }
                    try {
                        MyEdge edgeToBeRemoved = simulatedGraph.findEdge(sourceNode, destinationNode);
                        logger.log(Level.FINE, "\nEDGE DELETED: " + sourceNode.getName() + " d- =" + simulatedGraph.outDegree(sourceNode) + " - " + simulatedGraph.getProjectVersion() + "-> " + destinationNode.getName() + " d+ =" + simulatedGraph.inDegree(destinationNode));
                        simulatedGraph.removeEdge(simulatedGraph.findEdge(sourceNode, destinationNode));
                        checkIfNodesShouldBeDeleted(simulatedGraph, sourceNode, destinationNode);

                        GraphEventLogger.logEvent(new EdgeRemovalEvent(edgeToBeRemoved));

                        edgesDeletedSoFar++;
                        reporter.increaseDeletedEdges();
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, "Error while trying to delete edge between nodes");
                    }
                }
            }
            logger.log(Level.INFO, "Edges Deleted: {0}", edgesDeletedSoFar);
        } else {
            logger.log(Level.SEVERE, "Not enough data to simulate edge deletion");
        }
    }

}
