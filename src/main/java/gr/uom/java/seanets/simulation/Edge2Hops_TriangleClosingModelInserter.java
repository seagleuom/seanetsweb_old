package gr.uom.java.seanets.simulation;

import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.MyPackage;
import gr.uom.java.seanets.graph.event.EdgeAdditionEvent;
import gr.uom.java.seanets.graph.event.GraphEventLogger;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;

import java.util.ArrayList;
import java.util.Map;

public class Edge2Hops_TriangleClosingModelInserter extends EdgeFromExistingNodesAbstractInserter{
	
	public Edge2Hops_TriangleClosingModelInserter(ProjectEvolutionAnalysis pa, DistributionManager dm){
		super(pa, dm);
	}

	public void createEdgesFromExistingClassesWith_2Hops_TriangleClosingModel(double A, MyGraph simulatedGraph,
			Reporter reporter, boolean preserveDomainRules,
			boolean preserveHopDistance,
			boolean considerSourceNodeAge,
			boolean considerTargetNodeAge ) {
		/* A: separator: with prob A new edges are beginning from randomly selected nodes
		 * with prob 1-A new edges are beginning from nodes according to Inverse Preferential Attachment prob.
		 */
		if(newEdgeNumberDistributionOfExistingNodes != null) {
			Map<MyPackage, ArrayList<MyNode>> packageMap = simulatedGraph.getPackageMap();
			double random = MyRandomGenerator.getNextDoubleRandom();
			int howManyEdgesWillBeCreated = newEdgeNumberDistributionOfExistingNodes.getXValueForaY(random);
			System.out.println("--------------------------\nSimulation: creating edges From existing classes \n Will create "+howManyEdgesWillBeCreated+" edges, random = "+random);
			int edgesCreated = 0;
			while(edgesCreated < howManyEdgesWillBeCreated) {
				MyNode sourceNode = null;
				MyNode destinationNode = null;
				int bigCounter = 0;
				boolean foundProperDestinationNode = true;
				ArrayList<MyNode> destinationNodes = new ArrayList<MyNode>();
				ArrayList<MyPackage> packagesThisNodeAttachedTo = new ArrayList<MyPackage>();
				sourceNode = prepareAnExistingNode(simulatedGraph, packagesThisNodeAttachedTo, considerSourceNodeAge);
				while(destinationNodes.size() < packagesThisNodeAttachedTo.size()) {
					MyPackage aPackage = null;
					double randomNum = MyRandomGenerator.getNextDoubleRandom();
					if(bigCounter < EvolutionSimulator.bigCounterLimit) {
						aPackage = packagesThisNodeAttachedTo.get(destinationNodes.size());
						if(randomNum < A){
							destinationNode = simulatedGraph.getARandomNode(aPackage);
						}else{
							destinationNode = simulatedGraph.classSelectorAccordingTo_2Hops_TriangleClosingModel(packageMap.get(aPackage), -99);
						}
						int loopCounter = 0;
						while(destinationNode == null 
								|| sourceNode.getAge() == 0 
								|| sourceNode.equals(destinationNode)
								|| (simulatedGraph.findEdge(sourceNode, destinationNode) != null)
								|| (preserveDomainRules && sourceNode.isAbstractOrInterface())
								){
							if(destinationNode == null || sourceNode.getAge() == 0)
								reporter.increaseEdgesRejectedDueToNullDestination();
							else if (sourceNode.equals(destinationNode))
								reporter.increaseEdgesRejectedDueToSameSourceDestination();
							else if (preserveDomainRules && sourceNode.isAbstractOrInterface())
								reporter.increaseEdgesRejectedDueToDomainRules();

							if(randomNum < A){
								destinationNode = simulatedGraph.getARandomNode(aPackage);
							}else{
								destinationNode = simulatedGraph.classSelectorAccordingTo_2Hops_TriangleClosingModel(packageMap.get(aPackage), -99);
							}
							//	System.out.println("destination node selected");
							loopCounter++;
							if(loopCounter > EvolutionSimulator.loopLimit) {
								foundProperDestinationNode = false;
								//	System.out.print("+");
								break;
							}
							randomNum = MyRandomGenerator.getNextDoubleRandom();
						}
						if(foundProperDestinationNode) {
							destinationNodes.add(destinationNode);
						}
						else{
							//	System.out.println("changing source node and selecting new behavior");
							packagesThisNodeAttachedTo = new ArrayList<MyPackage>();
							sourceNode =  prepareAnExistingNode(simulatedGraph, packagesThisNodeAttachedTo, considerSourceNodeAge);
							destinationNodes = new ArrayList<MyNode>();
							foundProperDestinationNode = true;
							bigCounter++;
						}	
					}
					else{
						bigCounter=-1;
						break;
					}
				}
				if(bigCounter==-1) {
					System.out.println("\n  Aborted  \n");
					break;
				}
				for(MyNode destNode : destinationNodes) {
					try{
						MyEdge myEdge = new MyEdge(sourceNode, destNode,"simulated");
						myEdge.setVersionAdded(simulatedGraph.getProjectVersion());
						simulatedGraph.addEdge(myEdge,sourceNode, destNode,EdgeType.DIRECTED);
						GraphEventLogger.logEvent(new EdgeAdditionEvent(myEdge,sourceNode, destNode,EdgeType.DIRECTED));
					/*	System.out.println("\nNEW EDGE From Existing Node: "+sourceNode+" - "+
								simulatedGraph.getProjectVersion()+"-> "+
								destNode+" dest Node In degree "+simulatedGraph.inDegree(destNode));
					*/	edgesCreated++;
						reporter.increaseEdgesFromExistingClasses();
					}catch(Exception e){
						System.err.println("\nProblem while trying to add a new edge between existing classes");
					}
				}
			}
			System.out.println("Edges Created: "+edgesCreated);
		}
		else{
			System.out.println("Not enough data to simulate edges between existing nodes");
		}
	}



}
