package gr.uom.java.seanets.simulation;

import java.util.ArrayList;
import java.util.Random;

public class MyRandomGenerator {

	private static ArrayList<Double> myRandomDoubleList;
	private static ArrayList<Integer> myRandomIntList;
	private static int indexD = 0;
	private static int indexI = 0;
	private static Random r = new Random();
	
	private static int SIZE = 7000;

	public MyRandomGenerator(int maxInt) {
		myRandomDoubleList = new ArrayList<Double>();
		myRandomIntList = new ArrayList<Integer>();
		r = new Random();
		for(int i=0; i<SIZE; i++) {
			myRandomDoubleList.add(r.nextDouble());
			myRandomIntList.add(r.nextInt(maxInt));
		}
	}
	
	public static void resetRandomGenerator() {
		indexD = 0; indexI = 0;
	}
	
	public static double getNextDoubleRandom() {
		return myRandomDoubleList.get(nextIndexD());
	}
	
	public static double getNextDoubleRandom(double limit) {
		double num;
		num = myRandomDoubleList.get(nextIndexD());
		while(num >= limit) {
			num = myRandomDoubleList.get(nextIndexD());
		}
		return num;
	}
	
	public static int getNextIntRandom(int limit) {
		if(limit == 0)
			return 0;
		int num;
		num = myRandomIntList.get(nextIndexI());
		while(num >= limit) {
			num = myRandomIntList.get(nextIndexI());
		}
		return num;
	}
	
	private static int nextIndexI() {
		indexI++;
		indexI = indexI % SIZE;
		return indexI;
	}
	
	private static int nextIndexD() {
		indexD++;
		indexD = indexD % SIZE;
		return indexD;
	}
	
}
