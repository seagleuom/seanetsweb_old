package gr.uom.java.seanets.simulation;

import gr.uom.java.seanets.graph.ClassAndDegree;
import gr.uom.java.seanets.graph.MyGraph;

import java.util.Map;
import java.util.TreeSet;

public class Reporter {
	
	private int edgesBetweenNewClasses;
	private int edgesFromExistingClasses;
	private int edgesFromNewClasses;
	private int deletedEdges;
	
	private int edgesRejectedDueToNullDestination;
	private int edgesRejectedDueToSameSourceDestination;
	private int edgesRejectedDueToDomainRules;
	private int edgesRejectedDueToPathLength;
	private int edgesRejectedDueToAlreadyExist;
	
	public Reporter(){
		edgesBetweenNewClasses = 0;
		edgesFromExistingClasses =0;
		edgesFromNewClasses =0;
		deletedEdges =0;
		edgesRejectedDueToNullDestination=0;
		edgesRejectedDueToSameSourceDestination=0;
		edgesRejectedDueToDomainRules=0;
		edgesRejectedDueToPathLength=0;
		edgesRejectedDueToAlreadyExist=0;
	}

	public void printReport(String text){
		System.out.println("======================================Simulation Report for " + text + "===============================");
		System.out.println("Edges Btwn New created: "+edgesBetweenNewClasses);
		System.out.println("Edges From Existing created: "+edgesFromExistingClasses);
		System.out.println("Edges From New created: "+edgesFromNewClasses);
		System.out.println("Deleted Edges: "+deletedEdges);
		System.out.println("Edges Rejected Due To Null Destination: "+edgesRejectedDueToNullDestination);
		System.out.println("Edges Rejected Due To Same Source and Destination: "+edgesRejectedDueToSameSourceDestination);
		System.out.println("Edges Rejected Due To Domain Rules: "+edgesRejectedDueToDomainRules);
		System.out.println("Edges Rejected Due To Path Length: "+edgesRejectedDueToPathLength);
		System.out.println("Edges Rejected because they Already Existed: "+edgesRejectedDueToAlreadyExist);
		System.out.println("========================================================================================================");
	}
	
	public static void printTopTenOutDegreeClasses(MyGraph graph1, MyGraph graph2){
		Map<String,Integer> outDegreeAndNames1 = graph1.getClassesAndOutDegrees();
		Map<String,Integer> outDegreeAndNames2 = graph2.getClassesAndOutDegrees();

		TreeSet<ClassAndDegree> topTen1 = new TreeSet<ClassAndDegree>();
		TreeSet<ClassAndDegree> topTen2 = new TreeSet<ClassAndDegree>();

		for(String aclass : outDegreeAndNames1.keySet()) {
			int outDegree = outDegreeAndNames1.get(aclass);
			topTen1.add(new ClassAndDegree(aclass,outDegree));
		}
		for(String aclass : outDegreeAndNames2.keySet()) {
			int outDegree = outDegreeAndNames2.get(aclass);
			topTen2.add(new ClassAndDegree(aclass,outDegree));
		}

		ClassAndDegree[] cad1 = new ClassAndDegree[1];

		ClassAndDegree[] topTen1Arr = topTen1.toArray(cad1);
		ClassAndDegree[] topTen2Arr = topTen2.toArray(cad1);

		String title = "Top Ten Out Degree Classes for \n"+graph1.getProjectVersion()+"\t Out Degree\t"+graph2.getProjectVersion()+"\tOut Degree";
		printTopTen(title, topTen1Arr, topTen2Arr);
	}
	

	public static void printTopTenInDegreeClasses(MyGraph graph1, MyGraph graph2){
		Map<String,Integer> inDegreeAndNames1 = graph1.getClassesAndInDegrees();
		Map<String,Integer> inDegreeAndNames2 = graph2.getClassesAndInDegrees();

		TreeSet<ClassAndDegree> topTen1 = new TreeSet<ClassAndDegree>();
		TreeSet<ClassAndDegree> topTen2 = new TreeSet<ClassAndDegree>();

		for(String aclass : inDegreeAndNames1.keySet()) {
			int inDegree = inDegreeAndNames1.get(aclass);
			topTen1.add(new ClassAndDegree(aclass,inDegree));
		}
		for(String aclass : inDegreeAndNames2.keySet()) {
			int inDegree = inDegreeAndNames2.get(aclass);
			topTen2.add(new ClassAndDegree(aclass,inDegree));
		}

		ClassAndDegree[] cad1 = new ClassAndDegree[1];
		ClassAndDegree[] topTen1Arr = topTen1.toArray(cad1);
		ClassAndDegree[] topTen2Arr = topTen2.toArray(cad1);
		String title = "Top Ten In Degree Classes for \n"+graph1.getProjectVersion()+"\t In Degree\t"+graph2.getProjectVersion()+"\tIn Degree";
		printTopTen(title, topTen1Arr, topTen2Arr);
	}


	public static void printTopTen(String title,ClassAndDegree[] topTen1Arr, ClassAndDegree[] topTen2Arr) {
		int limit;
		if(topTen1Arr.length < topTen2Arr.length){
			if(topTen1Arr.length > 10)
				limit = 10;
			else
				limit = topTen1Arr.length;
		}
		else {
			if(topTen2Arr.length > 10)
				limit = 10;
			else
				limit = topTen2Arr.length;
		}

		System.out.println(title);
		for(int i=0; i<limit; i++) {
			System.out.println(topTen1Arr[i].getClassName()+"\t"+topTen1Arr[i].getDegree()+"\t"+topTen2Arr[i].getClassName()+"\t"+topTen2Arr[i].getDegree());
		}
	}
	
	public int getEdgesBetweenNewClasses() {
		return edgesBetweenNewClasses;
	}
	
	public void increaseEdgesBetweenNewClasses() {
		this.edgesBetweenNewClasses++;
	}
	
	public int getEdgesFromExistingClasses() {
		return edgesFromExistingClasses;
	}
	
	public void increaseEdgesFromExistingClasses() {
		this.edgesFromExistingClasses++;
	}
	
	public int getEdgesFromNewClasses() {
		return edgesFromNewClasses;
	}
	
	public void increaseEdgesFromNewClasses() {
		this.edgesFromNewClasses++;
	}
	
	public int getDeletedEdges() {
		return deletedEdges;
	}
	
	public void increaseDeletedEdges() {
		this.deletedEdges ++;
	}
	
	public void increaseEdgesRejectedDueToNullDestination(){
		edgesRejectedDueToNullDestination++;
	}
	
	public void increaseEdgesRejectedDueToSameSourceDestination(){
		edgesRejectedDueToSameSourceDestination++;
	}
	
	public void increaseEdgesRejectedDueToDomainRules(){
		edgesRejectedDueToDomainRules++;
	}
	
	public void increaseEdgesRejectedDueToPathLength(){
		edgesRejectedDueToPathLength++;
	}
	
	public void increaseEdgesRejectedDueToAlreadyExist(){
		edgesRejectedDueToAlreadyExist++;
	}
	
	
}
