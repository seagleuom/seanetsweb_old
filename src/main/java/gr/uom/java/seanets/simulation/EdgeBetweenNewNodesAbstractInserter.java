package gr.uom.java.seanets.simulation;

import gr.uom.java.seanets.statistics.FittingFunction;

public abstract class EdgeBetweenNewNodesAbstractInserter {

	protected FittingFunction newEdgeNumberDistributionOfANewToOtherNewNodes;
	protected FittingFunction newEdgeNumberDistributionOfNewNodes;
	
	public EdgeBetweenNewNodesAbstractInserter(DistributionManager dm) {
		newEdgeNumberDistributionOfANewToOtherNewNodes = dm.getNewEdgeNumberDistributionOfANewToOtherNewNodes();
		newEdgeNumberDistributionOfNewNodes = dm.getNewEdgeNumberDistributionOfNewNodes();
	}
	
	

}
