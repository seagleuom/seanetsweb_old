package gr.uom.java.seanets.simulation;

import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.statistics.DistributionFactory;
import gr.uom.java.seanets.statistics.FittingFunction;

public class DistributionManager {
	
	private final FittingFunction newNodesNumberThatMustBeInserted;
	private final FittingFunction newEdgeNumberDistributionOfANewToOtherNewNodes;
	private final FittingFunction newEdgeNumberDistributionOfNewNodes;
	private final FittingFunction edgesNumberThatANewNodeMustCreate;
	private final FittingFunction edgesNumberThatMustCreatedFromNewNodesInTotal;
	private final FittingFunction nodeAgeToNewIncomingEdgesDistribution;
	private final FittingFunction newEdgeNumberDistributionOfExistingNodes;
	private final FittingFunction ageOfSourceNodeThatCreatesOutgoingEdgeDistribution;
	private final FittingFunction hopsThatNewEdgeSpannedDistribution;
	private final FittingFunction deletedEdgesNumberDistribution;
	private final FittingFunction deletedEdgesSourceNodeOutDegreeDistribution;
	private final FittingFunction deletedEdgesTargetNodeInDegreeDistribution;
	
	public DistributionManager(ProjectEvolutionAnalysis projectEvolutionAnalysis,String projectName){
		newNodesNumberThatMustBeInserted = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false,
				projectEvolutionAnalysis.getNewNodeCountFrequencyMap(), 
				"Cumulative Distribution of new nodes inserted", "new nodes count", "Cumul. Frequency", projectName);
		
		newEdgeNumberDistributionOfANewToOtherNewNodes = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false, 
				projectEvolutionAnalysis.getEdgesFromANewToOtherNewNodesFrequencyMap(),
				"Cumulative Distribution of Number of new edges from A NEW Node to other NEW nodes","New Edge Number per Node","Cumul. Freq", projectName);

		newEdgeNumberDistributionOfNewNodes = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false, 
				projectEvolutionAnalysis.getNewEdgeNumberBtwnNewNodesToFrequencyMap(),
				"Cumulative Distribution of Number of new edges from NEW nodes","New Edge Number per version","Cumul. Freq", projectName);

		edgesNumberThatANewNodeMustCreate = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false,
				projectEvolutionAnalysis.getEdgesFromANewToExistingNodesFrequencyMap(), 
				"Cumulative Distribution of new edges created by a new node to existing nodes", "new edges number", "Cumul. Frequency", projectName);
		
		edgesNumberThatMustCreatedFromNewNodesInTotal = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false,
				projectEvolutionAnalysis.getEdgesNumberInTotalFronNewNodesFrequencyMap(), 
				"Cumulative Distribution of edges created by new nodes to existing nodes in total", "new edges number", "Cumul. Frequency", projectName);	
	
		nodeAgeToNewIncomingEdgesDistribution = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(true, 
				projectEvolutionAnalysis.getAgeToNewIncomingEdgeCountMap(),
				"Cumulative  distribution of Existing node age to new incoming edges", "Node Age", "Cumul. Percentage of incoming edges (target nodes)", projectName);
		newEdgeNumberDistributionOfExistingNodes = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false, 
						projectEvolutionAnalysis.getExistingNodeNewEdgeNumberToFrequencyMap(),
						"Number of new edges from Existing nodes","New Edge Number from existing nodes","Cumul. Freq", projectName);

		ageOfSourceNodeThatCreatesOutgoingEdgeDistribution = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(true,
				projectEvolutionAnalysis.getAgeOfSourceNodeThatCreatesOutgoingEdgeToCounterMap(), 
				"Cumulative Distribution of ages of nodes that create edges", "Node Age", "Cumul. Frequency", projectName);
		
		hopsThatNewEdgeSpannedDistribution = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false,
				projectEvolutionAnalysis.getHopsThatNewEdgeSpannedToCountMap(), 
				"Cumulative Distribution of hops that new edges spanned", "hops", "Cumul. Frequency", projectName);
		
		deletedEdgesNumberDistribution = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false,
				projectEvolutionAnalysis.getDeletedEdgesNumberToFrequencyMap(), 
				"Cumulative Distribution of number of edges deleted per version", "Nuber of Deleted Edges", "Cumul. Frequency", projectName);

		deletedEdgesSourceNodeOutDegreeDistribution = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false,
				projectEvolutionAnalysis.getOutDegreeOfSourceNodeToDeletedEdgesCountMap(), 
				"Cumulative Distribution of out degrees of deleted edge's source nodes", "Out Degree", "Cumul. Frequency", projectName);

		deletedEdgesTargetNodeInDegreeDistribution = 
				DistributionFactory.createCumulativeDistributionFromIntegerIntegerMap(false,
				projectEvolutionAnalysis.getInDegreeOfDestinationNodeToDeletedEdgesCountMap(), 
				"Cumulative Distribution of in degrees of deleted edge's target nodes", "in Degree", "Cumul. Frequency", projectName);
		
	}

	public FittingFunction getNewNodesNumberThatMustBeInserted() {
		return newNodesNumberThatMustBeInserted;
	}

	public FittingFunction getNewEdgeNumberDistributionOfANewToOtherNewNodes() {
		return newEdgeNumberDistributionOfANewToOtherNewNodes;
	}

	public FittingFunction getNewEdgeNumberDistributionOfNewNodes() {
		return newEdgeNumberDistributionOfNewNodes;
	}

	public FittingFunction getEdgesNumberThatANewNodeMustCreate() {
		return edgesNumberThatANewNodeMustCreate;
	}

	public FittingFunction getEdgesNumberThatMustCreatedFromNewNodesInTotal() {
		return edgesNumberThatMustCreatedFromNewNodesInTotal;
	}

	public FittingFunction getNodeAgeToNewIncomingEdgesDistribution() {
		return nodeAgeToNewIncomingEdgesDistribution;
	}

	public FittingFunction getNewEdgeNumberDistributionOfExistingNodes() {
		return newEdgeNumberDistributionOfExistingNodes;
	}

	public FittingFunction getAgeOfSourceNodeThatCreatesOutgoingEdgeDistribution() {
		return ageOfSourceNodeThatCreatesOutgoingEdgeDistribution;
	}

	public FittingFunction getHopsThatNewEdgeSpannedDistribution() {
		return hopsThatNewEdgeSpannedDistribution;
	}

	public FittingFunction getDeletedEdgesNumberDistribution() {
		return deletedEdgesNumberDistribution;
	}

	public FittingFunction getDeletedEdgesSourceNodeOutDegreeDistribution() {
		return deletedEdgesSourceNodeOutDegreeDistribution;
	}

	public FittingFunction getDeletedEdgesTargetNodeInDegreeDistribution() {
		return deletedEdgesTargetNodeInDegreeDistribution;
	}
	
}
