package gr.uom.java.seanets.simulation;

import gr.uom.java.seanets.util.SEANetsProperties;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyPackage;
import gr.uom.java.seanets.db.persistence.Version;
import gr.uom.java.seanets.ejb.SimulatorBean;
import gr.uom.java.seanets.util.StorageManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimulationResults implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static final String FILE_EXTENSION = "simres";
    private String resultsHeader;
    private MyGraph baseGraph;
    private MyGraph finalGraph;
    private MyGraph graphWithOnlyBarabasiSimulation;
    private MyGraph proposedModelGraph;
    private Map<Version, MyGraph> projectGraphSimulationEvolution;
    private Map<Version, MyGraph> projectGraphPASimulationEvolution;
    
    private final static Logger logger = Logger.getLogger(SimulationResults.class.getName());

    public SimulationResults(MyGraph baseGraph, MyGraph finalGraph, MyGraph graphWithOnlyBarabasiSimulation, MyGraph finalModelGraph) {
        this.baseGraph = baseGraph;
        this.finalGraph = finalGraph;
        this.graphWithOnlyBarabasiSimulation = graphWithOnlyBarabasiSimulation;
        this.proposedModelGraph = finalModelGraph;
        this.resultsHeader = "";
    }

    public SimulationResults() {

    }

    public String getResultsHeader() {
        return resultsHeader;
    }

    public void setResultsHeader(String resultsHeader) {
        this.resultsHeader = resultsHeader;
    }

    public MyGraph getBaseGraph() {
        return baseGraph;
    }

    public MyGraph getFinalGraph() {
        return finalGraph;
    }

    public MyGraph getGraphWithOnlyBarabasiSimulation() {
        return graphWithOnlyBarabasiSimulation;
    }

    public MyGraph getProposedModelGraph() {
        return proposedModelGraph;
    }

    public Map<Version, MyGraph> getProjectGraphSimulationOfEvolution() {
        return projectGraphSimulationEvolution;
    }

    public void setProjectGraphEvolution(
            Map<Version, MyGraph> projectGraphEvolution) {
        this.projectGraphSimulationEvolution = projectGraphEvolution;
    }

    public Map<Version, MyGraph> getProjectGraphPASimulationEvolution() {
        return projectGraphPASimulationEvolution;
    }

    public void setProjectGraphPASimulationEvolution(
            Map<Version, MyGraph> projectGraphPASimulationEvolution) {
        this.projectGraphPASimulationEvolution = projectGraphPASimulationEvolution;
    }

    public MyGraph[] getGraphsInArray() {
        int i = 0;
        MyGraph[] graphs = new MyGraph[projectGraphSimulationEvolution.keySet().size()];
        for (Version pv : projectGraphSimulationEvolution.keySet()) {
            graphs[i] = projectGraphSimulationEvolution.get(pv);
            i++;
        }
        return graphs;
    }

    public void printGraphDistributionMetrics() {
        baseGraph.printGraphMetrics();
        finalGraph.printGraphMetrics();
        graphWithOnlyBarabasiSimulation.printGraphMetrics();
        proposedModelGraph.printGraphMetrics();
        logger.log(Level.FINE, "{0}", getPackageCouplingsInString());
    }

    public void printGraphDistributionMetricsToFile(String projectName) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(resultsHeader).append("\n");
            sb.append(baseGraph.getGraphBasicProperties());
            sb.append(finalGraph.getGraphBasicProperties());
            sb.append(graphWithOnlyBarabasiSimulation.getGraphBasicProperties());
            sb.append(proposedModelGraph.getGraphBasicProperties());

            baseGraph.appendGraphMetrics(sb);
            finalGraph.appendGraphMetrics(sb);
            graphWithOnlyBarabasiSimulation.appendGraphMetrics(sb);
            proposedModelGraph.appendGraphMetrics(sb);
            
            sb.append(getPackageCouplingsInString()).append("\n");
            String outDir = SEANetsProperties.getInstance().getSimulationFolder();
            StorageManager.saveTextFileToLocalFolder(outDir + File.separator + projectName + "_Simulation_Results_Metrics", sb.toString());
        } catch (Exception ex) {
            Logger.getLogger(SimulationResults.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printGraphBasicProperties() {
        baseGraph.printGraphBasicProperties();
        finalGraph.printGraphBasicProperties();
        graphWithOnlyBarabasiSimulation.printGraphBasicProperties();
        proposedModelGraph.printGraphBasicProperties();
    }

    private StringBuilder getPackageCouplingsInString() {
        StringBuilder sb = new StringBuilder();
        sb.append("-------------------Afferent and Efferent Package Coupling:--------------------").append("\n");
        sb.append("\t Afferent Coupling \t \t Efferent Coupling").append("\n");
        sb.append("Package Name \tBase \tFinal \tProposed \tBase \tFinal \tProposed").append("\n");
        Set<MyPackage> commonPackages = new TreeSet<>();
        commonPackages.addAll(baseGraph.getPackageMap().keySet());
        commonPackages.retainAll(finalGraph.getPackageMap().keySet());
        commonPackages.retainAll(proposedModelGraph.getPackageMap().keySet());

        for (MyPackage aPackage : commonPackages) {
            sb.append(aPackage.getName()).
                    append("\t").append(baseGraph.getPackage(aPackage.getName()).getAfferentCoupling()).
                    append("\t").append(finalGraph.getPackage(aPackage.getName()).getAfferentCoupling()).
                    append("\t").append(proposedModelGraph.getPackage(aPackage.getName()).getAfferentCoupling()).
                    append("\t").append(baseGraph.getPackage(aPackage.getName()).getEfferentCoupling()).
                    append("\t").append(finalGraph.getPackage(aPackage.getName()).getEfferentCoupling()).
                    append("\t").append(proposedModelGraph.getPackage(aPackage.getName()).getEfferentCoupling());
            sb.append("\n");
        }
        return sb;
    }

    public void printGraphEdges() {
        System.out.println("\n\n----Graph " + proposedModelGraph.getProjectVersion() + "---------------------------");

    }

    public void saveSimulationResultsLocally(String projectName) {
        try {
            //After writing data to DB, we should store the projectEvolutionAnalysis serialized file locally
            File outDir = new File(SEANetsProperties.getInstance().getSimulationFolder());
            if (!outDir.exists()) {
                outDir.mkdir();
            }
            FileOutputStream fos = new FileOutputStream(outDir + File.separator + projectName + "." + FILE_EXTENSION);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.flush();
            oos.close();
            fos.close();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }

    public static SimulationResults loadResultsFromFile(String projectName) {

        try {
            File f = new File(SEANetsProperties.getInstance().getSimulationFolder() + File.separator + projectName + "." + FILE_EXTENSION);
            if (f.exists()) {
                SimulationResults results;
                FileInputStream fileIn = new FileInputStream(f);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                results = (SimulationResults) in.readObject();
                in.close();
                fileIn.close();
                return results;
            } else {
                return null;
            }
        } catch (Exception e) {

        }
        return null;
    }

}
