
package gr.uom.java.seanets.ws.restful;

import gr.uom.java.seanets.db.persistence.DBProject;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
@Path("gr.uom.java.seanets.db.persistence.dbproject")
public class DBProjectFacadeREST extends AbstractFacade<DBProject> {
    @PersistenceContext(unitName = "seanetsPU")
    private EntityManager em;

    public DBProjectFacadeREST() {
        super(DBProject.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(DBProject entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, DBProject entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public DBProject find(@PathParam("id") Integer id) {
        return super.find(id);
    }
    
    @GET
    @Path("getProjectName/{projectID}")
    @Produces({"application/xml", "application/json"})
    public String getProjectName(@PathParam("projectID") Integer projectID) {
        return super.find(projectID).getName();
    }
    
    @GET
    @Path("getProject/{projectID}")
    @Produces({"application/xml", "application/json"})
    public DBProject getProject(@PathParam("projectID") Integer projectID) {
        return super.find(projectID);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<DBProject> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<DBProject> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
