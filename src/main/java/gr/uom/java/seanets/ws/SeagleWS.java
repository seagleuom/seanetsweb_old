package gr.uom.java.seanets.ws;

import gr.uom.java.seanets.ejb.CommitVersion;
import gr.uom.java.seanets.ejb.JavaMailer;
import gr.uom.java.seanets.ejb.RemoteRepositoryReader;
import gr.uom.java.seanets.util.JSONExporter;
import gr.uom.java.seanets.util.JSONImporter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author teohaik
 */
@WebService(serviceName = "SeagleWS")
@Stateless
public class SeagleWS {

    @EJB
    private RemoteRepositoryReader reader;

    private final static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(SeagleWS.class.getName());

    /**
     * Web service operation
     */
    @WebMethod(operationName = "analyze")
    public void analyze(@WebParam(name = "gitPath") String gitPath) {
        try {
            if (gitPath != null) {
                reader.readFromRemoteURL(gitPath);
                reader.analyzeProject();
            }
        } catch (Exception e) {
        }
    }

    /**
     * Web service operation
     *
     * @param versionsInJSONString
     */
    @WebMethod(operationName = "analyzeVersions")
    public void analyzeVersions(@WebParam(name = "versionsInJSONString") String versionsInJSONString) {
        try {
            if (versionsInJSONString != null) {
                ArrayList<String> versionIDs = JSONImporter.getVersionIDs(versionsInJSONString);
                ArrayList<CommitVersion> selectedVersionList = new ArrayList<>();
                for (CommitVersion availableVersion : reader.getAvailableVersions()) {
                    String availableID = availableVersion.getCommit().getID();
                    if (versionIDs.contains(availableID)) {
                        selectedVersionList.add(availableVersion);
                    }
                }
                reader.setSelectedVersionList(selectedVersionList);
                reader.initializeAnalysis();
                reader.analyzeProject();
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Web service analyzeVersions encountered an error!", e);
        }
    }

    /**
     * Web service operation
     *
     * @param gitPath
     * @return
     */
    @WebMethod(operationName = "downloadProject")
    public String downloadProject(@WebParam(name = "gitPath") String gitPath) {
        String jsonToReturn = "";
        try {
            List<CommitVersion> availableVersionList;
            if (gitPath != null) {
                reader.readFromRemoteURL(gitPath);
                availableVersionList = reader.getAvailableVersions();
                jsonToReturn = JSONExporter.exportJSONString(availableVersionList);
            }
        } catch (Exception e) {
        }
        return jsonToReturn;
    }

    /**
     * Web service operation
     * @param userMail
     */
    @WebMethod(operationName = "setmail")
    public void setmail(@WebParam(name = "userMail") final String userMail) {
           String mail = userMail.substring(0,userMail.indexOf("00"));
           String path = userMail.substring(userMail.indexOf("00")+2, userMail.length());
           
        if (JavaMailer.isValidEmailAddress(mail)) {
            reader.setCorrespondingMail(mail);
            logger.log(Level.INFO, "User mail received. Email: {0}", mail);
            reader.sendNotificationOfCompletionEmail(path);
        }
    }

}
