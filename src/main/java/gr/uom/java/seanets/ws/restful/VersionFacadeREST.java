
package gr.uom.java.seanets.ws.restful;

import gr.uom.java.seanets.db.persistence.DBProject;
import gr.uom.java.seanets.db.persistence.Version;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
@Path("gr.uom.java.seanets.db.persistence.version")
public class VersionFacadeREST extends AbstractFacade<Version> {
    @PersistenceContext(unitName = "seanetsPU")
    
    private EntityManager em;
    

    public VersionFacadeREST() {
        super(Version.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Version entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Version entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }
/*
    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Version find(@PathParam("id") Integer id) {
        return super.find(id);
    }
    */
    
    @GET
    @Path("byPid/{pid}")
    @Produces({"application/xml", "application/json"})
    public List<String> findByProjectId(@PathParam("pid") Integer pid) {
       DBProject project = (DBProject)getEntityManager().createNamedQuery("DBProject.findByPid").setParameter("pid", pid).getResultList().get(0);
       if(project != null ) {
            List<Version> list = getEntityManager().createNamedQuery("Version.findByPid").setParameter("pid", project).getResultList();
            List<String> versionNames = new ArrayList();
            for(Version v : list) {
                versionNames.add(v.getName());
            }
            return versionNames;
       }else
           return new ArrayList();
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Version> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Version> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
