package gr.uom.java.seanets.websocket;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/** 
 * @ServerEndpoint gives the relative name for the end point
 * This will be accessed via ws://localhost:8080/WebSocketTest/websocket
 * Where localhost is the address of the host
 */
@ServerEndpoint("/loggerSocket") 
public class LoggerWebSocket {
    /**
     * @OnOpen allows us to intercept the creation of a new session.
     * The session class allows us to send data to the user.
     * In the method onOpen, we'll let the user know that the handshake was 
     * successful.
     */
    
    
    private static Session session;
    private static final Logger logger = java.util.logging.Logger.getLogger(LoggerWebSocket.class.getName());
    
    @OnOpen
    public void onOpen(Session session){
       logger.log(Level.INFO, "{0} has opened a connection", session.getId()); 
        try {
            LoggerWebSocket.session = session;
            session.getBasicRemote().sendText("Connection Established");
        } catch (IOException ex) {
             logger.log(Level.SEVERE,null,ex);
        }
    }
    
    public static void sendLogMessage(String status){
        try {
        //    System.out.println("Sending log Message                    "+status);
            session.getBasicRemote().sendText(status);
        } catch (Exception ex) {
         //   Logger.getLogger(LoggerWebSocket.class.getName()).log(Level.SEVERE, "Websocket connection has been closed, but the analysis will continue");
        }
    }
    
    /**
     * When a user sends a message to the server, this method will intercept the message
     * and allow us to react to it. For now the message is read as a String.
     * @param message
     * @param session
     */
    @OnMessage
    public void onMessage(String message, Session session){
        logger.log(Level.FINE, "Message from {0}: {1}", new Object[]{session.getId(), message});
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException ex) {
            logger.log(Level.SEVERE,null,ex);
        }
    }

    /**
     * The user closes the connection.
     * 
     * Note: you can't send messages to the client from this method
     * @param session
     */
    @OnClose
    public void onClose(Session session){
         logger.log(Level.INFO, "Session {0} has ended", session.getId());
    }
}