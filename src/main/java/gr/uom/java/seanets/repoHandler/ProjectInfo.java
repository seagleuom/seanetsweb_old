package gr.uom.java.seanets.repoHandler;

import gr.uom.java.seanets.util.SEANetsProperties;

import java.io.File;
import java.io.Serializable;

public class ProjectInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String projectName;
    private String projectRemoteRepositoryURL;
    private String projectLocalSourceFolder;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        if (projectName.length() > 1) {
            this.projectName = projectName;
        } else {
            this.projectName = NameResolverFactory.resolveProjectName(projectRemoteRepositoryURL);
        }
    }

    public String getProjectRemoteRepositoryURL() {
        return projectRemoteRepositoryURL;
    }

    public void setProjectRemoteRepositoryURL(String projectRemoteRepositoryURL) {
        this.projectRemoteRepositoryURL = projectRemoteRepositoryURL;
        setProjectName("");
        setProjectLocalSourceFolder();
    }

    @Override
    public String toString() {
        return "Project " + projectName;
    }

    public String getProjectLocalSourceFolder() {
        return projectLocalSourceFolder;
    }

    public String getProjectLocalRepositoryFolder() {
        return SEANetsProperties.getInstance().getRepositoriesFolder() + File.separator + projectName;
    }

    private void setProjectLocalSourceFolder() {
        projectLocalSourceFolder = SEANetsProperties.getInstance().getProjectsFolder() + File.separator + projectName;
    }

}
