
package gr.uom.java.seanets.repoHandler;

/**
 *
 * @author Theodore Chaikalis
 */
public class SourceForgeNameResolver implements ProjectNameResolver {

    @Override
    public String resolveName(String gitPath) {
        String name = null;
        if (gitPath.contains("code.sf.net")) {
            String temp = "" + gitPath;
            int indexOfP = temp.indexOf("/p/");  
            temp = temp.substring(indexOfP+3);
            int lastSlash = temp.indexOf("/");
            name = temp.substring(0,lastSlash);
        }
        return name;
    }

}
