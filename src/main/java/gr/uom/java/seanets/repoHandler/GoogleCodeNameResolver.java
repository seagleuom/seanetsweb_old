
package gr.uom.java.seanets.repoHandler;

/**
 *
 * @author Theodore Chaikalis
 */
public class GoogleCodeNameResolver implements ProjectNameResolver {

    @Override
    public String resolveName(String gitPath) {
        String name = null;
        if (gitPath.contains("code.google")) {
            String temp = "" + gitPath;
            int lastSlashPosition = gitPath.lastIndexOf("/");
            int pathLength = gitPath.length();
            if (lastSlashPosition == pathLength - 1) {
                temp = temp.substring(0, lastSlashPosition);
                name = temp.substring(temp.lastIndexOf("/") + 1);
            } else {
                name = temp.substring(temp.lastIndexOf("/") + 1);
            }
        }
        return name;
    }

}
