
package gr.uom.java.seanets.repoHandler;

/**
 *
 * @author Theodore Chaikalis
 */
public interface ProjectNameResolver {

    public abstract String resolveName(String gitPath);
}
