
package gr.uom.java.seanets.repoHandler;

/**
 *
 * @author Theodore Chaikalis
 */
public class GithubNameResolver implements ProjectNameResolver {

    @Override
    public String resolveName(String gitPath) {
        String name = null;
        if (gitPath.contains("github")) {
            String end = gitPath.substring(gitPath.lastIndexOf("/") + 1);
            end = end.substring(0, end.indexOf(".git"));
            name = end;
        }
        return name;
    }

}
