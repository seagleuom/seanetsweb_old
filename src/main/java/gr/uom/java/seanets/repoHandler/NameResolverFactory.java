package gr.uom.java.seanets.repoHandler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Theodore Chaikalis
 */
public class NameResolverFactory {

    private static final List<ProjectNameResolver> nameResolvers = new ArrayList<>();

    public static void registerNameResolver(ProjectNameResolver resolver) {
        nameResolvers.add(resolver);
    }

    static{
        nameResolvers.add(new GoogleCodeNameResolver());
        nameResolvers.add(new GithubNameResolver());
        nameResolvers.add(new SourceForgeNameResolver());
        nameResolvers.add(new DefaultNameResolver());
    }
    
    public static String resolveProjectName(String gitPath) {
        for (ProjectNameResolver pnr : nameResolvers) {
            String name = pnr.resolveName(gitPath);
            if (name != null) {
                return name;
            }
        }
        return "Project";
    }

}
