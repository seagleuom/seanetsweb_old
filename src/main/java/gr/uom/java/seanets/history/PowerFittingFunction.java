package gr.uom.java.seanets.history;

import gr.uom.java.seanets.statistics.FittingFunction;

/*
 * Cumulative Distribution Function form: a * x ^ b) 
 */
public class PowerFittingFunction extends FittingFunction{

	private double a;
	private double b;
//	private MyScatterPlotChartWithPowerFitting chart;
	private double rSquared;
	
	public PowerFittingFunction(double[] array){
		super(null);
		a = array[0];
		b = array[1];
	//	System.out.println("Regression result, Power Function: "+a+" * x ^ "+b);
	}
	
	public PowerFittingFunction(String title, String xLabel, String yLabel,double[][] data){
		super(data);
	//	chart = new MyScatterPlotChartWithPowerFitting(title, xLabel, yLabel, data, this);
		rSquared = getRSquared();
	}
	
//	public void displayDistributionChart(){
//		if(chart != null) {
//			chart.prepareChart();
//		//	chart.displayChart();
//		}
//	}

	public double getValue(double x){
		return a * Math.pow(x,b);
	}
	
	public int getIntValue(double x){
		return (int)(a * Math.pow(x,b));
	}

	public int getXValueForaY(double y){
		// The inverse of the power function Y = a * b ^x is 
		// X = b root of (y/a)
		/*            _____
		 *        \ b/ y/a
		 *    X =  \/   
		 */
		return (int)Math.round(Math.pow( ( y/a ) , ( 1/b ) ));
	}
	
	protected double getRSquared(){
		int dataRows = data.length;
		double squaredErrorFromActualY = 0;
		double squaredDistanceOfYFromMeanY = 0;
		double SumOfSquaredErrorFromActualY = 0;
		double SumOfSquaredDistanceOfErrorFromMeanY = 0;
		double meanY = getMean(data, 1);
		double estimatedY =0;
		if(data!= null) {
			for(int i=0; i<dataRows; i++){
				double x = data[i][0];
				double y = data[i][1];		
				estimatedY = getValue(x);
				squaredErrorFromActualY = Math.pow(estimatedY- y, 2);
				squaredDistanceOfYFromMeanY = Math.pow(y - meanY, 2);
				SumOfSquaredErrorFromActualY += squaredErrorFromActualY;
				SumOfSquaredDistanceOfErrorFromMeanY += squaredDistanceOfYFromMeanY;
			}
		}
		return 1 - (SumOfSquaredErrorFromActualY/SumOfSquaredDistanceOfErrorFromMeanY);
	}

	public double getA() {
		return a;
	}

	public double getB() {
		return b;
	}

	public void setA(double a) {
		this.a = a;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getrSquared() {
		return rSquared;
	}
}
