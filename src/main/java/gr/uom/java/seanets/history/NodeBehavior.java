package gr.uom.java.seanets.history;

import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.MyPackage;

import java.io.Serializable;
import java.util.ArrayList;

public class NodeBehavior implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MyNode node;
	private ArrayList<MyPackage> packagesAttached;
	
	public NodeBehavior(MyNode node, ArrayList<MyPackage> packagesAttached) {
		this.node = node;
		this.packagesAttached = packagesAttached;
	}
	
	public NodeBehavior(MyNode node) {
		this.node = node;
		this.packagesAttached = new ArrayList<MyPackage>();
	}

	public MyNode getNode() {
		return node;
	}

	public void setNode(MyNode node) {
		this.node = node;
	}

	public ArrayList<MyPackage> getPackagesAttached() {
		return packagesAttached;
	}

	public void setPackagesAttached(ArrayList<MyPackage> packagesAttached) {
		this.packagesAttached = packagesAttached;
	}
	
	public void addPackageAttached(MyPackage mp) {
		this.packagesAttached.add(mp);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder("Source Node: "+node.getName()+" -> \n");
		for(MyPackage p : packagesAttached){
			sb.append("\t"+p.getName()+"\n");
		}
		return sb.toString();
	}
}
