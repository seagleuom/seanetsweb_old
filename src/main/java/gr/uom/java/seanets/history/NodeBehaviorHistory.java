package gr.uom.java.seanets.history;

import gr.uom.java.seanets.simulation.MyRandomGenerator;
import gr.uom.java.seanets.statistics.FittingFunction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NodeBehaviorHistory implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<NodeBehavior> listOfNodeBehaviors;
	private List<Integer> usedIndexes;

	public NodeBehaviorHistory(){
		listOfNodeBehaviors =  Collections.synchronizedList(new ArrayList<NodeBehavior>());
		usedIndexes =  Collections.synchronizedList(new ArrayList<Integer>());
	}
	public void addBehavior(NodeBehavior behavior){
		listOfNodeBehaviors.add(behavior);
	}
	public List<NodeBehavior> getListOfNodeBehaviors() {
		return listOfNodeBehaviors;
	}

	public void setListOfNodeBehaviors(ArrayList<NodeBehavior> listOfNodeBehaviors) {
		this.listOfNodeBehaviors = listOfNodeBehaviors;
	}

	public List<Integer> getUsedIndexes() {
		return usedIndexes;
	}

	public void setUsedIndexes(ArrayList<Integer> usedIndexes) {
		this.usedIndexes = usedIndexes;
	}

	public NodeBehavior getARandomNodeBehavior() {
		int limit = listOfNodeBehaviors.size();
		int randomIndex = MyRandomGenerator.getNextIntRandom(limit);
		int counter = 0;
		while(usedIndexes.contains(randomIndex)) {
			counter++;
			if(counter > 2*limit) {
				usedIndexes = new ArrayList<Integer>();
			}
			randomIndex =  MyRandomGenerator.getNextIntRandom(limit);
		}
		NodeBehavior randomNodeBehavior = listOfNodeBehaviors.get(randomIndex);
		usedIndexes.add(randomIndex);
		if(usedIndexes.size() == limit){
			usedIndexes = new ArrayList<Integer>();
		}
		return randomNodeBehavior;
	}

	public NodeBehavior getANodeBehavior(FittingFunction ageDistribution, int tolerance, boolean considerNodeAge) {
		NodeBehavior nodeBehavior = null;
		if(considerNodeAge){		
			int counter = 0;
			int limit = listOfNodeBehaviors.size();
			int randomIndex = MyRandomGenerator.getNextIntRandom(limit);
			try{
				int targetAge = ageDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
				nodeBehavior = listOfNodeBehaviors.get(randomIndex);
				while(Math.abs(nodeBehavior.getNode().getAge() - targetAge) > tolerance) {
					randomIndex = MyRandomGenerator.getNextIntRandom(limit);
					nodeBehavior = listOfNodeBehaviors.get(randomIndex);
					counter++;
					if(counter > 2*limit) {
						targetAge = ageDistribution.getXValueForaY(MyRandomGenerator.getNextDoubleRandom());
						counter = 0;
						tolerance++;
					}
				}
			}
			catch(Exception e){
				System.err.println("Problem while selecting node behavior according to node age distribution");
			}
		}
		else{
			nodeBehavior = getARandomNodeBehavior();
		}
		return nodeBehavior;
	}

}
