package gr.uom.java.seanets.history;

import gr.uom.java.seanets.util.StorageManager;
import gr.uom.java.seanets.db.persistence.DBGraph;
import gr.uom.java.seanets.db.persistence.Version;
import gr.uom.java.seanets.graph.MyEdge;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.graph.MyPackage;
import gr.uom.java.seanets.graph.Utilities;
import gr.uom.java.seanets.gui.SelectedProjectName;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import org.eclipse.core.runtime.IProgressMonitor;

public class ProjectEvolutionAnalysis implements Serializable {

    private static final long serialVersionUID = 1L;

    //contains the versions that will be the sample for the simulation:
    private ArrayList<Version> projectBaseVersionList;
    //contains all project versions
    private ArrayList<Version> projectAllVersionsList;

    private Map<Version, Map<String, Set<String>>> accessedClassesMapEvolution;
    private Map<Version, Map<String, Set<MyEdge>>> accessedEdgesMapEvolution;
    private Map<Version, Set<MyNode>> newNodesMap;
    private Map<Version, Set<MyNode>> existingNodesMap;
    private Map<Version, Set<MyNode>> deletedNodesMap;

    private Map<Version, DBGraph> versionToProjectDataMap;
    private Map<Version, MyGraph> projectGraphEvolution;

    private Map<Integer, Integer> ageOfSourceNodeThatCreatesOutgoingEdgeToCounterMap;
    private Map<Integer, Integer> ageToDeletedNodeCountMap;
    private Map<Integer, Integer> ageToNewIncomingEdgeCountMap;
    private Map<Integer, Integer> ageToNewOutgoingEdgeCountMap;
    private Map<Integer, Integer> deletedEdgesNumberToFrequencyMap;
    private Map<Integer, Integer> existingNodeOutDegreeToNewOutgoingEdgesMap;
    private Map<Integer, Integer> existingNodeNewEdgeNumberToFrequencyMap;
    private Map<Integer, Integer> edgesNumberFronANewToExistingNodesFrequencyMap;
    private Map<Integer, Integer> edgesFronANewToOtherNewNodesFrequencyMap;
    private Map<Integer, Integer> edgesNumberInTotalFronNewNodesFrequencyMap;
    private Map<Integer, Integer> hopsThatNewEdgeSpannedToCountMap;
    private Map<Integer, Integer> inDegreeToDeletedNodeCountMap;
    private Map<Integer, Integer> inDegreeOfDestinationNodeToDeletedEdgesCountMap;
    private Map<Integer, Integer> newEdgeNumberBtwnNewNodesToFrequencyMap;
    private Map<Integer, Integer> newNodeCountFrequencyMap;
    private Map<Integer, Integer> outDegreeToDeletedNodeCountMap;
    private Map<Integer, Integer> outDegreeOfSourceNodeToDeletedEdgesCountMap;

    private Map<Integer, TreeMap<Version, Double>> inDegreeToNormalizedEdgesMap;

    private Map<MyPackage, Integer> packageToTotalIncomingEdgesMap;
    private Map<MyPackage, Integer> packageToTotalOutgoingEdgesMap;

    private NodeBehaviorHistory newNodeBehaviors;
    private NodeBehaviorHistory existingNodeBehaviors;

    private double averageNumberOfNewEdgesFromExistingNodes;
    private double averageNumberOfNewEdgesFromNewToNewNodes;

    private int edgesToSuperclasses;
    private int edgesToSubclasses;
    private int edgesFromSuperclasses;
    private int edgesFromSubclasses;
    private int edgesFromSuperclassesToSubclassesOfTheSameHierarchy;
    private int totalEdgesAdded;

    private ProjectEvolution projectEvolution;

    public ProjectEvolutionAnalysis(ProjectEvolution projectEvolution) {
        this.projectEvolution = projectEvolution;
        this.accessedClassesMapEvolution = new TreeMap<Version, Map<String, Set<String>>>();
        this.accessedEdgesMapEvolution = new TreeMap<Version, Map<String, Set<MyEdge>>>();
        this.versionToProjectDataMap = new TreeMap<Version, DBGraph>();
        this.projectGraphEvolution = new TreeMap<Version, MyGraph>();
        newNodeCountFrequencyMap = new TreeMap<Integer, Integer>();
        /*
         * Switch to getBaseVersionEntries if wanted:
         */
        this.projectBaseVersionList = projectEvolution.getAllVersionEntries();

        this.projectAllVersionsList = projectEvolution.getAllVersionEntries();

        this.packageToTotalIncomingEdgesMap = new LinkedHashMap<MyPackage, Integer>();
        this.packageToTotalOutgoingEdgesMap = new LinkedHashMap<MyPackage, Integer>();
        ageOfSourceNodeThatCreatesOutgoingEdgeToCounterMap = new TreeMap<Integer, Integer>();
        ageToNewIncomingEdgeCountMap = new TreeMap<Integer, Integer>();
        ageToNewOutgoingEdgeCountMap = new TreeMap<Integer, Integer>();
        inDegreeToNormalizedEdgesMap = new TreeMap<Integer, TreeMap<Version, Double>>();
        newNodeBehaviors = new NodeBehaviorHistory();
        existingNodeBehaviors = new NodeBehaviorHistory();
        averageNumberOfNewEdgesFromExistingNodes = 0;
        averageNumberOfNewEdgesFromNewToNewNodes = 0;
        edgesToSuperclasses = 0;
        edgesToSubclasses = 0;
        edgesFromSuperclasses = 0;
        edgesFromSubclasses = 0;
        edgesFromSuperclassesToSubclassesOfTheSameHierarchy = 0;
        totalEdgesAdded = 0;
    }

    public void addGraphInEvolutionHistory(Version pv, MyGraph graph) {
        projectGraphEvolution.put(pv, graph);
    }

    public void graphEvolutionCalculations() {
        int ageCounter = 0;
        for (Version pv : projectGraphEvolution.keySet()) {
            MyGraph graph = projectGraphEvolution.get(pv);
            if (graph == null) {
                System.err.println("break, graph is null");
            }
            updateNodeAges(ageCounter, graph);
            ageCounter++;
        }
    }

    private void updateNodeAges(int i, MyGraph graph) {
        //Update node ages:
        if (i > 0) {
            Version previousVersion = projectAllVersionsList.get(i - 1);
            MyGraph prevGraph = projectGraphEvolution.get(previousVersion);
            graph.updateNodeAges(prevGraph);
        }
    }

    public void performGraphCalculations(Version currentVersion,
            IProgressMonitor monitor,
            Map<String, Set<String>> accessedClassesMap,
            Map<String, Set<MyEdge>> accessedEdgesMap, MyGraph graph) {
        accessedClassesMapEvolution.put(currentVersion, accessedClassesMap);
        accessedEdgesMapEvolution.put(currentVersion, accessedEdgesMap);
        projectGraphEvolution.put(currentVersion, graph);
        graph.calculateDiameterAndShortestPaths();
        graph.calculateClusteringCoefficient();
        graph.calculateGlobalPreferentialAttachment();
        graph.calculateDegreeDistribution();
        graph.getInDegreeVSClusteringCoefficientMapString();
        graph.calculatePageRank();
        graph.calculateCentralities();
    }

    public void edgeAnalysis() {
        calculationOfNewExistingAndDeletedNodes();
        edgesToNewNodes();
        edgesFromExistingNodes();
        edgeBetweenNewNodes();
        edgesToExistingNodes();
        newNodeAnalysis();
        deletedEdgesAnalysis();
        calculateNormalizedPercentagesOfNewEdgesAttractedForDifferentInDegrees();
		//	existingNodeAgeAnalysis();
        //	existingNodeAnalysis();
        //	debugEdgePrinting();
        //	determineNewEdgesInEachVersion();
    }

    private void calculateNormalizedPercentagesOfNewEdgesAttractedForDifferentInDegrees() {
        Version finalVersion = projectBaseVersionList.get(projectBaseVersionList.size() - 1);
        Map<Integer, Integer> finalInDegreeToCountMap = projectGraphEvolution.get(finalVersion).getInDegreeToCountMap();
        for (Integer inDegree : finalInDegreeToCountMap.keySet()) {
            for (Version currentVersion : projectGraphEvolution.keySet()) {
                MyGraph currentGraph = projectGraphEvolution.get(currentVersion);
                Map<Integer, Integer> existingNodeInDegreeToNewEdgesAttractedFrequencyMap = currentGraph.getExistingNodeInDegreeToNewEdgesAttractedFrequencyMap();
                Map<Integer, Integer> inDegreeToCountMap = currentGraph.getInDegreeToCountMap();
                if (inDegreeToCountMap.containsKey(inDegree) && existingNodeInDegreeToNewEdgesAttractedFrequencyMap.containsKey(inDegree)) {
                    int edgesToNodesOfCurrentInDegree = existingNodeInDegreeToNewEdgesAttractedFrequencyMap.get(inDegree);
                    int countOfNodesHavingCurrentInDegree = inDegreeToCountMap.get(inDegree);
                    if (countOfNodesHavingCurrentInDegree > 0) {
                        double quotient = 1.0 * edgesToNodesOfCurrentInDegree / countOfNodesHavingCurrentInDegree;
                        if (inDegreeToNormalizedEdgesMap.containsKey(inDegree)) {
                            inDegreeToNormalizedEdgesMap.get(inDegree).put(currentVersion, quotient);
                        } else {
                            TreeMap<Version, Double> map = new TreeMap<Version, Double>();
                            map.put(currentVersion, quotient);
                            inDegreeToNormalizedEdgesMap.put(inDegree, map);
                        }
                    }
                }
            }
        }
        //	printInDegreeToNormalizedEdgesAttracted();
    }
    
    public String getInDegreeToNormalizedEdgesAttracted() {
        StringBuilder sb = new StringBuilder("In Degree" + "\t" + "Normalized Edges Attracted").append("\n");
        for (int inDegree : inDegreeToNormalizedEdgesMap.keySet()) {
            TreeMap<Version, Double> map = inDegreeToNormalizedEdgesMap.get(inDegree);
            double average = 0;

            for (Version pv : map.keySet()) {
                average += map.get(pv);
            }
            average = average / map.keySet().size();
            sb.append(inDegree).append("\t").append(average).append("\n");
        }
        return sb.toString();
    }

    private void updateFrequencyMap(Map<Integer, Integer> map, int key) {
        if (!map.containsKey(key)) {
            map.put(key, 1);
        } else {
            map.put(key, map.get(key) + 1);
        }
    }

    private void deletedEdgesAnalysis() {
        Map<Version, Set<MyEdge>> deletedEdgesBetweenExistingClassesForEachVersion = new LinkedHashMap<Version, Set<MyEdge>>();
        inDegreeOfDestinationNodeToDeletedEdgesCountMap = new TreeMap<Integer, Integer>();
        outDegreeOfSourceNodeToDeletedEdgesCountMap = new TreeMap<Integer, Integer>();
        deletedEdgesNumberToFrequencyMap = new TreeMap<Integer, Integer>();

        versionToProjectDataMap.get(projectBaseVersionList.get(0)).setDeletedEdges(0);
        for (int i = 0; i < projectBaseVersionList.size() - 1; i++) {
            Set<MyEdge> deletedEdgesForAVersion = new LinkedHashSet<MyEdge>();
            Version currentVersion = projectBaseVersionList.get(i);
            Version nextVersion = projectBaseVersionList.get(i + 1);
            MyGraph currentGraph = versionToProjectDataMap.get(currentVersion).getGraph();
            MyGraph nextGraph = versionToProjectDataMap.get(nextVersion).getGraph();
            Set<MyEdge> currentEdges = new LinkedHashSet<MyEdge>(currentGraph.getEdges());
            Set<MyEdge> nextEdges = new LinkedHashSet<MyEdge>(nextGraph.getEdges());
            currentEdges.removeAll(nextEdges);
            deletedEdgesForAVersion.addAll(currentEdges);
            deletedEdgesBetweenExistingClassesForEachVersion.put(nextVersion, deletedEdgesForAVersion);
            int numberOfDeletedEdges = deletedEdgesForAVersion.size();
            versionToProjectDataMap.get(nextVersion).setDeletedEdges(numberOfDeletedEdges);
            //Update of the frequency map:
            updateFrequencyMap(deletedEdgesNumberToFrequencyMap, numberOfDeletedEdges);

        }

        for (int i = 1; i < projectBaseVersionList.size(); i++) {
            Version currentVersion = projectBaseVersionList.get(i);
            Version previousVersion = projectBaseVersionList.get(i - 1);

            MyGraph graph = projectGraphEvolution.get(previousVersion);
            Set<MyEdge> deletedEdges = deletedEdgesBetweenExistingClassesForEachVersion.get(currentVersion);
            for (MyEdge deletedEdge : deletedEdges) {
                MyNode sourceNode = deletedEdge.getSourceNode();
                MyNode targetNode = deletedEdge.getTargetNode();
                int inDegreeOfTargetNode = graph.inDegree(targetNode);
                int outDegreeOfSourceNode = graph.outDegree(sourceNode);

                updateFrequencyMap(inDegreeOfDestinationNodeToDeletedEdgesCountMap, inDegreeOfTargetNode);
                updateFrequencyMap(outDegreeOfSourceNodeToDeletedEdgesCountMap, outDegreeOfSourceNode);
            }
        }
    }

    public void newNodeAnalysis() {
        edgesNumberFronANewToExistingNodesFrequencyMap = new TreeMap<Integer, Integer>();
        edgesFronANewToOtherNewNodesFrequencyMap = new TreeMap<Integer, Integer>();
        for (int i = 1; i < projectBaseVersionList.size(); i++) {
            Version currentVersion = projectBaseVersionList.get(i);
            Set<MyNode> newNodes = this.newNodesMap.get(currentVersion);
            for (MyNode newNode : newNodes) {
                int outgoingEdgesToExistingNodes = 0;
                int outgoingEdgesToNewNodes = 0;
                NodeBehavior newNodeBehavior = new NodeBehavior(newNode);
                Collection<MyNode> newNodeOutNeighbors = projectGraphEvolution.get(currentVersion).getOutNeighboorhood(newNode);
                for (MyNode neighborOfNewNode : newNodeOutNeighbors) {
                    newNodeBehavior.addPackageAttached(neighborOfNewNode.getPackage());
                    /*
                     * Counters update:
                     */
                    if (!newNodes.contains(neighborOfNewNode)) {
                        outgoingEdgesToExistingNodes++;
                    } else {
                        outgoingEdgesToNewNodes++;
                    }
                }
                if (newNodeBehavior.getPackagesAttached().size() > 0) {
                    this.newNodeBehaviors.addBehavior(newNodeBehavior);
                }
                //For each new software node, update the maps:
                if (edgesFronANewToOtherNewNodesFrequencyMap.containsKey(outgoingEdgesToNewNodes)) {
                    edgesFronANewToOtherNewNodesFrequencyMap.put(outgoingEdgesToNewNodes, edgesFronANewToOtherNewNodesFrequencyMap.get(outgoingEdgesToNewNodes) + 1);
                } else {
                    edgesFronANewToOtherNewNodesFrequencyMap.put(outgoingEdgesToNewNodes, 1);
                }
                averageNumberOfNewEdgesFromNewToNewNodes += outgoingEdgesToNewNodes;
                if (edgesNumberFronANewToExistingNodesFrequencyMap.containsKey(outgoingEdgesToExistingNodes)) {
                    edgesNumberFronANewToExistingNodesFrequencyMap.put(outgoingEdgesToExistingNodes, edgesNumberFronANewToExistingNodesFrequencyMap.get(outgoingEdgesToExistingNodes) + 1);
                } else {
                    edgesNumberFronANewToExistingNodesFrequencyMap.put(outgoingEdgesToExistingNodes, 1);
                }
            }
        }
        averageNumberOfNewEdgesFromNewToNewNodes = averageNumberOfNewEdgesFromNewToNewNodes * 1.0 / projectBaseVersionList.size() * 1.0;
    }

    public String getAgeOfSourceNodeThatCreatesOutgoingEdgeToCounterMapInString() {
        StringBuilder sb = new StringBuilder();
        for (Integer age : this.ageOfSourceNodeThatCreatesOutgoingEdgeToCounterMap.keySet()) {
            sb.append(age).append(" \t ").append(ageOfSourceNodeThatCreatesOutgoingEdgeToCounterMap.get(age)).append("\n");
        }
        return sb.toString();
    }

    public String getAgeVSnewIncomingEdgesMap() {
        StringBuilder sb = new StringBuilder();
        for (Integer age : this.ageToNewIncomingEdgeCountMap.keySet()) {
            sb.append(age).append(" \t ").append(ageToNewIncomingEdgeCountMap.get(age)).append("\n");
        }
        return sb.toString();
    }

    public String getAgeVSnewOutgoingEdgesMap() {
        StringBuilder sb = new StringBuilder();
        for (Integer age : this.ageToNewOutgoingEdgeCountMap.keySet()) {
            sb.append(age).append(" \t ").append(ageToNewOutgoingEdgeCountMap.get(age)).append("\n");
        }
        return sb.toString();
    }

    public void calculationOfNewExistingAndDeletedNodes() {
        newNodesMap = new LinkedHashMap<Version, Set<MyNode>>();
        existingNodesMap = new LinkedHashMap<Version, Set<MyNode>>();
        deletedNodesMap = new LinkedHashMap<Version, Set<MyNode>>();
        for (int i = 1; i < projectBaseVersionList.size(); i++) {
            Version currentVersion = projectBaseVersionList.get(i);
            Version previousVersion = projectBaseVersionList.get(i - 1);
            Set<MyNode> newClasses = new LinkedHashSet<MyNode>();
            Set<MyNode> existingClasses = new LinkedHashSet<MyNode>();
            Set<MyNode> deletedClasses = new LinkedHashSet<MyNode>();
            Collection<MyNode> classesInCurrentVersion = projectGraphEvolution.get(currentVersion).getVertices();
            for (MyNode node : classesInCurrentVersion) {
                if (node.getAge() == 0) {
                    newClasses.add(node);
                } else {
                    existingClasses.add(node);
                }
            }
            //Determination of the deleted classes:
            Collection<MyNode> classesInPreviousVersion = projectGraphEvolution.get(previousVersion).getVertices();
            for (MyNode classInPreviousVersion : classesInPreviousVersion) {
                if (!classesInCurrentVersion.contains(classInPreviousVersion)) {
                    deletedClasses.add(classInPreviousVersion);
                }
            }
            newNodesMap.put(currentVersion, newClasses);
            updateFrequencyMap(newNodeCountFrequencyMap, newClasses.size());
            existingNodesMap.put(currentVersion, existingClasses);
            deletedNodesMap.put(currentVersion, deletedClasses);
        }
    }

    /*
     * @Deprecated
     * This functionality is covered by method edgesFromExistingNodes()
     */
    private Map<Version, Set<MyEdge>> edgesToNewNodes() {
        int counter = 0;
        Map<Version, Set<MyEdge>> edgesToNewClassesPerVersion = new LinkedHashMap<Version, Set<MyEdge>>();
        for (int i = 1; i < projectBaseVersionList.size(); i++) {
            Map<String, Set<String>> previousAccessedClassesMap = accessedClassesMapEvolution.get(projectBaseVersionList.get(i - 1));
            Map<String, Set<String>> currentAccessedClassesMap = accessedClassesMapEvolution.get(projectBaseVersionList.get(i));
            List<String> previousAccessedClassesKeyList = new ArrayList<String>(previousAccessedClassesMap.keySet());
            List<String> currentAccessedClassesKeyList = new ArrayList<String>(currentAccessedClassesMap.keySet());
            Set<MyEdge> edgesToNewClassesForAVersion = new LinkedHashSet<MyEdge>();

            for (String currentAccessedClassesMapKeyName : currentAccessedClassesKeyList) {
                if (!previousAccessedClassesKeyList.contains(currentAccessedClassesMapKeyName)) {
                    for (String keyClassName : currentAccessedClassesKeyList) {
                        for (String accessedClassesName : currentAccessedClassesMap.get(keyClassName)) {
                            MyNode node = this.versionToProjectDataMap.get(projectBaseVersionList.get(i)).getGraph().getNode(keyClassName);
                            if (accessedClassesName.equals(currentAccessedClassesMapKeyName) && !(newNodesMap.get(projectBaseVersionList.get(i)).contains(node))) {
                                MyEdge edge = new MyEdge(keyClassName, currentAccessedClassesMapKeyName);
                                for (MyEdge edgeN : accessedEdgesMapEvolution.get(projectBaseVersionList.get(i)).get(keyClassName)) {
                                    if (edgeN.equals2(edge)) {
                                        edgesToNewClassesForAVersion.add(edgeN);
                                        counter++;
                                        break;
                                        //System.out.println(edgeN);
                                    }
                                }
                            }
                        }
                    }
                    /*if(!edgesToNewClasses.isEmpty())
                     edgesToNewClassesPerVersion.put(currentAccessedClassesMapKeyName, edge)sToNewClasses);*/
                }
            }
            edgesToNewClassesPerVersion.put(projectBaseVersionList.get(i), edgesToNewClassesForAVersion);
            this.versionToProjectDataMap.get(projectBaseVersionList.get(i)).setEdgesToNew(counter);
            counter = 0;
        }
        return edgesToNewClassesPerVersion;
    }

    private Map<Version, Set<MyEdge>> edgesToExistingNodes() {
        edgesNumberInTotalFronNewNodesFrequencyMap = new TreeMap<Integer, Integer>();
        Map<Version, Set<MyEdge>> edgesToOldClassesForEachVersion = new LinkedHashMap<Version, Set<MyEdge>>();
        for (int i = 1; i < projectBaseVersionList.size(); i++) {
            Map<Integer, Integer> existingNodeInDegreeToNewEdgesAttractedFrequencyMap = new TreeMap<Integer, Integer>();
            int numberOfEdgesFromNewToExistingClassesForAVersion = 0;
            //	System.out.println("VERSION: " +  projectVersionList.get(i).getKey());
            Set<MyEdge> edgesToExistingClassesForAVersion = new LinkedHashSet<MyEdge>();
            Map<String, Set<String>> previousAccessedClassesMap = accessedClassesMapEvolution.get(projectBaseVersionList.get(i - 1));
            Map<String, Set<String>> currentAccessedClassesMap = accessedClassesMapEvolution.get(projectBaseVersionList.get(i));
            Set<MyNode> newClasses = newNodesMap.get(projectBaseVersionList.get(i));
            for (MyNode newClass : newClasses) {
                Set<String> newClassFriends = currentAccessedClassesMap.get(newClass.getName());
                Set<String> friendsOfTheNewClassThatAreExistingClasses = new LinkedHashSet<String>();
                for (String friendOfTheNewClass : newClassFriends) {
                    if (previousAccessedClassesMap.keySet().contains(friendOfTheNewClass) || previousAccessedClassesMap.entrySet().contains(friendOfTheNewClass)) {
                        friendsOfTheNewClassThatAreExistingClasses.add(friendOfTheNewClass);
                        MyEdge edge = new MyEdge(newClass.getName(), friendOfTheNewClass);
                        MyGraph graph = projectGraphEvolution.get(projectBaseVersionList.get(i));
                        MyNode existingClass = graph.getNode(friendOfTheNewClass);
                        int inDegree = graph.inDegree(existingClass);
                        /*edgesToExistingClassesForAVersion.add(edge);
                         counter1++;*/
                        for (MyEdge edgeN : accessedEdgesMapEvolution.get(projectBaseVersionList.get(i)).get(newClass.getName())) {
                            if (edgeN.equals2(edge)) {
                                edgesToExistingClassesForAVersion.add(edgeN);
                                numberOfEdgesFromNewToExistingClassesForAVersion++;
                                updateFrequencyMap(existingNodeInDegreeToNewEdgesAttractedFrequencyMap, inDegree);
                                updateNodeCounters(newClass, existingClass);
                                break;
                                //System.out.println(edgeN.getFrom() + " -> " + edgeN.getTo() + " type: " + edgeN.getFriendship());
                            }
                        }
                    }
                }
                /*if(!friendsOfTheNewClassThatAreExistingClasses.isEmpty())
                 edgesToExistingClassesForAVersion.put(newClass, friendsOfTheNewClassThatAreExistingClasses);*/
            }
            edgesToOldClassesForEachVersion.put(projectBaseVersionList.get(i), edgesToExistingClassesForAVersion);

            updateFrequencyMap(edgesNumberInTotalFronNewNodesFrequencyMap, numberOfEdgesFromNewToExistingClassesForAVersion);
            versionToProjectDataMap.get(projectBaseVersionList.get(i)).setEdgesToExisting(numberOfEdgesFromNewToExistingClassesForAVersion);
            numberOfEdgesFromNewToExistingClassesForAVersion = 0;
            projectGraphEvolution.get(projectBaseVersionList.get(i)).setExistingNodeInDegreeToNewEdgesAttractedFrequencyMap(existingNodeInDegreeToNewEdgesAttractedFrequencyMap);
        }
        return edgesToOldClassesForEachVersion;
    }

    private Map<Version, Set<MyEdge>> edgesFromExistingNodes() {
        int edgesCreatedInThisVersion = 0;
        existingNodeNewEdgeNumberToFrequencyMap = new TreeMap<Integer, Integer>();
        hopsThatNewEdgeSpannedToCountMap = new TreeMap<Integer, Integer>();
        Map<Version, Set<MyEdge>> edgesFromExistingClassesForEachVersion = new LinkedHashMap<Version, Set<MyEdge>>();
        for (int i = 1; i < projectBaseVersionList.size(); i++) {
            Map<Integer, Integer> existingNodeInDegreeToNewEdgesAttractedFrequencyMap = new TreeMap<Integer, Integer>();

            Set<MyEdge> edgesFromExistingClassesForAVersion = new LinkedHashSet<MyEdge>();
            Version currentVersion = projectBaseVersionList.get(i);
            Version previousVersion = projectBaseVersionList.get(i - 1);
            Set<MyNode> existingNodes = new LinkedHashSet<MyNode>();
            existingNodes.addAll(this.projectGraphEvolution.get(currentVersion).getVertices());
            existingNodes.retainAll(this.projectGraphEvolution.get(previousVersion).getVertices());

            for (MyNode existingSourceNode : existingNodes) {
                NodeBehavior existingNodeBehavior = new NodeBehavior(existingSourceNode);
                for (MyEdge edge : projectGraphEvolution.get(currentVersion).getIncidentEdges(existingSourceNode)) {
                    if (existingNodes.contains(edge.getSourceNode()) /*&& existingNodes.contains(edge.getTargetNode())*/) {
                        //If this edge did not exist in the previous version...
                        MyGraph prevGraph = projectGraphEvolution.get(previousVersion);
                        boolean found = false;
                        for (MyEdge graphEdge : prevGraph.getEdges()) {
                            if (graphEdge.equals(edge)) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            if (!edgesFromExistingClassesForAVersion.contains(edge)) {
                                if (edge.getSourceNode().equals(existingSourceNode)) {
                                    edgesFromExistingClassesForAVersion.add(edge);
                                    edgesCreatedInThisVersion++;
                                    MyPackage targetPackage = edge.getTargetNode().getPackage();
                                    existingNodeBehavior.addPackageAttached(targetPackage);
                                    MyNode targetNode = edge.getTargetNode();
                                    updateNodeCounters(edge.getSourceNode(), targetNode);
                                    //Distance of these two nodes, before the initiation of this edge:
                                    int nodeDistanceThatWasSpanned = 0;
                                    int age = targetNode.getAge();
                                    if (age > 0) {
                                        updateFrequencyMap(ageToNewIncomingEdgeCountMap, age);
                                        nodeDistanceThatWasSpanned = prevGraph.shortestPathLengthBetween(existingSourceNode, targetNode);
                                        int inDegree = projectGraphEvolution.get(currentVersion).inDegree(targetNode);
                                        updateFrequencyMap(existingNodeInDegreeToNewEdgesAttractedFrequencyMap, inDegree);
                                    }
                                    updateFrequencyMap(hopsThatNewEdgeSpannedToCountMap, nodeDistanceThatWasSpanned);
                                }
                            }
                        }
                    }
                }
                if (existingNodeBehavior.getPackagesAttached().size() > 0) {
                    existingNodeBehaviors.addBehavior(existingNodeBehavior);
                    int age = existingSourceNode.getAge();
                    if (age > 0) {
                        updateFrequencyMap(ageOfSourceNodeThatCreatesOutgoingEdgeToCounterMap, age);
                        updateFrequencyMap(ageToNewOutgoingEdgeCountMap, age);
                    }
                }
            }
            edgesFromExistingClassesForEachVersion.put(projectBaseVersionList.get(i), edgesFromExistingClassesForAVersion);
            this.versionToProjectDataMap.get(projectBaseVersionList.get(i)).setEdgesBtwnExisting(edgesCreatedInThisVersion);
            if (edgesCreatedInThisVersion >= 0) {
                updateFrequencyMap(existingNodeNewEdgeNumberToFrequencyMap, edgesCreatedInThisVersion);
            }
            edgesCreatedInThisVersion = 0;
            projectGraphEvolution.get(currentVersion).setExistingNodeInDegreeToNewEdgesAttractedFrequencyMap(existingNodeInDegreeToNewEdgesAttractedFrequencyMap);
        }
        return edgesFromExistingClassesForEachVersion;
    }

    private Map<Version, Set<MyEdge>> edgeBetweenNewNodes() {
        int edgesCreatedBetweenNewNodesInThisVersion = 0;
        newEdgeNumberBtwnNewNodesToFrequencyMap = new TreeMap<Integer, Integer>();
        Map<Version, Set<MyEdge>> edgesBetweenNewClasses = new LinkedHashMap<Version, Set<MyEdge>>();
        for (int i = 1; i < projectBaseVersionList.size(); i++) {
            Set<MyEdge> edgesBetweenNewClassesForAVersion = new LinkedHashSet<MyEdge>();
            Version currentVersion = projectBaseVersionList.get(i);
            Set<MyNode> newClassesOfThisVersion = this.newNodesMap.get(currentVersion);

            for (MyNode newClass : newClassesOfThisVersion) {
                for (MyEdge edge : projectGraphEvolution.get(currentVersion).getIncidentEdges(newClass)) {
                    if (newClassesOfThisVersion.contains(edge.getSourceNode()) && newClassesOfThisVersion.contains(edge.getTargetNode())) {
                        //If this edge did not exist in the previous version...
                        MyGraph currentGraph = projectGraphEvolution.get(currentVersion);
                        for (MyEdge graphEdge : currentGraph.getEdges()) {
                            if (graphEdge.equals(edge)) {
                                if (!edgesBetweenNewClassesForAVersion.contains(edge)) {
                                    edgesBetweenNewClassesForAVersion.add(edge);
                                    edgesCreatedBetweenNewNodesInThisVersion++;

                                    MyNode sourceNode = edge.getSourceNode();
                                    MyNode targetNode = edge.getTargetNode();
                                    updateNodeCounters(sourceNode, targetNode);
                                }
                            }
                        }
                    }
                }
            }
            edgesBetweenNewClasses.put(projectBaseVersionList.get(i), edgesBetweenNewClassesForAVersion);
            if (edgesCreatedBetweenNewNodesInThisVersion >= 0) {
                updateFrequencyMap(newEdgeNumberBtwnNewNodesToFrequencyMap, edgesCreatedBetweenNewNodesInThisVersion);
            }
            this.versionToProjectDataMap.get(projectBaseVersionList.get(i)).setEdgesBtwnNew(edgesCreatedBetweenNewNodesInThisVersion);
            edgesCreatedBetweenNewNodesInThisVersion = 0;
        }
        return edgesBetweenNewClasses;
    }

    public void printPackagesInfo() {
        System.out.println("Package Name\tIncoming Edges");
        for (MyPackage mp : packageToTotalIncomingEdgesMap.keySet()) {
            System.out.println(mp.getName() + "\t" + packageToTotalIncomingEdgesMap.get(mp));
        }
        System.out.println("\nPackage Name\tOutgoing Edges");
        for (MyPackage mp : packageToTotalOutgoingEdgesMap.keySet()) {
            System.out.println(mp.getName() + "\t" + packageToTotalOutgoingEdgesMap.get(mp));
        }
    }

    public List<Version> getBaseProjectVersionList() {
        return projectBaseVersionList;
    }

    public Map<Version, DBGraph> getVersionToDBGraphMap() {
        return versionToProjectDataMap;
    }

    public void putDBGraphForAVersion(Version pv, DBGraph pData) {
        versionToProjectDataMap.put(pv, pData);
    }

    public Map<Version, Map<String, Set<String>>> getAccessedClassesMapEvolution() {
        return accessedClassesMapEvolution;
    }

    public String getCode(Version projectVersion) {
        return null;
    }

    public Map<Version, Set<MyNode>> getNewClassesMap() {
        return newNodesMap;
    }

    public Map<Version, Set<MyNode>> getExistingClassesMap() {
        return existingNodesMap;
    }

    public Map<Version, MyGraph> getProjectGraphEvolution() {
        return projectGraphEvolution;
    }

    public Map<Integer, Integer> getExistingNodeOutDegreeToNewEdgeCountMap() {
        return existingNodeOutDegreeToNewOutgoingEdgesMap;
    }

    public Map<Integer, Integer> getExistingNodeNewEdgeNumberToFrequencyMap() {
        return existingNodeNewEdgeNumberToFrequencyMap;
    }

    public void setProjectVersionList(ArrayList<Version> projectVersionList) {
        this.projectBaseVersionList = projectVersionList;
    }

    public double getAverageNumberOfNewEdgesFromExistingNodes() {
        return averageNumberOfNewEdgesFromExistingNodes;
    }

    public double getAverageNumberOfNewEdgesFromNewToNewNodes() {
        return averageNumberOfNewEdgesFromNewToNewNodes;
    }

    public Map<Integer, Integer> getAgeOfSourceNodeThatCreatesOutgoingEdgeToCounterMap() {
        return ageOfSourceNodeThatCreatesOutgoingEdgeToCounterMap;
    }

    public Map<Integer, Integer> getAgeToNewIncomingEdgeCountMap() {
        return ageToNewIncomingEdgeCountMap;
    }

    public Map<Integer, Integer> getEdgesFromANewToExistingNodesFrequencyMap() {
        return edgesNumberFronANewToExistingNodesFrequencyMap;
    }

    public Map<Integer, Integer> getEdgesFromANewToOtherNewNodesFrequencyMap() {
        return edgesFronANewToOtherNewNodesFrequencyMap;
    }

    public NodeBehaviorHistory getNewNodeBehaviors() {
        return newNodeBehaviors;
    }

    public NodeBehaviorHistory getExistingNodeBehaviors() {
        return existingNodeBehaviors;
    }

    public Map<Integer, Integer> getAgeToNewOutgoingEdgeCountMap() {
        return ageToNewOutgoingEdgeCountMap;
    }

    public Map<Integer, Integer> getNewEdgeNumberBtwnNewNodesToFrequencyMap() {
        return newEdgeNumberBtwnNewNodesToFrequencyMap;
    }

    public Map<Integer, Integer> getInDegreeToDeletedNodeCountMap() {
        return inDegreeToDeletedNodeCountMap;
    }

    public Map<Integer, Integer> getOutDegreeToDeletedNodeCountMap() {
        return outDegreeToDeletedNodeCountMap;
    }

    public Map<Integer, Integer> getAgeToDeletedNodeCountMap() {
        return ageToDeletedNodeCountMap;
    }

    public Map<Integer, Integer> getDeletedEdgesNumberToFrequencyMap() {
        return deletedEdgesNumberToFrequencyMap;
    }

    public Map<Integer, Integer> getInDegreeOfDestinationNodeToDeletedEdgesCountMap() {
        return inDegreeOfDestinationNodeToDeletedEdgesCountMap;
    }

    public Map<Integer, Integer> getOutDegreeOfSourceNodeToDeletedEdgesCountMap() {
        return outDegreeOfSourceNodeToDeletedEdgesCountMap;
    }

    public Map<Integer, Integer> getHopsThatNewEdgeSpannedToCountMap() {
        return hopsThatNewEdgeSpannedToCountMap;
    }

    public Map<Integer, Integer> getEdgesNumberInTotalFronNewNodesFrequencyMap() {
        return edgesNumberInTotalFronNewNodesFrequencyMap;
    }

    public Map<Integer, Integer> getNewNodeCountFrequencyMap() {
        return newNodeCountFrequencyMap;
    }

    public int getAverageNumberOfNewClassesInAllVersions() {
        int num = 0;
        for (int i = 1; i < projectBaseVersionList.size(); i++) {
            Version currentVersion = projectBaseVersionList.get(i);
            Version previousVersion = projectBaseVersionList.get(i - 1);
            MyGraph currentGraph = projectGraphEvolution.get(currentVersion);
            MyGraph previousGraph = projectGraphEvolution.get(previousVersion);
            int currentNodeNum = currentGraph.getVertices().size();
            int previousNodeNum = previousGraph.getVertices().size();
            num += currentNodeNum - previousNodeNum;
        }
        num = num / (projectBaseVersionList.size() - 1);
        return num;
    }

    private void updateNodeCounters(MyNode sourceNode, MyNode targetNode) {
        totalEdgesAdded++;
        if (sourceNode.isAbstractOrInterface()) {
            edgesFromSuperclasses++;
            //	System.out.println("edges From Superclasses increased for edge: "+sourceNode+" -> "+targetNode);
        } else if (sourceNode.isSubclass()) {
            edgesFromSubclasses++;
            //	System.out.println("edges From subclasses increased for edge: "+sourceNode+" -> "+targetNode);
        }

        if (targetNode.isAbstractOrInterface()) {
            edgesToSuperclasses++;
            //	System.out.println("edges TO Superclasses increased for edge: "+sourceNode+" -> "+targetNode);
        } else if (targetNode.isSubclass()) {
            edgesToSubclasses++;
            //	System.out.println("edges TO Subclasses increased for edge: "+sourceNode+" -> "+targetNode);
        }

        if (targetNode.isSubclass()) {
            ArrayList<MyNode> anscestors = new ArrayList<MyNode>();
            targetNode.getAnscestors(anscestors);
            if (anscestors.contains(sourceNode)) {
                edgesFromSuperclassesToSubclassesOfTheSameHierarchy++;
            }
        }
    }

    public void createNodeEdgeEvolutionCharts() {
        ArrayList<Entry<String, Integer>> nodeNumberEvolution = new ArrayList<Entry<String, Integer>>();
        ArrayList<Entry<String, Integer>> edgeNumberEvolution = new ArrayList<Entry<String, Integer>>();
        ArrayList<Entry<String, Integer>> edgesToNewNodesNumberEvolution = new ArrayList<Entry<String, Integer>>();
        ArrayList<Entry<String, Integer>> edgesToExistingNodesNumberEvolution = new ArrayList<Entry<String, Integer>>();
        ArrayList<Entry<String, Integer>> edgesBtwnExistingNodesNumberEvolution = new ArrayList<Entry<String, Integer>>();
        ArrayList<Entry<String, Integer>> edgesBtwnNewNodesNumberEvolution = new ArrayList<Entry<String, Integer>>();
        ArrayList<Entry<String, Integer>> deletedEdgesNumberEvolution = new ArrayList<Entry<String, Integer>>();

        for (Version version : versionToProjectDataMap.keySet()) {
            DBGraph pData = versionToProjectDataMap.get(version);
            int nodes = pData.getNodes();
            int edges = pData.getEdges();
            int edgesToNewNodes = pData.getEdgesToNew();
            int edgesToExisting = pData.getEdgesToExisting();
            int edgesBtwnExisting = pData.getEdgesBtwnExisting();
            int edgesBtwnNew = pData.getEdgesBtwnNew();
            int deletedEdges = pData.getDeletedEdges();

            nodeNumberEvolution.add(new AbstractMap.SimpleEntry<String, Integer>(version.toString(), nodes));
            edgeNumberEvolution.add(new AbstractMap.SimpleEntry<String, Integer>(version.toString(), edges));
            edgesToNewNodesNumberEvolution.add(new AbstractMap.SimpleEntry<String, Integer>(version.toString(), edgesToNewNodes));
            edgesToExistingNodesNumberEvolution.add(new AbstractMap.SimpleEntry<String, Integer>(version.toString(), edgesToExisting));
            edgesBtwnExistingNodesNumberEvolution.add(new AbstractMap.SimpleEntry<String, Integer>(version.toString(), edgesBtwnExisting));
            edgesBtwnNewNodesNumberEvolution.add(new AbstractMap.SimpleEntry<String, Integer>(version.toString(), edgesBtwnNew));
            deletedEdgesNumberEvolution.add(new AbstractMap.SimpleEntry<String, Integer>(version.toString(), deletedEdges));
        }
        Utilities.createChartFromMap4(nodeNumberEvolution, "Evolution of Nodes", "nodes", "Version", "#Nodes");
        Utilities.createChartFromMap4(edgeNumberEvolution, "Evolution of Edges", "edges", "Version", "#Edges");
        Utilities.createChartFromMap4(edgesToNewNodesNumberEvolution, "Edges to New nodes", "freq", "Version", "#Edges to New");
        Utilities.createChartFromMap4(edgesToExistingNodesNumberEvolution, "Edges to Existing nodes", "freq", "Version", "#Edges to Existing");
        Utilities.createChartFromMap4(edgesBtwnExistingNodesNumberEvolution, "Edges to Existing nodes", "freq", "Version", "#Edges Btwn Existing");
        Utilities.createChartFromMap4(edgesBtwnNewNodesNumberEvolution, "Edges to Existing nodes", "freq", "Version", "#Edges Btwn New");
        Utilities.createChartFromMap4(deletedEdgesNumberEvolution, "Deleted Edges evolution", "freq", "Version", "#Deleted Edges");
    }

    public void printSummaryReport() {
        System.out.println("Project " + SelectedProjectName.getInstance().getSelectedProjectName());
        System.out.println("Edges to superclasses: " + edgesToSuperclasses);
        System.out.println("Edges to subclasses: " + edgesToSubclasses);
        System.out.println("Edges from superclasses: " + edgesFromSuperclasses);
        System.out.println("Edges from subclasses: " + edgesFromSubclasses);
        System.out.println("Edges From Superclasses To Subclasses Of The Same Hierarchy " + edgesFromSuperclassesToSubclassesOfTheSameHierarchy);
        double div1 = 0, div2 = 0;
        if (totalEdgesAdded > 0) {
            div1 = 1.0 * edgesFromSuperclasses / totalEdgesAdded;
            div2 = 1.0 * edgesFromSuperclassesToSubclassesOfTheSameHierarchy / totalEdgesAdded;
        }
        System.out.println("Edges from super / total edges = " + div1);
        System.out.println("Edges from super to subclass of the same hierarchy / (total edges) = " + div2);

        System.out.println("Total Edges added " + totalEdgesAdded);
        int sum = 0;
        for (Integer i : deletedEdgesNumberToFrequencyMap.keySet()) {
            sum += i * deletedEdgesNumberToFrequencyMap.get(i);
        }
        System.out.println("Total Edges Deleted " + sum);
     //   Utilities.createChartFromMap3(ageToNewIncomingEdgeCountMap, "Distribution of node age to new incoming edges", "ageVsIncomingEdges", "Node Age", "New Incoming Edges");
     //   Utilities.createChartFromMap3(ageToNewOutgoingEdgeCountMap, "Distribution of node age to new outgoing edges", "ageVsOutgoingEdges", "Node Age", "New Outgoing Edges");
    //    createNodeEdgeEvolutionCharts();
    }

    public MyGraph[] getGraphsInArray() {
        MyGraph[] graphs = new MyGraph[projectGraphEvolution.size()];
        int i = 0;
        for (Version pv : projectGraphEvolution.keySet()) {
            graphs[i++] = projectGraphEvolution.get(pv);
        }
        return graphs;
    }

    private static String[] calculateClassesWithMaxInDegreeDifference(Map<Version, MyGraph> projectGraphEvolution, int howManyClasses) {
        int versions = projectGraphEvolution.keySet().size();
        Version firstVersion = (Version) projectGraphEvolution.keySet().toArray()[0];
        Version lastVersion = (Version) projectGraphEvolution.keySet().toArray()[versions - 1];
        Map<Integer, String> degreeDifferenceMap = new TreeMap<Integer, String>();
        MyGraph firstGraph = projectGraphEvolution.get(firstVersion);
        MyGraph lastGraph = projectGraphEvolution.get(lastVersion);
        for (MyNode node : firstGraph.getVertices()) {
            MyNode nodeInLastVersion = lastGraph.getNode(node.getName());
            if (nodeInLastVersion != null) {
                int degreeDiff = lastGraph.inDegree(nodeInLastVersion) - firstGraph.inDegree(node);
                degreeDifferenceMap.put(degreeDiff, node.getName());
            }
        }
        Integer[] degrees = degreeDifferenceMap.keySet().toArray(new Integer[1]);
        if (howManyClasses > degrees.length) {
            howManyClasses = degrees.length;
        }
        String[] classNames = new String[howManyClasses];
        int k = 0;
        for (int i = degrees.length - 1; i > degrees.length - howManyClasses; i--) {
            classNames[k] = degreeDifferenceMap.get(degrees[i]);
            k++;
        }
        return classNames;
    }

    public void calculateTopDegreeClassesHistorical(Map<Version, MyGraph> projectGraphEvolution,String fileName, String evolutionType, int howManyClasses) throws Exception {
        String[] selectedNodes = calculateClassesWithMaxInDegreeDifference(projectGraphEvolution, howManyClasses);
        final int NUM_ROWS = howManyClasses + 2;
        final int FIRST_DATA_ROW = 2;
        final int HEADER_ROW = 1;
        int rows = NUM_ROWS;
        int cols = projectGraphEvolution.keySet().size() + 1;
        String[][] tableToPrint = new String[rows][cols];
        tableToPrint[0][0] = "Evolution Type: " + evolutionType + " In Degrees:";
        tableToPrint[1][0] = "Class Name";
        int column = 1;
        int row = FIRST_DATA_ROW;
        for (String node : selectedNodes) {
            tableToPrint[row][0] = node;
            row++;
        }
        row = FIRST_DATA_ROW;
        for (Version version : projectGraphEvolution.keySet()) {
            tableToPrint[HEADER_ROW][column] = "" + version;
            MyGraph graph = projectGraphEvolution.get(version);

            for (String node : selectedNodes) {
                MyNode thisVersionNode = graph.getNode(node);
                if (thisVersionNode != null) {
                    int inDegree = graph.inDegree(thisVersionNode);
                    tableToPrint[row][column] = "" + inDegree;
                } else {
                    tableToPrint[row][column] = "-";
                }
                row++;
            }
            column++;
            row = FIRST_DATA_ROW;
        }
        StorageManager.getInstance().saveTableToFile(fileName, tableToPrint);
    }

}
