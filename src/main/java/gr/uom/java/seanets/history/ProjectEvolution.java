package gr.uom.java.seanets.history;

import gr.uom.java.seanets.repoHandler.ProjectInfo;
import gr.uom.java.seanets.util.SEANetsProperties;
import gr.uom.java.seanets.db.persistence.Version;
import gr.uom.java.seanets.ejb.MetricUpdater;
import gr.uom.java.seanets.graph.MyJavaProject;
import gr.uom.java.seanets.gui.SimulationOptions;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

public class ProjectEvolution implements Serializable {

    private static final long serialVersionUID = 1L;
    private Map<Version, MyJavaProject> projectHistoryMap;
    private String fileName;
    private ProjectInfo projectInfo;
    private ProjectEvolutionAnalysis projectEvolutionAnalysis;
    private boolean analyzed;
    private transient final static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(ProjectEvolution.class.getName());

    public ProjectEvolution(ProjectInfo projectInfo) {
        this.projectInfo = projectInfo;
        fileName = projectInfo.getProjectLocalSourceFolder() + File.separator + projectInfo.getProjectName() + ".ser";
        projectHistoryMap = new TreeMap<Version, MyJavaProject>();
        analyzed = false;
    }

    public void addProjectForVersion(Version pv, MyJavaProject jp) {
        projectHistoryMap.put(pv, jp);
    }

    public ArrayList<Version> getBaseVersionEntries() {
        ArrayList<Version> baseVersions = new ArrayList<Version>();
        int estimationCutOff = SimulationOptions.getInstance().getCutOffValue();
        int versionToStartSimulation = (int) Math.floor(getAllVersionEntries().size() * estimationCutOff / 100) + 1;
        int counter = 0;
        for (Version projectVersion : getAllVersionEntries()) {
            if (counter < versionToStartSimulation) {
                baseVersions.add(projectVersion);
            }
            counter++;
        }
        return baseVersions;
    }

    public ArrayList<Version> getAllVersionEntries() {
        return new ArrayList<Version>(projectHistoryMap.keySet());
    }

    public ArrayList<MyJavaProject> getAllProjectEntries() {
        return new ArrayList<MyJavaProject>(projectHistoryMap.values());
    }

    public MyJavaProject getProjectForVersion(Version pv) {
        return projectHistoryMap.get(pv);
    }
    
    public void removeProjectAndVersionEntry(Version version) {
        projectHistoryMap.keySet().remove(version);
    }

    public ProjectEvolutionAnalysis getProjectEvolutionAnalysis() {
        if ((projectEvolutionAnalysis == null)) {
            projectEvolutionAnalysis = new ProjectEvolutionAnalysis(this);
        }
        return projectEvolutionAnalysis;
    }

    public void performEvolutionaryCalculations() {
        projectEvolutionAnalysis.graphEvolutionCalculations();
        projectEvolutionAnalysis.edgeAnalysis();
    }

    public boolean isEvolutionAnalyzed() {
        return analyzed;
    }

    public String getFileName() {
        return fileName;
    }

    public ProjectInfo getProjectInfo() {
        return projectInfo;
    }

    public void saveProjectEvolutionLocally() {
        try {
            File outDir = new File(SEANetsProperties.getInstance().getSeanetsFolderPath());
            if (!outDir.exists()) {
                outDir.mkdir();
            }
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.flush();
            oos.close();
            fos.close();
        } catch (IOException ioe) {
            logger.log(Level.SEVERE,null,ioe);
        }
    }

    public static ProjectEvolution loadEvolutionFromDisk(String projectName) {
        ProjectEvolution instance = null;
        String fileName = SEANetsProperties.getInstance().getProjectsFolder() + File.separator + projectName + File.separator + projectName + ".ser";
        try {
            FileInputStream fileIn = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            instance = (ProjectEvolution) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException | ClassNotFoundException e) {
            logger.log(Level.FINEST,null,e);
            return null;
        }
        return instance;
    }

}
