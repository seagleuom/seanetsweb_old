
package gr.uom.java.seanets.db.persistence;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "timeline", catalog = "seanetsdb", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBTimeline.findAll", query = "SELECT d FROM DBTimeline d"),
    @NamedQuery(name = "DBTimeline.findById", query = "SELECT d FROM DBTimeline d WHERE d.id = :id"),
    @NamedQuery(name = "DBTimeline.findByTitle", query = "SELECT d FROM DBTimeline d WHERE d.title = :title"),
    @NamedQuery(name = "DBTimeline.findByDate", query = "SELECT d FROM DBTimeline d WHERE d.date = :date")})
public class DBTimeline implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @JoinColumn(name = "pid", referencedColumnName = "pid")
    @ManyToOne(optional = false)
    private DBProject pid;

    public DBTimeline() {
    }

    public DBTimeline(Integer id) {
        this.id = id;
    }

    public DBTimeline(Integer id, String title, Date date) {
        this.id = id;
        this.title = title;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DBProject getPid() {
        return pid;
    }

    public void setPid(DBProject pid) {
        this.pid = pid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBTimeline)) {
            return false;
        }
        DBTimeline other = (DBTimeline) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seanets.db.persistence.DBTimeline[ id=" + id + " ]";
    }

}
