package gr.uom.java.seanets.db.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author teo
 */
@Entity
@Table(name = "project", catalog = "seanetsdb", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBProject.findAll", query = "SELECT d FROM DBProject d"),
    @NamedQuery(name = "DBProject.deleteByPid", query = "DELETE FROM DBProject d WHERE d.pid = :pid"),
    @NamedQuery(name = "DBProject.findByPid", query = "SELECT d FROM DBProject d WHERE d.pid = :pid"),
    @NamedQuery(name = "DBProject.findByName", query = "SELECT d FROM DBProject d WHERE d.name = :name"),
    @NamedQuery(name = "DBProject.findByVersions", query = "SELECT d FROM DBProject d WHERE d.versions = :versions"),
    @NamedQuery(name = "DBProject.findByGithubpath", query = "SELECT d FROM DBProject d WHERE d.githubpath = :githubpath"),
    @NamedQuery(name = "DBProject.findByDescription", query = "SELECT d FROM DBProject d WHERE d.description = :description"),
    @NamedQuery(name = "DBProject.findByAuthor", query = "SELECT d FROM DBProject d WHERE d.author = :author"),
    @NamedQuery(name = "DBProject.findByPimage", query = "SELECT d FROM DBProject d WHERE d.pimage = :pimage"),
    @NamedQuery(name = "DBProject.findByAgeVSnewOutEdges", query = "SELECT d FROM DBProject d WHERE d.ageVSnewOutEdges = :ageVSnewOutEdges"),
    @NamedQuery(name = "DBProject.findByAgeVSnewIncomingEdges", query = "SELECT d FROM DBProject d WHERE d.ageVSnewIncomingEdges = :ageVSnewIncomingEdges")})

public class DBProject implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pid")
    private Collection<DBTimeline> dBTimelineCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pid")
    private Integer pid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "versions")
    private int versions;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "githubpath")
    private String githubpath;

    @Size(min = 1, max = 200)
    @Column(name = "description")
    private String description;

    @Size(min = 1, max = 20)
    @Column(name = "author")
    private String author;

    @Size(min = 1, max = 100)
    @Column(name = "pimage")
    private String pimage;

    @Lob
    @Column(name = "ageVSnewOutEdges")
    private String ageVSnewOutEdges;

    @Lob
    @Column(name = "ageVSnewIncomingEdges")
    private String ageVSnewIncomingEdges;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pid")
    private Collection<Version> versionCollection;

    public DBProject() {
    }

    public DBProject(Integer pid) {
        this.pid = pid;
    }

    public DBProject(Integer pid, String name, int versions, String githubpath, String description, String author, String pimage) {
        this.pid = pid;
        this.name = name;
        this.versions = versions;
        this.githubpath = githubpath;
        this.description = description;
        this.author = author;
        this.pimage = pimage;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersions() {
        return versions;
    }

    public void setVersions(int versions) {
        this.versions = versions;
    }

    public String getGithubpath() {
        return githubpath;
    }

    public void setGithubpath(String githubpath) {
        this.githubpath = githubpath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPimage() {
        return pimage;
    }

    public void setPimage(String pimage) {
        this.pimage = pimage;
    }

    public String getAgeVSnewOutEdges() {
        return ageVSnewOutEdges;
    }

    public void setAgeVSnewOutEdges(String ageVSnewOutEdges) {
        this.ageVSnewOutEdges = ageVSnewOutEdges;
    }

    public String getAgeVSnewIncomingEdges() {
        return ageVSnewIncomingEdges;
    }

    public void setAgeVSnewIncomingEdges(String ageVSnewIncomingEdges) {
        this.ageVSnewIncomingEdges = ageVSnewIncomingEdges;
    }

    @XmlTransient
    public Collection<Version> getVersionCollection() {
        return versionCollection;
    }

    public void setVersionCollection(Collection<Version> versionCollection) {
        this.versionCollection = versionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pid != null ? pid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBProject)) {
            return false;
        }
        DBProject other = (DBProject) object;
        if ((this.pid == null && other.pid != null) || (this.pid != null && !this.pid.equals(other.pid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return pid + " - "+ name;
    }

    @XmlTransient
    public Collection<DBTimeline> getDBTimelineCollection() {
        return dBTimelineCollection;
    }

    public void setDBTimelineCollection(Collection<DBTimeline> dBTimelineCollection) {
        this.dBTimelineCollection = dBTimelineCollection;
    }

}
