package gr.uom.java.seanets.db.persistence;

import gr.uom.java.seanets.graph.MyGraph;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author teo
 */
@Entity
@Table(name = "graph", catalog = "seanetsdb", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBGraph.findAll", query = "SELECT d FROM DBGraph d"),
    @NamedQuery(name = "DBGraph.findByGid", query = "SELECT d FROM DBGraph d WHERE d.gid = :gid"),
    @NamedQuery(name = "DBGraph.findByVid", query = "SELECT d FROM DBGraph d WHERE d.vid = :vid"),
    @NamedQuery(name = "DBGraph.findByNodes", query = "SELECT d FROM DBGraph d WHERE d.nodes = :nodes"),
    @NamedQuery(name = "DBGraph.findByEdges", query = "SELECT d FROM DBGraph d WHERE d.edges = :edges"),
    @NamedQuery(name = "DBGraph.findByCc", query = "SELECT d FROM DBGraph d WHERE d.cc = :cc"),
    @NamedQuery(name = "DBGraph.findByDiameter", query = "SELECT d FROM DBGraph d WHERE d.diameter = :diameter"),
    @NamedQuery(name = "DBGraph.findByEdgesToNew", query = "SELECT d FROM DBGraph d WHERE d.edgesToNew = :edgesToNew"),
    @NamedQuery(name = "DBGraph.findByEdgesBtwnExisting", query = "SELECT d FROM DBGraph d WHERE d.edgesBtwnExisting = :edgesBtwnExisting"),
    @NamedQuery(name = "DBGraph.findByEdgesBtwnNew", query = "SELECT d FROM DBGraph d WHERE d.edgesBtwnNew = :edgesBtwnNew"),
    @NamedQuery(name = "DBGraph.findByDeletedEdges", query = "SELECT d FROM DBGraph d WHERE d.deletedEdges = :deletedEdges"),
    @NamedQuery(name = "DBGraph.findByEdgesToExisting", query = "SELECT d FROM DBGraph d WHERE d.edgesToExisting = :edgesToExisting")})
public class DBGraph implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gid")
    private Integer gid;

    @Column(name = "nodes")
    private int nodes;

    @Column(name = "edges")
    private int edges;

    @Column(name = "cc")
    private double cc;

    @Column(name = "diameter")
    private double diameter;
    
    @Column(name = "density")
    private double density;
    
    @Column(name = "alpha")
    private double alpha;
    
    @Column(name = "averageDegree")
    private double averageDegree;

    @Column(name = "edgesToNew")
    private int edgesToNew;

    @Column(name = "edgesBtwnExisting")
    private int edgesBtwnExisting;

    @Column(name = "edgesBtwnNew")
    private int edgesBtwnNew;

    @Column(name = "deletedEdges")
    private int deletedEdges;

    @Column(name = "edgesToExisting")
    private int edgesToExisting;

    @Lob
    @Column(name = "friendships")
    private String friendships;

    @Lob
    @Column(name = "degreeVScc")
    private String degreeVScc;

    @Lob
    @Column(name = "degreeVScount")
    private String degreeVScount;

    @Lob
    @Column(name = "hopsVScount")
    private String hopsVScount;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gid")
    private List<DBNode> dBNodesCollection;
    
    @JoinColumn(name = "vid", referencedColumnName = "vid")
    @OneToOne(optional = false)
    private Version vid;

    @Lob
    @Column(name = "graph")
    private MyGraph graph;
    
    public DBGraph() {
    }

    public DBGraph(Integer gid) {
        this.gid = gid;
    }
    
    public DBGraph(Version version, MyGraph graph) {
        this.vid = version;
        this.graph = graph;
        this.nodes = graph.getVertexCount();
        this.edges = graph.getEdgeCount();
        this.cc = graph.getClusteringCoefficient();
        this.diameter = graph.getDiameter();
        this.friendships = graph.getFriendships();
        this.degreeVScount = graph.getDegreeVSCcountMapString();
        this.degreeVScc = graph.getInDegreeVSClusteringCoefficientMapString();
        this.hopsVScount = graph.getHopsFrequenciesMapString();
        dBNodesCollection = new ArrayList<>();
    }

    public DBGraph(Integer gid, int nodes, int edges, double cc, 
            double diameter, int edgesToNew, 
            int edgesBtwnExisting, int edgesBtwnNew, int deletedEdges, int edgesToExisting, 
            String friendships, String degreeVScc, String degreeVScount, String hopsVScount) {
        this.gid = gid;
        this.nodes = nodes;
        this.edges = edges;
        this.cc = cc;
        this.diameter = diameter;
        this.edgesToNew = edgesToNew;
        this.edgesBtwnExisting = edgesBtwnExisting;
        this.edgesBtwnNew = edgesBtwnNew;
        this.deletedEdges = deletedEdges;
        this.edgesToExisting = edgesToExisting;
        this.friendships = friendships;
        this.degreeVScc = degreeVScc;
        this.degreeVScount = degreeVScount;
        this.hopsVScount = hopsVScount;
    }

    public MyGraph getGraph() {
        return graph;
    }

    public void setGraph(MyGraph graph) {
        this.graph = graph;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public int getNodes() {
        return nodes;
    }

    public void setNodes(int nodes) {
        this.nodes = nodes;
    }

    public int getEdges() {
        return edges;
    }

    public void setEdges(int edges) {
        this.edges = edges;
    }

    public double getCc() {
        return cc;
    }

    public void setCc(double cc) {
        this.cc = cc;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public double getAverageDegree() {
        return averageDegree;
    }

    public void setAverageDegree(double averageDegree) {
        this.averageDegree = averageDegree;
    }

    public int getEdgesToNew() {
        return edgesToNew;
    }

    public void setEdgesToNew(int edgesToNew) {
        this.edgesToNew = edgesToNew;
    }

    public int getEdgesBtwnExisting() {
        return edgesBtwnExisting;
    }

    public void setEdgesBtwnExisting(int edgesBtwnExisting) {
        this.edgesBtwnExisting = edgesBtwnExisting;
    }

    public int getEdgesBtwnNew() {
        return edgesBtwnNew;
    }

    public void setEdgesBtwnNew(int edgesBtwnNew) {
        this.edgesBtwnNew = edgesBtwnNew;
    }

    public int getDeletedEdges() {
        return deletedEdges;
    }

    public void setDeletedEdges(int deletedEdges) {
        this.deletedEdges = deletedEdges;
    }

    public int getEdgesToExisting() {
        return edgesToExisting;
    }

    public void setEdgesToExisting(int edgesToExisting) {
        this.edgesToExisting = edgesToExisting;
    }

    public String getFriendships() {
        return friendships;
    }

    public void setFriendships(String friendships) {
        this.friendships = friendships;
    }

    public String getDegreeVScc() {
        return degreeVScc;
    }

    public void setDegreeVScc(String degreeVScc) {
        this.degreeVScc = degreeVScc;
    }

    public String getDegreeVScount() {
        return degreeVScount;
    }

    public void setDegreeVScount(String degreeVScount) {
        this.degreeVScount = degreeVScount;
    }

    public String getHopsVScount() {
        return hopsVScount;
    }

    public void setHopsVScount(String hopsVScount) {
        this.hopsVScount = hopsVScount;
    }

    @XmlTransient
    public Collection<DBNode> getdBNodesCollection() {
        return dBNodesCollection;
    }

    public void setdBNodesCollection(List<DBNode> dBNodesCollection) {
        this.dBNodesCollection = dBNodesCollection;
    }

    public Version getVid() {
        return vid;
    }

    public void setVid(Version vid) {
        this.vid = vid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gid != null ? gid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBGraph)) {
            return false;
        }
        DBGraph other = (DBGraph) object;
        if ((this.gid == null && other.gid != null) || (this.gid != null && !this.gid.equals(other.gid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seanets.db.persistence.DBGraph[ gid=" + gid + " ]";
    }
    
}
