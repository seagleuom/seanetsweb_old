
package gr.uom.java.seanets.db.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "sourcemetric", catalog = "seanetsdb", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"vid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBSourceMetric.findAll", query = "SELECT d FROM DBSourceMetric d"),
    @NamedQuery(name = "DBSourceMetric.findByMid", query = "SELECT d FROM DBSourceMetric d WHERE d.mid = :mid"),
    @NamedQuery(name = "DBSourceMetric.findByVersion", query = "SELECT d FROM DBSourceMetric d WHERE d.vid = :vid")})
public class DBSourceMetric implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "mid", nullable = false)
    private Integer mid;
    @Column(name = "cbo", precision = 22)
    private Double cbo;
    @Column(name = "lcom", precision = 22)
    private Double lcom;
    @Column(name = "nom", precision = 22)
    private Double nom;
    @Column(name = "nof", precision = 22)
    private Double nof;
    @Column(name = "wmc", precision = 22)
    private Double wmc;
    @Column(name = "loc", precision = 22)
    private Double loc;
    @JoinColumn(name = "vid", referencedColumnName = "vid", nullable = false)
    @OneToOne(optional = false)
    private Version vid;

    public DBSourceMetric() {
    }

    public DBSourceMetric(Integer mid) {
        this.mid = mid;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public Double getCbo() {
        return cbo;
    }

    public void setCbo(Double cbo) {
        this.cbo = cbo;
    }

    public Double getLcom() {
        return lcom;
    }

    public void setLcom(Double lcom) {
        this.lcom = lcom;
    }

    public Double getNom() {
        return nom;
    }

    public void setNom(Double nom) {
        this.nom = nom;
    }

    public Double getNof() {
        return nof;
    }

    public void setNof(Double nof) {
        this.nof = nof;
    }

    public Double getWmc() {
        return wmc;
    }

    public void setWmc(Double wmc) {
        this.wmc = wmc;
    }

    public Double getLoc() {
        return loc;
    }

    public void setLoc(Double loc) {
        this.loc = loc;
    }

    public Version getVid() {
        return vid;
    }

    public void setVid(Version vid) {
        this.vid = vid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mid != null ? mid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBSourceMetric)) {
            return false;
        }
        DBSourceMetric other = (DBSourceMetric) object;
        if ((this.mid == null && other.mid != null) || (this.mid != null && !this.mid.equals(other.mid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seanets.db.persistence.DBMetric[ mid=" + mid + " ]";
    }

}
