package gr.uom.java.seanets.db.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "repometric", catalog = "seanetsdb", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBRepoMetric.findAll", query = "SELECT d FROM DBRepoMetric d"),
    @NamedQuery(name = "DBRepoMetric.findByRmid", query = "SELECT d FROM DBRepoMetric d WHERE d.rmid = :rmid"),
    @NamedQuery(name = "DBRepoMetric.findByVersion", query = "SELECT d FROM DBRepoMetric d WHERE d.vid = :vid")})
public class DBRepoMetric implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rmid")
    private Integer rmid;
    @Column(name = "authors")
    private Integer authors;
    @Column(name = "commits")
    private Integer commits;
    @Column(name = "filesAdded")
    private Integer filesAdded;
    @Column(name = "filesDeleted")
    private Integer filesDeleted;
    @Column(name = "filesModified")
    private Integer filesModified;
    @Column(name = "linesAdded")
    private Integer linesAdded;
    @Column(name = "linesDeleted")
    private Integer linesDeleted;
    @Column(name = "testFilesAdded")
    private Integer testFilesAdded;
    @Column(name = "testFilesModified")
    private Integer testFilesModified;
    @JoinColumn(name = "vid", referencedColumnName = "vid")
    @ManyToOne(optional = false)
    private Version vid;

    public DBRepoMetric() {
    }

    public DBRepoMetric(Integer rmid) {
        this.rmid = rmid;
    }

    public Integer getRmid() {
        return rmid;
    }

    public void setRmid(Integer rmid) {
        this.rmid = rmid;
    }

    public Integer getAuthors() {
        return authors;
    }

    public void setAuthors(Integer authors) {
        this.authors = authors;
    }

    public Integer getCommits() {
        return commits;
    }

    public void setCommits(Integer commits) {
        this.commits = commits;
    }

    public Integer getFilesAdded() {
        return filesAdded;
    }

    public void setFilesAdded(Integer filesAdded) {
        this.filesAdded = filesAdded;
    }

    public Integer getFilesDeleted() {
        return filesDeleted;
    }

    public void setFilesDeleted(Integer filesDeleted) {
        this.filesDeleted = filesDeleted;
    }

    public Integer getFilesModified() {
        return filesModified;
    }

    public void setFilesModified(Integer filesModified) {
        this.filesModified = filesModified;
    }

    public Integer getLinesAdded() {
        return linesAdded;
    }

    public void setLinesAdded(Integer linesAdded) {
        this.linesAdded = linesAdded;
    }

    public Integer getLinesDeleted() {
        return linesDeleted;
    }

    public void setLinesDeleted(Integer linesDeleted) {
        this.linesDeleted = linesDeleted;
    }

    public Integer getTestFilesAdded() {
        return testFilesAdded;
    }

    public void setTestFilesAdded(Integer testFilesAdded) {
        this.testFilesAdded = testFilesAdded;
    }

    public Integer getTestFilesModified() {
        return testFilesModified;
    }

    public void setTestFilesModified(Integer testFilesModified) {
        this.testFilesModified = testFilesModified;
    }

    public Version getVid() {
        return vid;
    }

    public void setVid(Version vid) {
        this.vid = vid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rmid != null ? rmid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBRepoMetric)) {
            return false;
        }
        DBRepoMetric other = (DBRepoMetric) object;
        if ((this.rmid == null && other.rmid != null) || (this.rmid != null && !this.rmid.equals(other.rmid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seanets.db.persistence.DBRepoMetric[ rmid=" + rmid + " ]";
    }

}
