/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seanets.db.persistence;

import gr.uom.java.seanets.graph.MyNode;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author teo
 */
@Entity
@Table(name = "node", catalog = "seanetsdb", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBNode.findAll", query = "SELECT d FROM DBNode d"),
    @NamedQuery(name = "DBNode.findByNid", query = "SELECT d FROM DBNode d WHERE d.nid = :nid"),
    @NamedQuery(name = "DBNode.findByName", query = "SELECT d FROM DBNode d WHERE d.name = :name"),
    @NamedQuery(name = "DBNode.findByCc", query = "SELECT d FROM DBNode d WHERE d.cc = :cc"),
    @NamedQuery(name = "DBNode.findByPagerank", query = "SELECT d FROM DBNode d WHERE d.pagerank = :pagerank"),
    @NamedQuery(name = "DBNode.findByBtwncentrality", query = "SELECT d FROM DBNode d WHERE d.btwncentrality = :btwncentrality"),
    @NamedQuery(name = "DBNode.findByCbo", query = "SELECT d FROM DBNode d WHERE d.cbo = :cbo"),
    @NamedQuery(name = "DBNode.findByLcom", query = "SELECT d FROM DBNode d WHERE d.lcom = :lcom"),
    @NamedQuery(name = "DBNode.findByNom", query = "SELECT d FROM DBNode d WHERE d.nom = :nom"),
    @NamedQuery(name = "DBNode.findByWmc", query = "SELECT d FROM DBNode d WHERE d.wmc = :wmc"),
    @NamedQuery(name = "DBNode.findByGid", query = "SELECT d FROM DBNode d WHERE d.gid = :gid")})
public class DBNode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "nid")
    private Integer nid;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;

    @Column(name = "cc")
    private double cc;

    @Column(name = "pagerank")
    private double pagerank;

    @Column(name = "btwncentrality")
    private double btwncentrality;

    @Column(name = "cbo")
    private int cbo;

    @Column(name = "lcom")
    private double lcom;

    @Column(name = "nom")
    private int nom;
    
    @Column(name = "loc")
    private int loc;
    
    @Column(name = "nof")
    private int nof;

    @Column(name = "wmc")
    private double wmc;

    @JoinColumn(name = "gid", referencedColumnName = "gid")
    @OneToOne(optional = false)
    private DBGraph gid;

    public DBNode() {
    }

    public DBNode(MyNode node, DBGraph dbGraph) {
        this.name = node.getName();
        this.gid = dbGraph;
        dbGraph.getdBNodesCollection().add(this);
        this.cc = node.getClusteringCoefficient();
        this.btwncentrality = node.getBetweenessCentrality();
        this.pagerank = node.getPageRank();
    }

    public DBNode(Integer nid) {
        this.nid = nid;
    }

    public DBNode(Integer nid, String name, double cc, double pagerank, double btwncentrality, int cbo, double lcom, int nom, double wmc, int loc) {
        this.nid = nid;
        this.name = name;
        this.cc = cc;
        this.pagerank = pagerank;
        this.btwncentrality = btwncentrality;
        this.cbo = cbo;
        this.lcom = lcom;
        this.nom = nom;
        this.wmc = wmc;
        this.loc = loc;
    }

    public Integer getNid() {
        return nid;
    }

    public void setNid(Integer nid) {
        this.nid = nid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCc() {
        return cc;
    }

    public void setCc(double cc) {
        this.cc = cc;
    }

    public double getPagerank() {
        return pagerank;
    }

    public void setPagerank(double pagerank) {
        this.pagerank = pagerank;
    }

    public double getBtwncentrality() {
        return btwncentrality;
    }

    public void setBtwncentrality(double btwncentrality) {
        this.btwncentrality = btwncentrality;
    }

    public double getCbo() {
        return cbo;
    }

    public void setCbo(int cbo) {
        this.cbo = cbo;
    }

    public double getLcom() {
        return lcom;
    }

    public void setLcom(double lcom) {
        this.lcom = lcom;
    }

    public double getNom() {
        return nom;
    }

    public void setNom(int nom) {
        this.nom = nom;
    }

    public int getNof() {
        return nof;
    }

    public void setNof(int nof) {
        this.nof = nof;
    }

    public double getWmc() {
        return wmc;
    }

    public void setWmc(double wmc) {
        this.wmc = wmc;
    }

    public int getLoc() {
        return loc;
    }

    public void setLoc(int loc) {
        this.loc = loc;
    }

    public DBGraph getGid() {
        return gid;
    }

    public void setGid(DBGraph gid) {
        this.gid = gid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nid != null ? nid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBNode)) {
            return false;
        }
        DBNode other = (DBNode) object;
        if ((this.nid == null && other.nid != null) || (this.nid != null && !this.nid.equals(other.nid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seanets.db.persistence.DBNode[ nid=" + nid + " ]";
    }

}
