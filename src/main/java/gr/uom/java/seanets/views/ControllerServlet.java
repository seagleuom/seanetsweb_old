/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.uom.java.seanets.views;

import gr.uom.java.seanets.ejb.RemoteRepositoryReader;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author teo
 */
@WebServlet(name = "Controller")
public class ControllerServlet extends HttpServlet {
    
   @EJB
   RemoteRepositoryReader remoteRepositoryReader;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        String path = request.getParameter("projectpath");
        System.out.println("Requested download for: "+path);
        
        remoteRepositoryReader.readFromRemoteURL(path);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
