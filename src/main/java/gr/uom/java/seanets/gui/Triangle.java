package gr.uom.java.seanets.gui;

import java.io.Serializable;

import gr.uom.java.seanets.graph.MyEdge;

public class Triangle implements Serializable {


	private static final long serialVersionUID = 1L;
	private String coreVertex; 
	private String secondVertex;
	private String thirdVertex;

	private boolean directionSecondToThird;
	private boolean directionThirdToSecond;

	private boolean coreExtendsSecondVertex;
	private boolean coreExtendsThirdVertex;

	private MyEdge edge1;
	private MyEdge edge2;
	private MyEdge edge3;

	private boolean kleinberg;
	private boolean closed;


	public Triangle(MyEdge edge1, MyEdge edge2, MyEdge edge3){
		this.edge1 = edge1;
		this.edge2 = edge2;
		this.edge3 = edge3;

		this.coreVertex = edge1.getFrom();
		this.secondVertex = edge1.getTo();
		if(edge3!=null)
			this.thirdVertex = edge3.getTo();
		else
			this.thirdVertex = edge2.getTo();
	}



	public void setClosed(boolean closed) {
		this.closed = this.isDirectionSecondToThird() || this.isDirectionThirdToSecond();
	}

	public boolean isKleinberg() {
		return kleinberg;
	}

	public void setKleinberg(boolean kleinberg) {
		this.kleinberg = kleinberg;
	}

	public MyEdge getEdge1() {
		return edge1;
	}

	public MyEdge getEdge2() {
		return edge2;
	}

	public MyEdge getEdge3() {
		return edge3;
	}

	public boolean isDirectionSecondToThird() {
		return directionSecondToThird;
	}

	public void setDirectionSecondToThird(boolean directionSecondToThird) {
		this.directionSecondToThird = directionSecondToThird;
	}

	public boolean isDirectionThirdToSecond() {
		return directionThirdToSecond;
	}

	public void setDirectionThirdToSecond(boolean directionThirdToSecond) {
		this.directionThirdToSecond = directionThirdToSecond;
	}

	public boolean coreExtendsSecondVertex() {
		return coreExtendsSecondVertex;
	}

	public void setCoreExtendsSecondVertex(boolean coreExtendsSecondVertex) {
		this.coreExtendsSecondVertex = coreExtendsSecondVertex;
	}

	public boolean coreExtendsThirdVertex() {
		return coreExtendsThirdVertex;
	}

	public void setCoreExtendsThirdVertex(boolean coreExtendsThirdVertex) {
		this.coreExtendsThirdVertex = coreExtendsThirdVertex;
	}

	public Triangle(String coreVertex, String secondVertex, String thirdVertex) {
		this.coreVertex = coreVertex;
		this.secondVertex = secondVertex;
		this.thirdVertex = thirdVertex;

		this.directionSecondToThird = false;
		this.directionThirdToSecond = false;

		this.coreExtendsSecondVertex = false;
		this.coreExtendsThirdVertex = false;
		this.kleinberg = false;
		this.closed = false;
	}




	public String getCoreVertex() {
		return coreVertex;
	}

	public boolean isClosed() {
		return closed;
	}

	public String getSecondVertex() {
		return secondVertex;
	}

	public String getThirdVertex() {
		return thirdVertex;
	}

	public boolean equals(Object o) {
		if(this == o)
			return true;
		if(o instanceof Triangle) {
			Triangle triangle = (Triangle)o;
			return (this.coreVertex.equals(triangle.coreVertex)) &&
			(this.secondVertex.equals(triangle.secondVertex)) &&
			(this.thirdVertex.equals(triangle.thirdVertex));
		}
		return false;
	}

	public int hashCode() {
		int result = 17;
		result = 37*result + coreVertex.hashCode();
		result = 37*result + secondVertex.hashCode();
		result = 37*result + thirdVertex.hashCode();
		return result;
	}

	public void edges(){

		edge1 = new MyEdge(coreVertex, secondVertex);
		edge1.setDirection("->");
		edge2 = new MyEdge(secondVertex, thirdVertex);
		edge3 = new MyEdge(coreVertex, thirdVertex);
		edge3.setDirection("->");

		if(this.isDirectionSecondToThird()&& this.isDirectionThirdToSecond())
			edge2.setDirection("<->");

		else if(this.isDirectionSecondToThird())
			edge2.setDirection("->");

		else if(this.isDirectionThirdToSecond())
			edge2.setDirection("<-");

	}



	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (this.isDirectionSecondToThird()&& this.isDirectionThirdToSecond())
			sb.append(coreVertex + "->" + secondVertex + "<-> " + thirdVertex + " (closed...)");
		else if(this.isDirectionSecondToThird())
			sb.append(coreVertex + "->" + secondVertex + "-> " + thirdVertex + " (closed)");
		else if(this.isDirectionThirdToSecond())
			sb.append(coreVertex + "->" + secondVertex + "<- " + thirdVertex + " (closed)");
		else
			sb.append(coreVertex + "->" + secondVertex + " , " + thirdVertex + " (open)");
		if(coreExtendsSecondVertex)
			sb.append(" " + coreVertex + " extends " + secondVertex);
		if(coreExtendsThirdVertex)
			sb.append(" " + coreVertex + " extends " + thirdVertex);

		return sb.toString();
	}
}
