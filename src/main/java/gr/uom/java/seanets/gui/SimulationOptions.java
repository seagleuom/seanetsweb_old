package gr.uom.java.seanets.gui;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

public class SimulationOptions extends JPanel implements  ChangeListener {

	private static final long serialVersionUID = 1L;
	private static final int MIN = 5;
	private static final int MAX = 100;
	private static final int INITcutOff = 65; 
	private static final int INITrandomness = 5;   


	private JSlider cutOffValueSlider;
	private JSlider randomnessSlider;
	private JLabel cutoffSliderLabel, randomnessSliderLabel;
	private JCheckBox considerDomainRules;
	private JCheckBox considerNodeAge;
	private JCheckBox preserveMaxHopDistance;
	private int cutOffValue;
	private int randomness;


	private static SimulationOptions instance;

	public static SimulationOptions getInstance(){
		if(instance != null)
			return instance;
		else{
			instance = new SimulationOptions();
			return instance;
		}
	}

	private SimulationOptions() {
		super();
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		initializeComponents();

		add(considerDomainRules);
		add(considerNodeAge);
		add(preserveMaxHopDistance);
		add(cutoffSliderLabel);
		add(cutOffValueSlider);
		add(randomnessSliderLabel);
		add(randomnessSlider);
	}

	private void initializeComponents() {
		cutOffValue = INITcutOff;
		randomness = INITrandomness;
		
		considerDomainRules = new JCheckBox("Apply Domain Rules");
		considerDomainRules.setSelected(true);
		considerDomainRules.setAlignmentX(Component.LEFT_ALIGNMENT);
		considerNodeAge = new JCheckBox("Apply Node Age Rules");
		considerNodeAge.setSelected(true);
		considerNodeAge.setAlignmentX(Component.LEFT_ALIGNMENT);
		preserveMaxHopDistance = new JCheckBox("Preserve Max Hop Distance");
		preserveMaxHopDistance.setSelected(true);
		preserveMaxHopDistance.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		cutoffSliderLabel = new JLabel("Estimation Cutoff  "+cutOffValue+"%");
		cutoffSliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		cutOffValueSlider = new JSlider(JSlider.HORIZONTAL, MIN, MAX, INITcutOff);
		cutOffValueSlider.addChangeListener(this);
		cutOffValueSlider.setMajorTickSpacing(10);
		cutOffValueSlider.setMinorTickSpacing(1);
		cutOffValueSlider.setPaintTicks(true);
		cutOffValueSlider.setPaintLabels(true);
		cutOffValueSlider.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));

		randomnessSliderLabel = new JLabel("Simulation Randomness  "+randomness+"%");
		randomnessSliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		randomnessSlider = new JSlider(JSlider.HORIZONTAL, MIN, MAX, INITrandomness);
		randomnessSlider.addChangeListener(this);
		randomnessSlider.setMajorTickSpacing(10);
		randomnessSlider.setMinorTickSpacing(1);
		randomnessSlider.setPaintTicks(true);
		randomnessSlider.setPaintLabels(true);
		randomnessSlider.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if(e.getSource() == cutOffValueSlider){
			JSlider source = (JSlider)e.getSource();
			if (!source.getValueIsAdjusting()) {
				cutOffValue = (int)source.getValue();
				cutoffSliderLabel.setText("Estimation Cutoff  "+cutOffValue+"%");
			}
		}
		else{
			JSlider source = (JSlider)e.getSource();
			if (!source.getValueIsAdjusting()) {
				randomness = (int)source.getValue();
				cutoffSliderLabel.setText("Simulation Randomness  "+randomness+"%");
			}
		}
	}

	public int getCutOffValue() {
		return cutOffValue;
	}

	public boolean isPreserveDomainRulesSelected(){
		return considerDomainRules.isSelected();
	}

	public boolean isConsiderNodeAgeSelected(){
		return considerNodeAge.isSelected();
	}

	public boolean isPreserveMaxHopDistanceSelected(){
		return preserveMaxHopDistance.isSelected();
	}
}
