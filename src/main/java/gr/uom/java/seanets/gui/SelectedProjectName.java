package gr.uom.java.seanets.gui;

public class SelectedProjectName {

	private String selectedProjectName;
	private static SelectedProjectName instance;
	
	private SelectedProjectName(String aName) {
		selectedProjectName = aName;
	}

	public static SelectedProjectName getInstance(){
		if(instance == null) instance = new SelectedProjectName("Undefined");
		return instance;
	}
	
	public void setSelectedProjectName(String projectName) {
		this.selectedProjectName = projectName;
	}
	
	public String getSelectedProjectName(){
		return selectedProjectName;
	}
}
