package gr.uom.java.seanets.statistics;

import gr.uom.java.seanets.graph.Utilities;
import gr.uom.java.seanets.history.PowerFittingFunction;

import java.util.ArrayList;

import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.PowerFunction2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.statistics.Regression;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RefineryUtilities;

public class MyScatterPlotChartWithPowerFitting extends MyScatterPlotChart {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<XYDataset> regressionData = null;


	public MyScatterPlotChartWithPowerFitting(String title, String xLabel,String yLabel) {
		super(title, xLabel, yLabel, false);
		regressionData = new ArrayList<XYDataset>();
	}
	
	public MyScatterPlotChartWithPowerFitting(String title, String xLabel, String yLabel, double[][] dataTable, PowerFittingFunction cumulativeDistributionPowerFunction) {
		super(title, xLabel, yLabel, false);
		regressionData = new ArrayList<XYDataset>();
		try{
			//dataTable is already transposed here. It is given in the appropriate form
			double[] coefficients = Regression.getPowerRegression(dataTable);
			cumulativeDistributionPowerFunction.setA(coefficients[0]);
			cumulativeDistributionPowerFunction.setB(coefficients[1]);
			Function2D curve = new PowerFunction2D(coefficients[0], coefficients[1]);  
			int length = dataTable.length;
			regressionData.add(DatasetUtilities.sampleFunction2D(curve,dataTable[0][0],dataTable[length-1][0], 100, title+" Power Fitting"));   
		}
		catch(IllegalArgumentException ex){
			System.err.println("Not enough data to regress");
		}
	}

	public void addData(String dataName, double[][] dataTable1) {
		if(dataTable1!= null) {
			data.addSeries(dataName, dataTable1);
			try{
				double dataTable[][] = Utilities.transposeMatrix(dataTable1);
				double[] coefficients = Regression.getPowerRegression(dataTable);
				Function2D curve = new PowerFunction2D(coefficients[0], coefficients[1]);  
				int length = dataTable.length;
				regressionData.add(DatasetUtilities.sampleFunction2D(curve,dataTable[0][0],dataTable[length-1][0], 100, dataName+" Power Fitting"));   
			}
			catch(IllegalArgumentException ex){
				ex.printStackTrace();
			}
		}
	}

	public void prepareChart(){
		super.prepareChart();
		int i=1;
		for(XYDataset xyd : regressionData) {
			plot.setRenderer(i, new  XYLineAndShapeRenderer(true,true));
			plot.setDataset(i,xyd);
			i++;
		}
			
	}
	
//	public void displayChart() {
//		prepareChart();
//		pack();
//		RefineryUtilities.centerFrameOnScreen(this);
//		setVisible(true);
//	}

}
