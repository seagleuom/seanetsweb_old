package gr.uom.java.seanets.statistics;

public abstract class FittingFunction {
	
	protected double[][] data;
	/*data form:
	| x | y	|
	|   |	|
	|	|	|
	|	|	|
	*/
	
	public FittingFunction(double data[][]){
		this.data = data;
	}
		
	protected double getMean(double ar[][], int column){
		double sum = 0;
		for(int i=0; i< ar.length; i++){
			sum += ar[i][column];
		}
		return sum / ar.length;
	}
	
	public void setData(double[][] dataTable) {
		data = dataTable;
	}
	
	public abstract double getValue(double x);
	public abstract int getIntValue(double x);
	public abstract int getXValueForaY(double y);
	protected abstract double getRSquared();
//	public abstract void displayDistributionChart();
}
