package gr.uom.java.seanets.statistics;

import java.awt.RenderingHints;
import java.awt.Shape;


import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;

import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.util.ShapeUtilities;

public class MyScatterPlotChart {

	private static final long serialVersionUID = 1L;


	protected String xLabel, yLabel, title;
	protected JFreeChart chart;
	protected XYPlot plot;
	protected DefaultXYDataset data;
	private boolean drawLines = false;
	
	private boolean useLogAxis = false;

	public MyScatterPlotChart(String title, String xLabel, String yLabel, boolean drawLines) {
		this.title = title;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		this.drawLines = drawLines;
		data = new DefaultXYDataset();
	}

	public void prepareChart(){
		if(data != null) {
			NumberAxis domainAxis;
			NumberAxis rangeAxis;
			if(useLogAxis){
				domainAxis = new LogarithmicAxis(xLabel);
				rangeAxis = new LogarithmicAxis(yLabel);
			}
			else{
				domainAxis = new NumberAxis(xLabel);
				rangeAxis = new NumberAxis(yLabel);
				NumberAxis.createIntegerTickUnits();
			}
			domainAxis.setAutoRangeIncludesZero(false);
			rangeAxis.setAutoRangeIncludesZero(false);
		

			//XYItemRenderer xyRenderer = new  StandardXYItemRenderer(StandardXYItemRenderer.SHAPES);
			XYLineAndShapeRenderer xyRenderer = new  XYLineAndShapeRenderer(drawLines,true);
			xyRenderer.setDrawSeriesLineAsPath(true);
			try{
				plot = new XYPlot(this.data, domainAxis, rangeAxis, xyRenderer);	
				
				chart = new JFreeChart(title, plot);
				ChartPanel panel = new ChartPanel(chart, true);
				panel.setPreferredSize(new java.awt.Dimension(500, 270));
				chart.getRenderingHints().put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				
				//panel.setHorizontalZoom(true);
				//panel.setVerticalZoom(true);
				Shape cross = ShapeUtilities.createDiagonalCross(3, 1);
				//	XYItemRenderer renderer = plot.getRenderer();
				//	renderer.setSeriesShape(0, cross);
				panel.setMinimumDrawHeight(10);
				panel.setMaximumDrawHeight(2000);
				panel.setMinimumDrawWidth(20);
				panel.setMaximumDrawWidth(2000);
			}
			catch(Exception e){
				System.out.println("break");
			}
		}else{
			
		}
	}

//	public void displayChart() {
//		prepareChart();
//		pack();
//		RefineryUtilities.centerFrameOnScreen(this);
//		setVisible(true);
//	}
	
	public void setUseLogarithmicAxis(boolean useLogAxis){
		this.useLogAxis = useLogAxis;
	}

	public void addData(String dataName, double[][] n) {
		if(n!= null)
			data.addSeries(dataName, n);
	}

}
