package gr.uom.java.seanets.statistics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.Range;
import org.jfree.data.category.DefaultCategoryDataset;

public class MyDoubleLineChart extends JFrame{

	private DefaultCategoryDataset dataset;
	private DefaultCategoryDataset dataset2;
	private static final long serialVersionUID = 1L;
	private String title, xLabel, yLabel1,yLabel2;

	public MyDoubleLineChart(String title, String xLabel, String yLabel1, String yLabel2) {
		super();
		this.title = title;
		this.xLabel = xLabel;
		this.yLabel1 = yLabel1;
		this.yLabel2 = yLabel2;
		dataset = new DefaultCategoryDataset();
		dataset2 = new DefaultCategoryDataset();
	}

	public void addData1(String xLabel, int y) {
		dataset.addValue(y, yLabel1, xLabel);
	}
	
	public void addData2(String xLabel, double y) {
		dataset2.addValue(y, yLabel2, xLabel);
	}

	public void showChart() {
		// create the chart...
		final JFreeChart chart = ChartFactory.createLineChart(
				title,       // chart title
				xLabel,                    // domain axis label
				yLabel1,                   // range axis label
				dataset,                   // data
				PlotOrientation.VERTICAL,  // orientation
				true,                      // include legend
				true,                      // tooltips
				false                      // urls
				);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		//        final StandardLegend legend = (StandardLegend) chart.getLegend();
		//      legend.setDisplaySeriesShapes(true);
		//    legend.setShapeScaleX(1.5);
		//  legend.setShapeScaleY(1.5);
		//legend.setDisplaySeriesLines(true);

		chart.setBackgroundPaint(Color.white);

		final CategoryPlot plot = (CategoryPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setRangeGridlinePaint(Color.white);

		// customize the range axis...
		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		rangeAxis.setAutoRangeIncludesZero(true);
		
		
		NumberAxis axis2 = new NumberAxis(yLabel2);
		axis2.setStandardTickUnits(NumberAxis.createStandardTickUnits());
		axis2.setLabelFont(rangeAxis.getLabelFont());
		axis2.setAutoRangeIncludesZero(true);
		plot.setRangeAxis(1, axis2);
		plot.setRangeAxisLocation(1, AxisLocation.BOTTOM_OR_RIGHT);
		plot.setDataset(1, dataset2);
		plot.mapDatasetToRangeAxis(1, 1);
		
		// Customize the renderer...
		final LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();
		final LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
		
		plot.setRenderer(1, renderer2);
		//        renderer.setDrawShapes(true);

		renderer.setSeriesStroke(
				0, new BasicStroke(
						2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
						1.0f, new float[] {10.0f, 6.0f}, 0.0f
						)
				);
		renderer.setSeriesStroke(
				1, new BasicStroke(
						2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
						1.0f, new float[] {6.0f, 6.0f}, 0.0f
						)
				);
		renderer.setSeriesStroke(
				2, new BasicStroke(
						2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
						1.0f, new float[] {2.0f, 6.0f}, 0.0f
						)
				);
		// OPTIONAL CUSTOMISATION COMPLETED.

		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new Dimension(500, 270));
		setContentPane(chartPanel);
		this.pack();
		this.setSize(550,300);
		setVisible(true);
	}

}
