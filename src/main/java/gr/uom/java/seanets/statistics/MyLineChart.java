package gr.uom.java.seanets.statistics;


import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.ui.RefineryUtilities;
import org.jfree.data.category.*;


public class MyLineChart extends JFrame{

	private static final long serialVersionUID = 1L;

	private DefaultCategoryDataset dataset;
	private String xLabel, yLabel, title;
	
	public MyLineChart(String title, String xLabel, String yLabel) {
		super(title);
		this.title = title;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		dataset = new DefaultCategoryDataset();
	}
	
	public void displayChart(){
		if(dataset != null) {
			JFreeChart chart = ChartFactory.createLineChart(title,xLabel, yLabel, dataset, PlotOrientation.VERTICAL, true, true, false);
			ChartPanel chartPanel = new ChartPanel(chart);
			chartPanel.setPreferredSize(new Dimension(500, 270));
			this.setContentPane(chartPanel);
			this.pack();
			RefineryUtilities.centerFrameOnScreen(this);
			this.setVisible(true);
		}else{
			JOptionPane.showMessageDialog(this, "No data inserted!");
		}
	}
	
	public void addValue(int value, String category, String xAxisKey) {
		dataset.addValue(value, category, xAxisKey);
	}
		
}