package gr.uom.java.seanets.statistics;

import org.jfree.data.statistics.Regression;
import org.jfree.data.xy.DefaultXYDataset;

import gr.uom.java.seanets.graph.Utilities;

public class PolynomialFittingFunction extends FittingFunction {

	private double[] coefficients;
//	private MyScatterPlotChartWithPolynomialFitting chart;
	
	public PolynomialFittingFunction(double[][] data, int order, String title, String xLabel, String yLabel) {
		super(data);
		DefaultXYDataset dataSet = new DefaultXYDataset();
		dataSet.addSeries("one", Utilities.transposeMatrix(data));
		coefficients = Regression.getPolynomialRegression(dataSet, dataSet.indexOf("one"), order);
//		chart = new MyScatterPlotChartWithPolynomialFitting(this,data,coefficients, title, xLabel, yLabel) ;
	}

	@Override
	public double getValue(double x) {
		double value = 0;
		for(int i=0; i < coefficients.length-1; i++){
			value += coefficients[i] * Math.pow(x, i);
		}
		
		return value;
	}

	@Override
	public int getIntValue(double x) {
		return (int)Math.round(getValue(x));
	}

	@Override
	public int getXValueForaY(double y) {
		double step = 0.005;
		double tolerance = 0.005;
		double xLimit = data[data.length-1][0];
		double yEstim = 0;
		for(double x =0.000001; x <xLimit; x+= step) {
			yEstim = getValue(x);
			if(Math.abs(yEstim - y) < tolerance){
				return (int)Math.ceil(x);
			}
		}
		return 0;
	}

	@Override
	protected double getRSquared(){
		return coefficients[coefficients.length-1];
	}

	public double[] getCoefficients() {
		return coefficients;
	}
//
        
//	@Override
//	public void displayDistributionChart() {
//		if(chart != null) {
//			chart.prepareChart();
//			chart.displayChart();
//		}
//	}

}
