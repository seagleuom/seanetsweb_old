package gr.uom.java.seanets.statistics;

import gr.uom.java.seanets.graph.Utilities;

import java.util.ArrayList;

import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

public class MyScatterPlotChartWithPolynomialFitting extends MyScatterPlotChart {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<XYDataset> regressionData = null;
	private PolynomialFittingFunction fittingFunction;

	public MyScatterPlotChartWithPolynomialFitting(PolynomialFittingFunction ff, double[][] dataTable, double[] coefs, String title, String xLabel,String yLabel) {
		super(title, xLabel, yLabel, false);
		regressionData = new ArrayList<XYDataset>();
		fittingFunction = ff;
		int length = dataTable.length;
		regressionData.add(sampleFunction(dataTable[0][0],dataTable[length-1][0], dataTable.length, title+" Power Fitting")); 
	}

	public void addData(String dataName, double[][] dataTable1) {
		if(dataTable1!= null) {
			data.addSeries(dataName, dataTable1);
			try{
				double dataTable[][] = Utilities.transposeMatrix(dataTable1);
				int length = dataTable.length;
				regressionData.add(sampleFunction(dataTable[0][0],dataTable[length-1][0], 100, dataName+" Polynomial Fitting"));   
			}
			catch(IllegalArgumentException ex){
				ex.printStackTrace();
			}
		}
	}

	private XYDataset sampleFunction(double start, double end, int points, String string) {
		DefaultXYDataset xyd = new DefaultXYDataset();
		double[][] data = new double[2][points];
		double step = (end-start)/points;
		int i=0;
		for(double x=start; x<=end; x+=step){
			if(i>=points) break;
			data[0][i] = x;
			data[1][i] = fittingFunction.getValue(x);
			i++;
		}
		xyd.addSeries(string, data);
		return xyd;
	}

	public void prepareChart(){
		super.prepareChart();
		int i=1;
		for(XYDataset xyd : regressionData) {
			plot.setRenderer(i, new  XYLineAndShapeRenderer(true,true));
			plot.setDataset(i,xyd);
			i++;
		}
			
	}

}
