package gr.uom.java.seanets.statistics;


import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;


public class MyXYLineChart extends JFrame{

	private static final long serialVersionUID = 1L;

	private XYSeriesCollection dataset;
	private String xLabel, yLabel, title;
	
	public MyXYLineChart(String title, String xLabel, String yLabel) {
		super(title);
		this.title = title;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		dataset = new XYSeriesCollection();
	}
	
	public void showChart(){
		if(dataset != null) {
			JFreeChart chart = ChartFactory.createXYLineChart(title,xLabel, yLabel, dataset, PlotOrientation.VERTICAL, true, true, false);
			ChartPanel chartPanel = new ChartPanel(chart);
			chartPanel.setPreferredSize(new Dimension(500, 270));
			this.setContentPane(chartPanel);
			this.pack();
			RefineryUtilities.centerFrameOnScreen(this);
			this.setVisible(true);
		}else{
			JOptionPane.showMessageDialog(this, "No data inserted!");
		}
	}
	
	public void addXYData(XYSeries dataline, Number n1, Number n2) {
		dataline.add(n1, n2);
	}
	
	public XYSeries createDataSeries(String lineName){
		XYSeries dataSeries = new XYSeries(lineName);
		dataset.addSeries(dataSeries);
		return dataSeries;
	}
		
}