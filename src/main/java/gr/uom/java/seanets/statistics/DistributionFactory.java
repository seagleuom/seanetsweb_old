package gr.uom.java.seanets.statistics;

import gr.uom.java.seanets.history.PowerFittingFunction;
import gr.uom.java.seanets.util.SEANetsProperties;
import gr.uom.java.seanets.util.StorageManager;
import java.io.File;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DistributionFactory {
    
    private final static Logger logger = Logger.getLogger(DistributionFactory.class.getName());
	
	public static FittingFunction createCumulativeDistributionFromIntegerIntegerMap(boolean mapHasAges, Map<Integer, Integer> map, String title, String xName, String yName, String projectName) {
		FittingFunction functionThatBestFitsData = null;
		if(map.keySet().size() >= 3) {
			double[][] data = prepareData(mapHasAges, map, title, xName, "cumul_%",projectName);
			FittingFunction powerFunction = new PowerFittingFunction(title+" with Power Fitting", xName, yName, data);
			FittingFunction polynomialFunction = new PolynomialFittingFunction(data, 2, title+" with Polynomial Fitting", xName, yName );
			double powerRSquared = powerFunction.getRSquared();
			double polynomialRSquared = polynomialFunction.getRSquared();
			if(powerRSquared > polynomialRSquared)
				functionThatBestFitsData = powerFunction;
			else
				functionThatBestFitsData = polynomialFunction;
		}
		else{
			logger.log(Level.SEVERE, "Not enough data to create {0}", title);
                        
		}
		return functionThatBestFitsData;
	}

	private static double[][] prepareData(boolean mapHasAges, Map<Integer, Integer> map, String title, String keyName, String valueName, String projectName) {
		Map<Integer, Double> cumulativeFrequencies = new TreeMap<Integer,Double>();
		Map<Integer, Double> percentageFrequencies = new TreeMap<Integer,Double>();
		double sum = 0;
		for(Integer number : map.keySet()){
			sum += map.get(number);	
		}
		// Calculate frequency %
		for(Integer i : map.keySet()){
			percentageFrequencies.put(i, map.get(i) / sum);
		}
		// Calculate cumulative frequency %
		double[][] data = new double[percentageFrequencies.keySet().size()][2];
		double previousFrequency = 0, freq=0;
		int k = 0;
		String fileName = ""+title;
		StringBuilder sb = new StringBuilder();
                sb.append(fileName).append("\n");
                sb.append(keyName).append("\t").append(valueName).append("\n");
		for(Integer i : percentageFrequencies.keySet()){
			freq = percentageFrequencies.get(i) + previousFrequency;
			cumulativeFrequencies.put(i, freq);
			sb.append(i).append("\t").append(freq).append("\n");
			previousFrequency = freq;
			data[k][0] = i;
			data[k][1] = freq;
			k++;
		}
		/*data form:
		| x | y	|
		|   |	|
		|	|	|
		|	|	|
		*/
		if(!mapHasAges){
			//Correction of the first key, which is equal to zero, to 0.0000001
			data[0][0] = 0.000000001;
		}
                String localFolder = SEANetsProperties.getInstance().getSimulationFolder();
                String cumulativeFile = localFolder + File.separator + projectName + File.separator + fileName + ".txt";
                 String simpleDistributionFile = cumulativeFile.replaceAll("Cumulative", "");
                StorageManager.getInstance().saveTextFileLocally(cumulativeFile, sb.toString());
                StorageManager.getInstance().frequencyMapSaver(map, keyName, valueName, simpleDistributionFile);
		return data;
	}


}
