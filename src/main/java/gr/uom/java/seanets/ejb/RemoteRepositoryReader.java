package gr.uom.java.seanets.ejb;

import gr.uom.java.ast.ASTReader;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.source.SourceCodeMetricCalculator;
import gr.uom.java.seanets.db.persistence.DBGraph;
import gr.uom.java.seanets.db.persistence.DBNode;
import gr.uom.java.seanets.db.persistence.DBProject;
import gr.uom.java.seanets.db.persistence.DBTimeline;
import gr.uom.java.seanets.db.persistence.Version;
import gr.uom.java.seanets.graph.MyJavaProject;
import gr.uom.java.seanets.gui.SelectedProjectName;
import gr.uom.java.seanets.history.ProjectEvolution;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.repoHandler.ProjectInfo;
import gr.uom.java.seanets.util.Timer;
import gr.uom.java.seanets.websocket.LoggerWebSocket;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSFile;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.VCSResource;
import gr.uom.se.vcs.analysis.version.TagVersionProvider;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import gr.uom.se.vcs.jgit.VCSRepositoryImp;
import gr.uom.se.vcs.walker.ResourceVisitor;
import gr.uom.se.vcs.walker.filter.resource.ResourceFilterUtility;
import gr.uom.se.vcs.walker.filter.resource.VCSResourceFilter;
import java.io.File;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.eclipse.persistence.exceptions.DatabaseException;

/**
 *
 * @author teo
 */
@Stateless
@LocalBean
public class RemoteRepositoryReader {

    @EJB
    private MetricUpdater metricUpdater;

    @Resource
    private SessionContext context;

    @PersistenceContext(unitName = "seanetsPU")
    private EntityManager entityManager;

    @EJB
    MetricUpdater metricUpdaterBean;

    private DBProject dbProject;
    private String localRepoFolder;
    private String localSourceFolder;
    private VCSRepository remoteRepository;
    private ProjectEvolution evolution;
    private String correspondingMail;
    private List<CommitVersion> selectedVersionList;
    private TagVersionProvider versionProvider;

    private final static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(RemoteRepositoryReader.class.getName());

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void readFromRemoteURL(String remoteRepositoryAddress) {
        logger.setLevel(Level.INFO);
        ProjectInfo projectInfo = new ProjectInfo();
        projectInfo.setProjectRemoteRepositoryURL(remoteRepositoryAddress);
        String projectName = projectInfo.getProjectName();
        SelectedProjectName.getInstance().setSelectedProjectName(projectName);
        if (ProjectEvolution.loadEvolutionFromDisk(projectName) == null || !projectAlreadyExists(projectName)) {
            try {

                evolution = new ProjectEvolution(projectInfo);
                log("Retrieving project information from remote repository");
                localRepoFolder = projectInfo.getProjectLocalRepositoryFolder();
                localSourceFolder = projectInfo.getProjectLocalSourceFolder();
                remoteRepository = new VCSRepositoryImp(localRepoFolder, remoteRepositoryAddress);
                log("Cloning remote repository for project " + SelectedProjectName.getInstance().getSelectedProjectName());

                if (!VCSRepositoryImp.containsGitDir(localRepoFolder)) {
                    remoteRepository.cloneRemote();
                } else {
                    try {
                        remoteRepository.update();
                    } catch (VCSRepositoryException ex) {
                        logger.log(Level.SEVERE, "Cannot update local repo", ex);
                    }
                }
                versionProvider = new TagVersionProvider(remoteRepository);
                log("Cloning remote repository completed ");
            } catch (Exception e) {
                logger.log(Level.SEVERE, "An error occured during project analysis, aborting DB transaction...", e);
                try {
                    context.setRollbackOnly();
                } catch (IllegalStateException | SecurityException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
        } else {
            log("Project " + projectInfo.getProjectName() + " has already been analysed");
        }
    }

    public void initializeAnalysis() {
        dbProject = new DBProject();
        ProjectInfo projectInfo = new ProjectInfo();
        projectInfo.setProjectRemoteRepositoryURL(remoteRepository.getRemotePath());
        String projectName = projectInfo.getProjectName();
        dbProject.setGithubpath(projectInfo.getProjectRemoteRepositoryURL());
        dbProject.setName(projectName);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void analyzeProject() {
        try {
            boolean hasVersions = createSourceOnDisk(dbProject.getName());
            if (hasVersions) {
                createASTsAndAnalyzeProject(dbProject);
            } else {
                log("Not enough versions!");
            }
            LoggerWebSocket.sendLogMessage("Finished!");
            updateDBTimeline(dbProject.getName(), "Project Added");
            evolution.saveProjectEvolutionLocally();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "An error occured during project analysis, aborting DB transaction...", ex);
            try {
                context.setRollbackOnly();
            } catch (IllegalStateException | SecurityException ex2) {
                logger.log(Level.SEVERE, null, ex2);
            }
        }
    }

    public void sendNotificationOfCompletionEmail(String projectPath) {
        entityManager.flush();
        DBProject dbProjectLocal = getProjectByGitPath(projectPath);
        while(dbProjectLocal == null){
            try{
                Thread.sleep(1000);
                dbProjectLocal = getProjectByGitPath(projectPath);
            }catch(InterruptedException ex){}
        }
        String projectName = dbProjectLocal.getName();
        int pid = dbProjectLocal.getPid();
        JavaMailer.sendNotificationEmail(correspondingMail, pid, projectName);
        logger.log(Level.INFO, "Email want sent successfully");

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int deleteProject(DBProject dbProject) {
        int rowsAffected = 0;
        try {
            Query q = entityManager.createNamedQuery("DBProject.deleteByPid");
            Query setParameter = q.setParameter("pid", dbProject.getPid());
            rowsAffected = setParameter.executeUpdate();
            if (rowsAffected == 1) {
                logger.log(Level.INFO, "Project {0} has been deleted from the Database", dbProject.getName());
            }
        } catch (Exception e) {
            try {
                context.setRollbackOnly();
                logger.log(Level.SEVERE, "An error occured during project deletion", e);
            } catch (IllegalStateException | SecurityException ex) {
                java.util.logging.Logger.getLogger(SeanetsView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return rowsAffected;
    }

    public void calculateSourceCodeMetrics(SourceCodeMetricCalculator metricCalculator, DBGraph dbGraph) {
        try {
            metricCalculator.createMetrics();
            updateNodeMetricsOnMemory(metricCalculator, dbGraph);
            dbGraph.setAlpha(dbGraph.getGraph().getAlpha());
            dbGraph.setAverageDegree(metricUpdater.getAverageInDegree(dbGraph.getGraph()));
            dbGraph.setDensity(dbGraph.getGraph().getDensity());
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
            try {
                context.setRollbackOnly();
            } catch (IllegalStateException | SecurityException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }

    public void updateNodeMetricsOnMemory(SourceCodeMetricCalculator metricCalculator, DBGraph dbGraph) {
        if (dbGraph != null) {

            Collection<DBNode> nodes = dbGraph.getdBNodesCollection();
            for (DBNode dbNode : nodes) {
                try {
                    metricUpdater.calculateNodeMetrics(dbNode, metricCalculator);
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Something happened during the calculation of metrics", e);
                }
            }
            metricUpdater.calculateDBSourceMetric(metricCalculator, dbGraph.getVid());

        }
    }

    private List<DBGraph> getDBGraphs(List<Version> versions) {
        List<DBGraph> graphs = new ArrayList<DBGraph>();
        for (Version v : versions) {
            graphs.addAll(entityManager.createNamedQuery("DBGraph.findByVid").
                    setParameter("vid", v).getResultList());
        }
        return graphs;
    }

    private List<Version> getVersions(DBProject project) {
        return entityManager.createNamedQuery("Version.findByPid").setParameter("pid", project).getResultList();
    }

    public DBProject getProject(String projectName) {
        try {
            return (DBProject) entityManager.
                    createNamedQuery("DBProject.findByName").
                    setParameter("name", projectName).
                    getResultList().get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public DBProject getProjectByGitPath(String projectPath) {
        try {
            return (DBProject) entityManager.
                    createNamedQuery("DBProject.findByGithubpath").
                    setParameter("githubpath", projectPath).
                    getResultList().get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public String getCorrespondingMail() {
        return correspondingMail;
    }

    public void setCorrespondingMail(String correspondingMail) {
        this.correspondingMail = correspondingMail;
    }

    public List<DBProject> getProjects() {
        return entityManager.createNamedQuery("DBProject.findAll").getResultList();
    }

    private boolean projectAlreadyExists(String projectName) {
        List<DBProject> results = getProjects();
        for (DBProject project : results) {
            if (project.getName().equalsIgnoreCase(projectName)) {
                return true;
            }
        }
        return false;
    }

    private void createASTsAndAnalyzeProject(DBProject dbProject) throws Exception {
        if (!evolution.getAllProjectEntries().isEmpty()) {
            sanitizeVersionSet();
            Timer createASTs = new Timer("Parsing source files, creating ASTs and Graphs");
            Timer calcSCMetrics = new Timer("Calculating source code metrics");

            log("Creating graph representation and analyzing versions.");
            ProjectEvolutionAnalysis projectEvolutionAnalysis = evolution.getProjectEvolutionAnalysis();
            for (MyJavaProject javaProject : evolution.getAllProjectEntries()) {
                if (javaProject.getJavaFilePaths() != null && javaProject.getJavaFilePaths().size() > 1) {
                    createASTs.start();
                    log("Analyzing version : " + javaProject.getProjectVersion());
                    SystemObject systemObject = ASTReader.parseASTAndCreateSystemObject(javaProject);
                       if(systemObject == null) {
                           continue;
                       }
                    DBGraph dbGraph = javaProject.createGraph(systemObject, projectEvolutionAnalysis);

                    dbProject.setAgeVSnewIncomingEdges(projectEvolutionAnalysis.getAgeVSnewIncomingEdgesMap());
                    dbProject.setAgeVSnewOutEdges(projectEvolutionAnalysis.getAgeVSnewOutgoingEdgesMap());
                    createASTs.stop();
                    calcSCMetrics.start();
                    SourceCodeMetricCalculator sourceCodeMetricsCalculator = new SourceCodeMetricCalculator(systemObject);
                    calculateSourceCodeMetrics(sourceCodeMetricsCalculator, dbGraph);
                    calcSCMetrics.stop();
                } else {
                    logger.log(Level.WARNING, "Version {0} does not contain any java files or the local repository folder is not properly configured", javaProject.getProjectVersion());
                }
            }

            evolution.performEvolutionaryCalculations();
            persistGraphAndNodes();
            log("Calculating Repository Metrics");
            Timer calcRepoMetrics = new Timer("Calculating Repository metrics");
            calcRepoMetrics.start();
            metricUpdaterBean.calculateRepositoryMetrics(remoteRepository,evolution.getAllVersionEntries());
            calcRepoMetrics.stop();

            createASTs.printDurationInMinutes();
            calcSCMetrics.printDurationInMinutes();
            calcRepoMetrics.printDurationInMinutes();
        }
    }

    private void sanitizeVersionSet() {
        ArrayList<Version> versionsToRemove = new ArrayList<>();
        for (MyJavaProject javaProject : evolution.getAllProjectEntries()) {
            if (javaProject.getJavaFilePaths() == null || javaProject.getJavaFilePaths().size() <= 1) {
                versionsToRemove.add(javaProject.getProjectVersion());
            }
        }
        for (Version v : versionsToRemove) {
            evolution.removeProjectAndVersionEntry(v);
        }
    }

    private void persistGraphAndNodes() {
        ProjectEvolutionAnalysis pea = evolution.getProjectEvolutionAnalysis();
        Map<Version, DBGraph> versionToDBGraphMap = pea.getVersionToDBGraphMap();
        for (Version version : versionToDBGraphMap.keySet()) {
            DBGraph dbGraph = versionToDBGraphMap.get(version);
            entityManager.persist(dbGraph);
            for (DBNode dbNode : dbGraph.getdBNodesCollection()) {
                entityManager.persist(dbNode);
            }
        }
        entityManager.flush();
    }

    public List<CommitVersion> getAvailableVersions() throws VCSRepositoryException {
        List<CommitVersion> availableVersionList = new ArrayList<>();
        for (VCSCommit versionCommit : versionProvider) {
            availableVersionList.add(new CommitVersion(versionCommit, versionProvider.getName(versionCommit)));
        }
        return availableVersionList;
    }

    public boolean createSourceOnDisk(String projectName) throws VCSRepositoryException {

        if (selectedVersionList != null && selectedVersionList.size() > 0) {
            dbProject.setVersions(selectedVersionList.size());
            try {
                entityManager.persist(dbProject);
                entityManager.flush();
                logger.log(Level.INFO,"Project ID = "+dbProject.getPid());
                for (CommitVersion versionCommit : selectedVersionList) {

                    String commitPath = localSourceFolder + FileSystems.getDefault().getSeparator() + versionCommit.getCommit().toString();
                    String[] s = null;

                    if (new File(commitPath).exists() == false) {
                        versionCommit.getCommit().checkout(commitPath, s);
                    }
                    Version version = new Version(versionCommit.getCommit().getCommitDate(), versionCommit.getVersionName(), dbProject);
                    log("Checking out commit with date " + versionCommit.getDate() + " version name: " + version);
                    entityManager.persist(version);
                    entityManager.flush();
                    logger.log(Level.INFO,"Version ID = "+version.getVid());
                    final MyJavaProject javaProject = new MyJavaProject(version, projectName);
                    javaProject.setSourceFolderForThisRevision(commitPath);
                    versionCommit.getCommit().walkTree(new ResourceVisitor<VCSResource>() {

                        @Override
                        public VCSResourceFilter<VCSResource> getFilter() {
                            return ResourceFilterUtility.suffix(".java");
                        }

                        @Override
                        public boolean includeDirs() {
                            return false;
                        }

                        @Override
                        public boolean includeFiles() {
                            return true;
                        }

                        @Override
                        public boolean visit(VCSResource t) {

                            if (t instanceof VCSFile) {
                                String fullFilePath = javaProject.getSourceFolder() + FileSystems.getDefault().getSeparator() + t.getPath();
                                javaProject.addJavaFilePath(fullFilePath);
                                logger.log(Level.FINE, "Added new file path in project {0}", fullFilePath);
                            }
                            return true;
                        }
                    });

                    evolution.addProjectForVersion(javaProject.getProjectVersion(), javaProject);
                }
                return true;
            } catch (DatabaseException ex) {
                logger.log(Level.SEVERE, "Project  has already been analysed");
                return false;
            }
        } else {
            logger.log(Level.SEVERE, "Error! Not Enough versions (tags) in the corresponding project");
            return false;
        }
    }

    public void updateDBTimeline(String projectName, String message) {
        DBProject dbProject = getProject(projectName);
        if (dbProject != null) {
            DBTimeline timeline = new DBTimeline();
            timeline.setPid(dbProject);
            timeline.setTitle(message);
            entityManager.persist(timeline);
            entityManager.flush();
        }
    }

    public SessionContext getContext() {
        return context;
    }

    public void setContext(SessionContext context) {
        this.context = context;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public DBProject getDbProject() {
        return dbProject;
    }

    public void setDbProject(DBProject dbProject) {
        this.dbProject = dbProject;
    }

    public String getLocalRepoFolder() {
        return localRepoFolder;
    }

    public void setLocalRepoFolder(String localRepoFolder) {
        this.localRepoFolder = localRepoFolder;
    }

    public String getLocalSourceFolder() {
        return localSourceFolder;
    }

    public void setLocalSourceFolder(String localSourceFolder) {
        this.localSourceFolder = localSourceFolder;
    }

    public VCSRepository getRemoteRepository() {
        return remoteRepository;
    }

    public void setRemoteRepository(VCSRepository remoteRepository) {
        this.remoteRepository = remoteRepository;
    }

    public void log(String status) {
        logger.log(Level.INFO, status);
        LoggerWebSocket.sendLogMessage(status + "...");
    }

    public List<CommitVersion> getSelectedVersionList() {
        return selectedVersionList;
    }

    public void setSelectedVersionList(List<CommitVersion> selectedVersionList) {
        this.selectedVersionList = selectedVersionList;
    }

}
