
package gr.uom.java.seanets.ejb;

import gr.uom.java.seanets.history.ProjectEvolution;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.simulation.EvolutionSimulator;
import gr.uom.java.seanets.util.SEANetsProperties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
@LocalBean
public class SimulatorBean 
{
    
    @EJB
    EvolutionSimulator evolutionSimulator;
    
    
    private final static Logger logger = Logger.getLogger(SimulatorBean.class.getName());
    
    public void simulate(String projectName){
        logger.log(Level.INFO, "Initializing Simulation Procedure for Project {0}", projectName);
        logger.log(Level.INFO, "Loading project...");
        ProjectEvolution evolution = ProjectEvolution.loadEvolutionFromDisk(projectName);
        ProjectEvolutionAnalysis projEvolAnalysis = evolution.getProjectEvolutionAnalysis();
        logger.log(Level.INFO, "Initializing simulator...");
        evolutionSimulator.initializeSimulator(projEvolAnalysis, projectName);
        logger.log(Level.INFO, "Initialization done! Starting simulation...");
        double randomness = SEANetsProperties.getInstance().getSimulationRandomness();
        boolean applyDomainRules = SEANetsProperties.getInstance().getApplyDomainRules();
        boolean applyAgeRules = SEANetsProperties.getInstance().getApplyAgeRules();
        boolean preserveMaxHopDistance = SEANetsProperties.getInstance().getPreserveMaxHopDistance();
        evolutionSimulator.simulationOfEvolution(randomness, null, applyDomainRules, applyAgeRules, preserveMaxHopDistance);
        
    }
}
