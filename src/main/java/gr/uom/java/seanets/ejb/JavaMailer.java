/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seanets.ejb;

import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author teohaik
 */
public class JavaMailer {
    
    private final static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(JavaMailer.class.getName());


    public static void sendNotificationEmail(String userEmail, int pid, String projectName) {
        if (userEmail != null && !userEmail.isEmpty()) {
            Properties props = new Properties();
            props.setProperty("mail.smtp.auth", "true");
            props.put("mail.smtp.host", "localhost");

            javax.mail.Session mailSession = javax.mail.Session.getInstance(props, null);
            try {
                MimeMessage msg = new MimeMessage(mailSession);
                msg.setFrom("seagle@se.uom.gr");
                msg.setRecipients(Message.RecipientType.TO, userEmail);
                msg.setSubject("Analysis is now Complete");
                msg.setSentDate(new Date());
               
                String messageText = "<html>"
                        + "<body>"
                        + "Dear researcher,"
                        + "<p>"
                        + "The analysis for the project you requested is now complete.<br>"
                        + "You may view your results in the link below:<p>"
                        + "<a href=http://se.uom.gr/seagle/_/php/_startProjectSession.php?pid="+ pid + ">"+ projectName +"</a>"
                        + "<p>The SEAgle Team."
                        + "</body>"
                        + "</html>";
                
                msg.setContent(messageText, "text/html; charset=utf-8");
                Transport transport = mailSession.getTransport("smtp");
                transport.connect("localhost", 25, "seagle", "sE@gle!!");
                transport.sendMessage(msg, msg.getAllRecipients());
                transport.close();
            } catch (MessagingException mex) {
                logger.log(Level.SEVERE, "send of notification mail failed, exception: ", mex);
            }
        }
    }
    
    
    public static void sendMailTo(String receiver, String text) {
        
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

}
