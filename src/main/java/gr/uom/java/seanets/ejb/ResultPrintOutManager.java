package gr.uom.java.seanets.ejb;

import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.history.ProjectEvolution;
import gr.uom.java.seanets.history.ProjectEvolutionAnalysis;
import gr.uom.java.seanets.simulation.DistributionManager;
import gr.uom.java.seanets.simulation.SimulationResults;
import gr.uom.java.seanets.util.SEANetsProperties;
import gr.uom.java.seanets.util.StorageManager;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
@LocalBean
public class ResultPrintOutManager {

    private final static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(ResultPrintOutManager.class.getName());

    private ProjectEvolutionAnalysis projectEvolutionAnalysis;
    private SimulationResults simulationResults;
    private String projectName;

    public boolean loadProject(String projectName) {
        this.projectName = projectName;
        ProjectEvolution evolution = ProjectEvolution.loadEvolutionFromDisk(projectName);
        simulationResults = SimulationResults.loadResultsFromFile(projectName);
        if (evolution == null || simulationResults == null) {
            return false;
        } else {
            projectEvolutionAnalysis = evolution.getProjectEvolutionAnalysis();
            return true;
        }
    }

    public void saveDataForPaper() {
        try {
            File outDir = new File(SEANetsProperties.getInstance().getSimulationFolder() + File.separator + projectName);
            if (!outDir.exists()) {
                outDir.mkdir();
            }

            projectEvolutionAnalysis.calculateTopDegreeClassesHistorical(projectEvolutionAnalysis.getProjectGraphEvolution(),outDir + File.separator +  "TopDegreeClassesHistorical_ACTUAL.csv", "NORMAL", 20);
            projectEvolutionAnalysis.calculateTopDegreeClassesHistorical(simulationResults.getProjectGraphPASimulationEvolution(),outDir + File.separator +  "TopDegreeClassesHistorical_PA_Simulation.csv", "PA_Simul", 20);
            projectEvolutionAnalysis.calculateTopDegreeClassesHistorical(simulationResults.getProjectGraphSimulationOfEvolution(),outDir + File.separator +  "TopDegreeClassesHistorical_Proposed_Simul.csv", "Proposed_Simul", 20);

//            String s = projectEvolutionAnalysis.getInDegreeToNormalizedEdgesAttracted();
//
//            StorageManager.getInstance().saveTextFileLocally(outDir + File.separator + "inDegreeToNormalizedEdgesAttracted.txt", s);
//
//            new DistributionManager(projectEvolutionAnalysis, projectName);     
//
//            String ageVSNewIncoming = projectEvolutionAnalysis.getAgeVSnewIncomingEdgesMap();
//            String ageVSNewOutGoing = projectEvolutionAnalysis.getAgeVSnewOutgoingEdgesMap();
//            
//            StorageManager.getInstance().saveTextFileLocally(outDir + File.separator + "AgeVSnewIncomingEdgesMap.txt", ageVSNewIncoming);
//            StorageManager.getInstance().saveTextFileLocally(outDir + File.separator + "AgeVSnewOutgoingEdgesMap.txt", ageVSNewOutGoing);
//            
//            
            projectEvolutionAnalysis.printSummaryReport();

            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

}
