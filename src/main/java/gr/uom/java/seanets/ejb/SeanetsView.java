package gr.uom.java.seanets.ejb;

import gr.uom.java.seanets.db.persistence.DBProject;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Theodore Chaikalis
 */
@ManagedBean(name = "SeanetsView")
@ViewScoped
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SeanetsView implements Serializable {

    private String remoteRepositoryAddress;
    private transient DataModel<DBProject> model;
    private DBProject selectedDBProject;
    private String status;

    private List<CommitVersion> selectedVersionList;
    private List<CommitVersion> availableVersionList;

    @PersistenceContext(unitName = "seanetsPU")
    private EntityManager entityManager;

    @EJB
    private RemoteRepositoryReader reader;

    @EJB
    private MetricUpdater metricUpdater;

    @EJB
    private SimulatorBean simulatorBean;

    @EJB
    private ResultPrintOutManager printOutManager;

    public SeanetsView() {
        remoteRepositoryAddress = null;
        availableVersionList = new ArrayList<>();
        System.out.println("Seanets View created");
    }

    public DataModel<DBProject> getModel() {
        if (model == null) {
            model = new ListDataModel<>(getProjectlist());
        }
        return model;
    }

    public void printOutDataForPaper() {
        selectedDBProject = model.getRowData();
        String projectName = selectedDBProject.getName();
        boolean projectEvolutionExists = printOutManager.loadProject(projectName);
        if(projectEvolutionExists){
            printOutManager.saveDataForPaper();
            RequestContext.getCurrentInstance().execute("alert('PrintOut process finished successfully!.')");
        }
        else{
            RequestContext.getCurrentInstance().execute("PF('warningNoEvolutionExist').show();");
        }
    }

    public void readFromRemoteUrl() throws VCSRepositoryException {
        if (remoteRepositoryAddress != null) {
            reader.readFromRemoteURL(remoteRepositoryAddress);
            availableVersionList = reader.getAvailableVersions();
            sendMessage("Project Download Completed");
        }
    }

    public void doEvolutionAnalysis() {
        if (reader != null) {
            selectedVersionList = new ArrayList<>();
            for (CommitVersion commitVersion : availableVersionList) {
                if (commitVersion.isIncludeInAnalysis()) {
                    selectedVersionList.add(commitVersion);
                }
            }
            if (selectedVersionList.size() > 3) {
                reader.setSelectedVersionList(selectedVersionList);
                reader.initializeAnalysis();
                reader.analyzeProject();
            } else {
                RequestContext.getCurrentInstance().execute("PF('warningNoVersions').show();");
            }
        }
    }

    public void simulate() {
        selectedDBProject = model.getRowData();
        simulatorBean.simulate(selectedDBProject.getName());
    }

    public void updateAllProjectGraphMetrics() {
        metricUpdater.updateAllProjectGraphMetrics();
    }

    public void updateAllProjectSourceCodeMetrics() {
        metricUpdater.updateAllProjectSourceCodeMetrics();
    }

    public void updateAllProjectRepositoryMetrics() {
        metricUpdater.updateAllProjectRepositoryMetrics();
    }

    public void updateGraphBasedMetrics() {
        selectedDBProject = model.getRowData();
        metricUpdater.updateGraphBasedMetrics(selectedDBProject.getName());
    }

    public void updateSourceCodeMetrics() {
        selectedDBProject = model.getRowData();
        metricUpdater.updateObjectOrientedMetrics(selectedDBProject.getName());
    }

    public void updateRepositoryMetrics() {
        selectedDBProject = model.getRowData();
        metricUpdater.updateRepositoryMetrics(selectedDBProject.getName());
    }

    public void deleteProject() {
        selectedDBProject = model.getRowData();
        reader.deleteProject(selectedDBProject);
        model = new ListDataModel<>(getProjectlist());
    }

    public List<CommitVersion> getAvailableVersionList() {
        return availableVersionList;
    }

    public void setAvailableVersionList(List<CommitVersion> availableVersionList) {
        this.availableVersionList = availableVersionList;
    }

    public String getRemoteRepositoryAddress() {
        return remoteRepositoryAddress;
    }

    public void setRemoteRepositoryAddress(String remoteRepositoryAddress) {
        this.remoteRepositoryAddress = remoteRepositoryAddress;
    }

    @Override
    public String toString() {
        return "SEAnets View";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        //   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("",status));
    }

    public List<DBProject> getProjectlist() {
        return entityManager.createNamedQuery("DBProject.findAll").getResultList();
    }

    public RemoteRepositoryReader getReader() {
        return reader;
    }

    public void setReader(RemoteRepositoryReader reader) {
        this.reader = reader;
    }

    public DBProject getSelectedProjectName() {
        return selectedDBProject;
    }

    public void setSelectedProjectName(DBProject selectedProjectName) {
        this.selectedDBProject = selectedProjectName;
    }

    public void selectAllVersions() {
        for (CommitVersion cv : availableVersionList) {
            cv.setIncludeInAnalysis(true);
        }
    }

    public void sendMessage(String messageText) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Seagle Information", messageText);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
