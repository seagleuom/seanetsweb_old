/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seanets.ejb;

import gr.uom.se.vcs.VCSCommit;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Theodore Chaikalis
 *
 */
public class CommitVersion {

    private VCSCommit commit;
    private String versionName;
    private boolean includeInAnalysis;

    public CommitVersion(VCSCommit commit, String versionName) {
        this.commit = commit;
        this.versionName = versionName;
        includeInAnalysis = false;
    }

    public VCSCommit getCommit() {
        return commit;
    }

    public void setCommit(VCSCommit commit) {
        this.commit = commit;
    }

    public String getDate() {
        Date d = commit.getCommitDate();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(d);
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public boolean isIncludeInAnalysis() {
        return includeInAnalysis;
    }

    public void setIncludeInAnalysis(boolean includeInAnalysis) {
        this.includeInAnalysis = includeInAnalysis;
    }

    @Override
    public String toString() {
        return "Commit Version " + versionName + " Date " + commit.getCommitDate();
    }

}
