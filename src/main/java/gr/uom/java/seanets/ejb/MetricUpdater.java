package gr.uom.java.seanets.ejb;

import gr.uom.java.ast.ASTReader;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.repo.AuthorsPerVersion;
import gr.uom.java.ast.metrics.repo.CommitsPerVersion;
import gr.uom.java.ast.metrics.repo.FilesAddedPerVersion;
import gr.uom.java.ast.metrics.repo.FilesDeletedPerVersion;
import gr.uom.java.ast.metrics.repo.FilesModifiedPerVersion;
import gr.uom.java.ast.metrics.repo.LinesAddedPerVersion;
import gr.uom.java.ast.metrics.repo.LinesDeletedPerVersion;
import gr.uom.java.ast.metrics.repo.RepositoryMetric;
import gr.uom.java.ast.metrics.repo.RepositoryMetricCalculator;
import gr.uom.java.ast.metrics.repo.TestFilesAddedPerVersion;
import gr.uom.java.ast.metrics.repo.TestFilesModifiedPerVersion;
import gr.uom.java.ast.metrics.source.CBO;
import gr.uom.java.ast.metrics.source.LCOM;
import gr.uom.java.ast.metrics.source.LOC;
import gr.uom.java.ast.metrics.source.NOF;
import gr.uom.java.ast.metrics.source.NOM;
import gr.uom.java.ast.metrics.source.SourceCodeMetricCalculator;
import gr.uom.java.ast.metrics.source.WMC;
import gr.uom.java.seanets.db.persistence.DBGraph;
import gr.uom.java.seanets.db.persistence.DBNode;
import gr.uom.java.seanets.db.persistence.DBProject;
import gr.uom.java.seanets.db.persistence.DBRepoMetric;
import gr.uom.java.seanets.db.persistence.DBSourceMetric;
import gr.uom.java.seanets.db.persistence.Version;
import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyJavaProject;
import gr.uom.java.seanets.graph.MyNode;
import gr.uom.java.seanets.history.ProjectEvolution;
import gr.uom.java.seanets.util.Timer;
import gr.uom.java.seanets.websocket.LoggerWebSocket;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import gr.uom.se.vcs.jgit.VCSRepositoryImp;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
@LocalBean
public class MetricUpdater {

    @Resource
    private SessionContext context;

    @PersistenceContext(unitName = "seanetsPU")
    private EntityManager entityManager;

    private final static Logger logger = Logger.getLogger(MetricUpdater.class.getName());

    public void updateRepositoryMetrics(String projectName) {
        try {
            
            logger.log(Level.INFO, "Updating Repository metrics for {0}", projectName);
            ProjectEvolution evolution = ProjectEvolution.loadEvolutionFromDisk(projectName);
            String localRepoFolder = evolution.getProjectInfo().getProjectLocalRepositoryFolder();
            String remoteGitPath = evolution.getProjectInfo().getProjectRemoteRepositoryURL();
            List<Version> versions = evolution.getAllVersionEntries();
            VCSRepository repository = new VCSRepositoryImp(localRepoFolder, remoteGitPath);
            if (!VCSRepositoryImp.containsGitDir(localRepoFolder)) {
                repository.cloneRemote();
            }
            Timer calcRepoMetrics = new Timer("Calculating Repository metrics");
            calcRepoMetrics.start();
            calculateRepositoryMetrics(repository,versions);
            calcRepoMetrics.stop();
            calcRepoMetrics.printDurationInMinutes();
        } catch (VCSRepositoryException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void calculateRepositoryMetrics(VCSRepository repository, List<Version> versions) {
        try {
            RepositoryMetricCalculator repoMetricCalc = new RepositoryMetricCalculator(repository);
            repoMetricCalc.createAndAddMetric(AuthorsPerVersion.MNEMONIC);
            repoMetricCalc.createAndAddMetric(CommitsPerVersion.MNEMONIC);
            repoMetricCalc.createAndAddMetric(FilesAddedPerVersion.MNEMONIC);
            repoMetricCalc.createAndAddMetric(FilesDeletedPerVersion.MNEMONIC);
            repoMetricCalc.createAndAddMetric(FilesModifiedPerVersion.MNEMONIC);
            repoMetricCalc.createAndAddMetric(LinesAddedPerVersion.MNEMONIC);
            repoMetricCalc.createAndAddMetric(LinesDeletedPerVersion.MNEMONIC);
            repoMetricCalc.createAndAddMetric(TestFilesAddedPerVersion.MNEMONIC);
            repoMetricCalc.createAndAddMetric(TestFilesModifiedPerVersion.MNEMONIC);

            repoMetricCalc.makeCalculations();
            for (Version version : versions) {
                Version projectVersion = getVersion(version);
                if (projectVersion != null) {
                    logger.log(Level.INFO, "Calculating Repository metrics version {0}", projectVersion);
                    LoggerWebSocket.sendLogMessage("Calculating Repository metrics version " + projectVersion + "...");
                    updateDBRepositoryMetric(repoMetricCalc, projectVersion);
                }
            }
            logger.log(Level.INFO, "All metrics updated successfully!");
        } catch (VCSRepositoryException | InterruptedException ex) {
            Logger.getLogger(MetricUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateGraphBasedMetrics(String projectName) {
        try {
            Timer calcGraphMetrics = new Timer("Updating Graph metrics");
            calcGraphMetrics.start();
            ProjectEvolution evolution = ProjectEvolution.loadEvolutionFromDisk(projectName);
            for (Version v : evolution.getAllVersionEntries()) {
                DBGraph dbGraph = getGraph(v);
                if (dbGraph != null) {
                    logger.log(Level.INFO, "Updating graph metrics for project {0} version: {1}", new Object[]{projectName, v});
                    MyGraph myGraph = dbGraph.getGraph();
                    dbGraph.setAlpha(myGraph.getAlpha());
                    dbGraph.setAverageDegree(getAverageInDegree(myGraph));
                    dbGraph.setDensity(myGraph.getDensity());
                    entityManager.persist(dbGraph);
                }
            }
            calcGraphMetrics.stop();
            calcGraphMetrics.printDurationInMinutes();
            logger.log(Level.INFO, "All metrics updated successfully!");
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
            try {
                context.setRollbackOnly();
            } catch (IllegalStateException | SecurityException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateAllProjectGraphMetrics() {
        List<DBProject> projectList = entityManager.createNamedQuery("DBProject.findAll").getResultList();
        int projects = projectList.size();
        int i = 1;
        for (DBProject dbProject : projectList) {
            try {
                logger.log(Level.INFO, "Updating project " + (i++) + " from " + projects);
                updateGraphBasedMetrics(dbProject.getName());

            } catch (Exception e) {
                logger.log(Level.SEVERE, null, e);
                try {
                    context.setRollbackOnly();
                } catch (IllegalStateException | SecurityException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updateAllProjectSourceCodeMetrics() {
        List<DBProject> projectList = entityManager.createNamedQuery("DBProject.findAll").getResultList();
        int projects = projectList.size();
        int i = 1;
        for (DBProject dbProject : projectList) {
            try {
                logger.log(Level.INFO, "Updating project " + (i++) + " from " + projects);
                updateObjectOrientedMetrics(dbProject.getName());
            } catch (Exception e) {
                try {
                    context.setRollbackOnly();
                } catch (IllegalStateException | SecurityException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updateAllProjectRepositoryMetrics() {
        List<DBProject> projectList = entityManager.createNamedQuery("DBProject.findAll").getResultList();
        int projects = projectList.size();
        int i = 1;
        for (DBProject dbProject : projectList) {
            try {
                logger.log(Level.INFO, "Updating project " + (i++) + " from " + projects);
                updateRepositoryMetrics(dbProject.getName());
            } catch (Exception e) {
                try {
                    context.setRollbackOnly();
                } catch (IllegalStateException | SecurityException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updateAllProjectAllMetrics() {
        List<DBProject> projectList = entityManager.createNamedQuery("DBProject.findAll").getResultList();
        for (DBProject dbProject : projectList) {
            updateGraphBasedMetrics(dbProject.getName());
            updateObjectOrientedMetrics(dbProject.getName());
            updateRepositoryMetrics(dbProject.getName());
        }
    }

    public double getAverageInDegree(MyGraph graph) {
        double sum = 0;
        int count = 1;
        Map<Integer, Integer> degreeMap = graph.getInDegreeToCountMap();
        for (Integer degree : degreeMap.keySet()) {
            int degreeFrequency = degreeMap.get(degree);
            sum += degree * degreeFrequency;
            count += degreeFrequency;
        }
        return (sum / count);

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateObjectOrientedMetrics(String projectName) {
        try {
            Timer calcSCMetrics = new Timer("Updating source code metrics");
            calcSCMetrics.start();
            ProjectEvolution evolution = ProjectEvolution.loadEvolutionFromDisk(projectName);
            if (evolution != null) {
                for (MyJavaProject project : evolution.getAllProjectEntries()) {
                    logger.log(Level.INFO, "Updating source code metrics for project {0} version: {1}", new Object[]{project.getProjectName(), project.getProjectVersion()});
                    SystemObject systemObject = ASTReader.parseASTAndCreateSystemObject(project);

                    SourceCodeMetricCalculator metricCalculator = new SourceCodeMetricCalculator(systemObject);
                    metricCalculator.createMetrics();

                    updateNodeMetricsOnDB(metricCalculator, project.getProjectVersion());
                }
            }
            calcSCMetrics.stop();
            calcSCMetrics.printDurationInMinutes();
            logger.log(Level.INFO, "All metrics updated successfully!");
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
            try {
                context.setRollbackOnly();
            } catch (IllegalStateException | SecurityException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }

    private Version getVersion(Version version) {
        try {
            List<Version> list = entityManager.createNamedQuery("Version.findByVid").setParameter("vid", version.getVid()).getResultList();
            return list.get(0);
        } catch (Exception e) {
            return null;
        }
    }

    private DBGraph getGraph(Version version) {
        try {
            List<DBGraph> list = entityManager.createNamedQuery("DBGraph.findByVid").setParameter("vid", version).getResultList();
            return list.get(0);
        } catch (Exception e) {
            return null;
        }
    }

    private DBSourceMetric getSourceMetric(Version version) {
        try {
            List<DBSourceMetric> list = entityManager.createNamedQuery("DBSourceMetric.findByVersion").setParameter("vid", version).getResultList();
            return list.get(0);
        } catch (Exception e) {
            return null;
        }
    }

    private DBRepoMetric getRepositoryMetric(Version version) {
        try {
            List<DBRepoMetric> list = entityManager.createNamedQuery("DBRepoMetric.findByVersion").setParameter("vid", version).getResultList();
            return list.get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public void updateNodeMetricsOnDB(SourceCodeMetricCalculator metricCalculator, Version projectVersion) {
        DBGraph dbGraph = getGraph(projectVersion);
        if (dbGraph != null) {
            List<DBNode> nodes = entityManager.createNamedQuery("DBNode.findByGid").setParameter("gid", dbGraph).getResultList();
            for (DBNode dbNode : nodes) {
                calculateNodeMetrics(dbNode, metricCalculator);
                entityManager.persist(dbNode);
            }
        }
        calculateDBSourceMetric(metricCalculator, projectVersion);
    }

    public void calculateNodeMetrics(DBNode dbNode, SourceCodeMetricCalculator metricCalculator) {
        try {
            dbNode.setCbo(metricCalculator.getMetric(CBO.MNEMONIC).getMetricValueForClass(dbNode.getName()).intValue());
            dbNode.setLcom(metricCalculator.getMetric(LCOM.MNEMONIC).getMetricValueForClass(dbNode.getName()).doubleValue());
            dbNode.setNof(metricCalculator.getMetric(NOF.MNEMONIC).getMetricValueForClass(dbNode.getName()).intValue());
            dbNode.setNom(metricCalculator.getMetric(NOM.MNEMONIC).getMetricValueForClass(dbNode.getName()).intValue());
            dbNode.setWmc(metricCalculator.getMetric(WMC.MNEMONIC).getMetricValueForClass(dbNode.getName()).intValue());
            dbNode.setLoc(metricCalculator.getMetric(LOC.MNEMONIC).getMetricValueForClass(dbNode.getName()).intValue());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Something happened during the calculation of node metrics for class " + dbNode.getName(), e);
        }
    }

    public void calculateDBSourceMetric(SourceCodeMetricCalculator metricCalculator, Version projectVersion) {
        try {
            DBSourceMetric dbMetric = getSourceMetric(projectVersion);
            if (dbMetric == null) {
                dbMetric = new DBSourceMetric();
            }
            dbMetric.setVid(projectVersion);
            dbMetric.setCbo(metricCalculator.getMetric(CBO.MNEMONIC).getSystemLevelValue().doubleValue());
            dbMetric.setLcom(metricCalculator.getMetric(LCOM.MNEMONIC).getSystemLevelValue().doubleValue());
            dbMetric.setNof(metricCalculator.getMetric(NOF.MNEMONIC).getSystemLevelValue().doubleValue());
            dbMetric.setNom(metricCalculator.getMetric(NOM.MNEMONIC).getSystemLevelValue().doubleValue());
            dbMetric.setWmc(metricCalculator.getMetric(WMC.MNEMONIC).getSystemLevelValue().doubleValue());
            dbMetric.setLoc(metricCalculator.getMetric(LOC.MNEMONIC).getSystemLevelValue().doubleValue());
            entityManager.persist(dbMetric);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Something happened during the calculation of source code metrics", e);
        }
    }

     @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateDBRepositoryMetric(RepositoryMetricCalculator repoMetricCalc, Version projectVersion) {
        DBRepoMetric repoMetric = getRepositoryMetric(projectVersion);
        if (repoMetric == null) {
            repoMetric = new DBRepoMetric();
            
        }
        repoMetric.setVid(projectVersion);
        repoMetric.setAuthors(((RepositoryMetric) repoMetricCalc.getMetric(AuthorsPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        repoMetric.setCommits(((RepositoryMetric) repoMetricCalc.getMetric(CommitsPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        repoMetric.setFilesAdded(((RepositoryMetric) repoMetricCalc.getMetric(FilesAddedPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        repoMetric.setFilesDeleted(((RepositoryMetric) repoMetricCalc.getMetric(FilesDeletedPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        repoMetric.setFilesModified(((RepositoryMetric) repoMetricCalc.getMetric(FilesModifiedPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        repoMetric.setLinesAdded(((RepositoryMetric) repoMetricCalc.getMetric(LinesAddedPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        repoMetric.setLinesDeleted(((RepositoryMetric) repoMetricCalc.getMetric(LinesDeletedPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        repoMetric.setTestFilesAdded(((RepositoryMetric) repoMetricCalc.getMetric(TestFilesAddedPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        repoMetric.setTestFilesModified(((RepositoryMetric) repoMetricCalc.getMetric(TestFilesModifiedPerVersion.MNEMONIC)).getMetricValueForAVersion(projectVersion.toString()).intValue());
        
        entityManager.persist(repoMetric);
    }

}
