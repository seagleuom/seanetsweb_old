
package gr.uom.java.ast.metrics.repo;

import gr.uom.java.ast.metrics.AbstractMetric;
import gr.uom.java.ast.metrics.AbstractMetricFactory;
import gr.uom.se.vcs.analysis.version.TagVersionProvider;

/**
 *
 * @author Theodore Chaikalis
 */
public class RepositoryMetricFactory extends AbstractMetricFactory {
    
    private TagVersionProvider versionProvider;

    public RepositoryMetricFactory(TagVersionProvider versionProvider) {
        this.versionProvider = versionProvider;
    }
    
    @Override
    public AbstractMetric make(String metricMnemonic) {
        switch(metricMnemonic){
            case AuthorsPerVersion.MNEMONIC:
                return new AuthorsPerVersion(versionProvider);
            case CommitsPerVersion.MNEMONIC:
                return new CommitsPerVersion(versionProvider);
            case FilesAddedPerVersion.MNEMONIC:
                return new FilesAddedPerVersion(versionProvider);
            case FilesDeletedPerVersion.MNEMONIC:
                return new FilesDeletedPerVersion(versionProvider);
            case FilesModifiedPerVersion.MNEMONIC:
                return new FilesModifiedPerVersion(versionProvider);
            case LinesAddedPerVersion.MNEMONIC:
                return new LinesAddedPerVersion(versionProvider);
            case LinesDeletedPerVersion.MNEMONIC:
                return new LinesDeletedPerVersion(versionProvider);
            case TestFilesAddedPerVersion.MNEMONIC:
                return new TestFilesAddedPerVersion(versionProvider);
            case TestFilesModifiedPerVersion.MNEMONIC:
                return new TestFilesModifiedPerVersion(versionProvider);
        }
        return null;
    }

}
