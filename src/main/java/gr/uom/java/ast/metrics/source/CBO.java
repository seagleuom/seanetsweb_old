/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.uom.java.ast.metrics.source;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import java.util.ListIterator;

/**
 *
 * @author Thoeodore Chaikalis
 */
public class CBO extends SourceCodeMetric{
    
    public static final String MNEMONIC = "CBO";
    
    public CBO(SystemObject systemObject) {
        super(systemObject);
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classCBO(classObject);
        }
    }

    private void classCBO(ClassObject classObject) {
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        int dependencies = 0;
        while (classIterator.hasNext()) {
            String otherClass = classIterator.next().getName();
            if(classObject.isFriend(otherClass)) {
                dependencies++;
            }
        }
        valuePerClass.put(classObject.getName(), new Double(dependencies));
    }
    
    @Override
    public void accept(AbstractMetricVisitor visitor) {
        visitor.visit(this);
    }

}
