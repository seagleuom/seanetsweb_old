/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.uom.java.ast.metrics.source;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import java.util.ListIterator;
import java.util.Set;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

/**
 *
 * @author Theodore Chaikalis
 */
public class NOM extends SourceCodeMetric {
    
    public static final String MNEMONIC = "NOM";
    
    public NOM(SystemObject system) {
        super(system);
        ListIterator<ClassObject> classIterator = system.getClassListIterator();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classNOM(classObject);
        }
    }
    
    private void classNOM(ClassObject classObject) {
        TypeDeclaration type = classObject.getTypeDeclaration();
        MethodDeclaration[] methods = type.getMethods();
        if(methods != null && methods.length > 0 )
            valuePerClass.put(classObject.getName(), (double)methods.length);
        else
            valuePerClass.put(classObject.getName(), 0.0);
    }
    
    @Override
    public Number getSystemLevelValue() {
        Set<String> keySet = valuePerClass.keySet();
        double sum = 0;
        int notDefined = 0;
        for (String key : keySet) {
            Number value = valuePerClass.get(key);
            if (value != null) {
                sum += value.doubleValue();
            } else {
                notDefined++;
            }
        }
        if (keySet.size() == notDefined) {
            return 0;
        } else {
            return sum;
        }
    }


    @Override
    public void accept(AbstractMetricVisitor visitor) {
        visitor.visit(this);
    }
    
}
