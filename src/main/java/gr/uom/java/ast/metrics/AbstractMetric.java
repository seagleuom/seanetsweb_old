
package gr.uom.java.ast.metrics;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class AbstractMetric {

        
    public abstract void accept(AbstractMetricVisitor visitor);
    public abstract Number getSystemLevelValue();
    public abstract Number getMetricValueForClass(String className);
}
