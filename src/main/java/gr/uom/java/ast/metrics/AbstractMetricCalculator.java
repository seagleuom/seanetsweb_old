
package gr.uom.java.ast.metrics;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class AbstractMetricCalculator {
    
    protected Map<String,AbstractMetric> metricsToCalculate;
    private AbstractMetricVisitor metricVisitor;

    public AbstractMetricCalculator() {
        metricsToCalculate = new HashMap<>();
        metricVisitor = new AbstractMetricVisitor();
    }

    public AbstractMetricCalculator(AbstractMetricVisitor metricVisitor) {
        this.metricVisitor = metricVisitor;
    }

    public Map<String, AbstractMetric> getMetricsToCalculate() {
        return metricsToCalculate;
    }

    public void setMetricsToCalculate(Map<String, AbstractMetric> metricsToCalculate) {
        this.metricsToCalculate = metricsToCalculate;
    }

    public AbstractMetricVisitor getMetricVisitor() {
        return metricVisitor;
    }

    public void setMetricVisitor(AbstractMetricVisitor metricVisitor) {
        this.metricVisitor = metricVisitor;
    }
        
    public AbstractMetric getMetric(String metricMnemonic) {
        return metricsToCalculate.get(metricMnemonic);
    }
    
    public void acceptMetricVisitor() {
        for (AbstractMetric m : metricsToCalculate.values()) {
            m.accept(metricVisitor);
        }
    }
    
    public abstract void createAndAddMetric(String metricMnemonic);
}
