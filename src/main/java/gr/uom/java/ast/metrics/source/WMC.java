package gr.uom.java.ast.metrics.source;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import gr.uom.java.ast.util.ExpressionExtractor;
import java.util.List;
import java.util.ListIterator;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.WhileStatement;

/**
 *
 * @author Theodore Chaikalis
 */
public class WMC extends SourceCodeMetric {
    
    public static final String MNEMONIC = "WMC";


    public WMC(SystemObject system) {
        super(system);
        ListIterator<ClassObject> classIterator = system.getClassListIterator();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            calculateCC(classObject);
        }
    }

    private void calculateCC(ClassObject classObject) {
        int complexity = 0;
        TypeDeclaration type = classObject.getTypeDeclaration();
        MethodDeclaration[] methods = type.getMethods();
        for (MethodDeclaration method : methods) {
            CyclomaticComplexityVisitor ccVisitor = new CyclomaticComplexityVisitor();
            method.accept(ccVisitor);
            complexity += ccVisitor.getCyclomaticComplexity();
            //            debugPrinting(ccVisitor, method);
        }
        valuePerClass.put(classObject.getName(), (double)complexity);
    }

    @Override
    public void accept(AbstractMetricVisitor visitor) {
        visitor.visit(this);
    }

    
    
    
    
    
    
    
    
    public class CyclomaticComplexityVisitor extends ASTVisitor {

        private double cyclomaticComplexity;

        public CyclomaticComplexityVisitor() {
            cyclomaticComplexity = 1;
        }

        public double getCyclomaticComplexity() {
            return cyclomaticComplexity;
        }

        @Override
        public boolean visit(IfStatement stmt) {
            cyclomaticComplexity++;
            ExpressionExtractor exprExtractor = new ExpressionExtractor();
            List<Expression> infixExpressions = exprExtractor.getInfixExpressions(stmt);
            int numberOfInfixExpressions = infixExpressions.size();
            if (numberOfInfixExpressions > 2) {
                cyclomaticComplexity += numberOfInfixExpressions - 1;
            }
            return super.visit(stmt);
        }

        @Override
        public boolean visit(WhileStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

        @Override
        public boolean visit(ForStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

        @Override
        public boolean visit(EnhancedForStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

        @Override
        public boolean visit(DoStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

        @Override
        public boolean visit(SwitchStatement stmt) {
            cyclomaticComplexity++;
            return super.visit(stmt);
        }

    }
}
