package gr.uom.java.ast.metrics.source;

import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.AbstractMetric;
import gr.uom.java.ast.metrics.AbstractMetricFactory;

/**
 *
 * @author Theodore Chaikalis
 */
public class SourceCodeMetricFactory extends AbstractMetricFactory {

    private final SystemObject system;

    public SourceCodeMetricFactory(SystemObject system) {
        this.system = system;
    }

    @Override
    public AbstractMetric make(String metricMnemonic) {
        switch (metricMnemonic) {
            case CBO.MNEMONIC:
                return new CBO(system);
            case LCOM.MNEMONIC:
                return new LCOM(system);
            case MMIC.MNEMONIC:
                return new MMIC(system);
            case NOF.MNEMONIC:
                return new NOF(system);
            case NOM.MNEMONIC:
                return new NOM(system);
            case WMC.MNEMONIC:
                return new WMC(system);
            case LOC.MNEMONIC:
                return new LOC(system);
        }
        return null;
    }

}
