
package gr.uom.java.ast.metrics.source;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import java.util.ListIterator;
import java.util.Set;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

/**
 * Calculates Number of Fields per class
 * @author Theodore Chaikalis
 */
public class NOF extends SourceCodeMetric {
    
    public static final String MNEMONIC = "NOF";

    public NOF(SystemObject system) {
        super(system);
        ListIterator<ClassObject> classIterator = system.getClassListIterator();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classNOF(classObject);
        }
    }
    
    private void classNOF(ClassObject classObject) {
        TypeDeclaration type = classObject.getTypeDeclaration();
        FieldDeclaration[] fields = type.getFields();
        if(fields != null && fields.length > 0 )
            valuePerClass.put(classObject.getName(), (double)fields.length);
        else
            valuePerClass.put(classObject.getName(), 0.0);
    }
    
    @Override
    public Number getSystemLevelValue() {
        Set<String> keySet = valuePerClass.keySet();
        double sum = 0;
        int notDefined = 0;
        for (String key : keySet) {
            Number value = valuePerClass.get(key);
            if (value != null) {
                sum += value.doubleValue();
            } else {
                notDefined++;
            }
        }
        if (keySet.size() == notDefined) {
            return 0;
        } else {
            return sum;
        }
    }

    @Override
    public void accept(AbstractMetricVisitor visitor) {
        visitor.visit(this);
    }

}
