
package gr.uom.java.ast.metrics.repo;

import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import gr.uom.se.util.pattern.processor.Processor;
import gr.uom.se.vcs.analysis.version.CommitVersionCounterProcessor;
import gr.uom.se.vcs.analysis.version.VersionProvider;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
public class CommitsPerVersion extends RepositoryMetric {
    
    public static final String MNEMONIC = "COMMITS_PER_VERSION";
    
    private CommitVersionCounterProcessor commitPerVesionCounterProcessor;
    private Map<String, Integer> commitsPerVersion;

    public CommitsPerVersion(VersionProvider versionProvider) {
        super(versionProvider);
        commitPerVesionCounterProcessor = new CommitVersionCounterProcessor(versionProvider, null);
    }

    @Override
    public void accept(AbstractMetricVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Number getSystemLevelValue() {
        return 0;
    }

    @Override
    public Number getMetricValueForClass(String className) {
        return 0;
    }

    public CommitVersionCounterProcessor getCommitPerVesionCounter() {
        return commitPerVesionCounterProcessor;
    }

    public void setCommitPerVesionCounter(CommitVersionCounterProcessor commitPerVesionCounter) {
        this.commitPerVesionCounterProcessor = commitPerVesionCounter;
    }

    @Override
    public Number getMetricValueForAVersion(String version) {
        if(commitsPerVersion == null) 
            commitsPerVersion = commitPerVesionCounterProcessor.getResult();
        return commitsPerVersion.get(version);
    }

    @Override
    public Processor getProcessor() {
        return commitPerVesionCounterProcessor;
    }

}
