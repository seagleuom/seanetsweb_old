
package gr.uom.java.ast.metrics.source;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Theodore Chaikalis
 */
public class LOC extends SourceCodeMetric {

    public static final String MNEMONIC = "LOC";
    
    public LOC(SystemObject systemObject) {
        super(systemObject);
    }
    
    @Override
    public void accept(AbstractMetricVisitor visitor) {
         visitor.visit(this);
    }

    @Override
    public Number getMetricValueForClass(String className) {
        ClassObject classObject = systemObject.getClassObject(className);
        if(classObject != null) 
            return classObject.getLinesOfCode();
        else
            return 0;
    }
    
    @Override
    public Number getSystemLevelValue() {
        Map<String, Integer> LOCperClass = systemObject.getLinesOfCodePerClassMap();
        double sum = 0;
        int notDefined = 0;
        for (String key : LOCperClass.keySet()) {
            Number value = LOCperClass.get(key);
            if (value != null) {
                sum += value.doubleValue();
            } else {
                notDefined++;
            }
        }
        if (LOCperClass.keySet().size() == notDefined) {
            return 0;
        } else {
            return sum;
        }
    }
}
