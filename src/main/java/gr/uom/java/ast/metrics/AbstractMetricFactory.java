
package gr.uom.java.ast.metrics;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class AbstractMetricFactory {

    public abstract AbstractMetric make(String metricMnemonic);
}
