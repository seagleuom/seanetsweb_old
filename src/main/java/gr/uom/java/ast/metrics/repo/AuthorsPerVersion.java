
package gr.uom.java.ast.metrics.repo;

import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import gr.uom.se.util.pattern.processor.Processor;
import gr.uom.se.vcs.analysis.version.AuthorVersionProcessor;
import gr.uom.se.vcs.analysis.version.VersionProvider;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Theodore Chaikalis
 */
public class AuthorsPerVersion extends RepositoryMetric {
    
    public static final String MNEMONIC = "AUTHORS_PER_VERSION";
    private Map<String, Set<String>> authorsPerVersion;
    private final AuthorVersionProcessor authorsPerVersionProcessor;

    public AuthorsPerVersion(VersionProvider versionProvider) {
        super(versionProvider);
        authorsPerVersionProcessor = new AuthorVersionProcessor(
            versionProvider, null/* id */, true /* true for authors */);
    }

    @Override
    public Number getMetricValueForAVersion(String version) {
        if(authorsPerVersion == null && authorsPerVersionProcessor != null)
           authorsPerVersion = authorsPerVersionProcessor.getResult();
        return authorsPerVersion.get(version).size();
    }
    
    @Override
    public void accept(AbstractMetricVisitor visitor) {visitor.visit(this); }
    
    @Override
    public Number getSystemLevelValue() {return 0; }

    @Override
    public Number getMetricValueForClass(String className) { return 0;  }

    @Override
    public Processor getProcessor() {
        return authorsPerVersionProcessor;
    }  

}
