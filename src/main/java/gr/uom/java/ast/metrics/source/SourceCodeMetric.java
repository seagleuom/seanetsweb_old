package gr.uom.java.ast.metrics.source;

import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.AbstractMetric;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class SourceCodeMetric  extends AbstractMetric {

    protected SystemObject systemObject;
    protected Map<String, Number> valuePerClass;

    public SourceCodeMetric(SystemObject systemObject) {
        this.systemObject = systemObject;
        valuePerClass = new LinkedHashMap<>();
    }
    
    public SourceCodeMetric() {
        valuePerClass = new LinkedHashMap<>();
    }

    public SystemObject getSystemObject() {
        return systemObject;
    }

    public void setSystemObject(SystemObject systemObject) {
        this.systemObject = systemObject;
    }
    
    @Override
    public Number getMetricValueForClass(String className) {
        if(valuePerClass.get(className) != null)
            return valuePerClass.get(className);
        else
            return 0;
    }
    
    @Override
    public Number getSystemLevelValue(){
        Set<String> keySet = valuePerClass.keySet();
        double sum = 0;
        int notDefined = 0;
        for (String key : keySet) {
            Number value = valuePerClass.get(key);
            if (value != null) {
                sum += value.doubleValue();
            } else {
                notDefined++;
            }
        }
        if (keySet.size() == notDefined) {
            return 0;
        } else {
            return sum / (double) (keySet.size() - notDefined);
        }
    }

}
