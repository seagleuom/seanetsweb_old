
package gr.uom.java.ast.metrics.repo;

import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import gr.uom.se.vcs.VCSChange;
import gr.uom.se.vcs.analysis.util.CommitEdits;
import gr.uom.se.vcs.analysis.version.VersionProvider;

/**
 *
 * @author Theodore Chaikalis
 */
public class FilesAddedPerVersion extends FileModificationRepositoryMetric  {
    
     public static final String MNEMONIC = "FILES_ADDED";

    public FilesAddedPerVersion(VersionProvider versionProvider) {
        super(versionProvider);
    }

    @Override
    public Number getMetricValueForAVersion(String version) {
        if(versionProvider.getPrevious(versionProvider.getCommit(version)) == null) {
            // THIS IS FIRST VERSION
            return 0;
        }
        CommitEdits commitEdits = getFileChanges().get(version);
        if(commitEdits == null) return 0;
        return commitEdits.getNumberOfFilesWithChange(VCSChange.Type.ADDED);
    }

    @Override
    public void accept(AbstractMetricVisitor visitor) {visitor.visit(this); }
    
    @Override
    public Number getSystemLevelValue() {return 0; }

    @Override
    public Number getMetricValueForClass(String className) { return 0;  }

}
