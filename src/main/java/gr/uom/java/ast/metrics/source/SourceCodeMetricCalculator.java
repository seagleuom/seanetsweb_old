package gr.uom.java.ast.metrics.source;

import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.metrics.AbstractMetric;
import gr.uom.java.ast.metrics.AbstractMetricCalculator;
import gr.uom.java.ast.metrics.AbstractMetricVisitor;

/**
 *
 * @author Theodore Chaikalis
 */
public class SourceCodeMetricCalculator extends AbstractMetricCalculator {

    private SystemObject systemObject;

    public SourceCodeMetricCalculator(SystemObject systemObject) {
        super();
        this.systemObject = systemObject;
    }

    public SourceCodeMetricCalculator(AbstractMetricVisitor metricVisitor) {
        super(metricVisitor);
    }

    @Override
    public void createAndAddMetric(String metricMnemonic) {
        AbstractMetric metric = new SourceCodeMetricFactory(systemObject).make(metricMnemonic);
        metricsToCalculate.put(metricMnemonic, metric);
    }

    public SystemObject getSystemObject() {
        return systemObject;
    }

    public void setSystemObject(SystemObject systemObject) {
        this.systemObject = systemObject;
    }

    public void createMetrics() {
        createAndAddMetric(CBO.MNEMONIC);
        createAndAddMetric(ConnectivityMetric.MNEMONIC);
        createAndAddMetric(LCOM.MNEMONIC);
        createAndAddMetric(MMIC.MNEMONIC);
        createAndAddMetric(NOF.MNEMONIC);
        createAndAddMetric(NOM.MNEMONIC);
        createAndAddMetric(WMC.MNEMONIC);
        createAndAddMetric(LOC.MNEMONIC);
    }

}
