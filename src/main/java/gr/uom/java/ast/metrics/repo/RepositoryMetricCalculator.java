package gr.uom.java.ast.metrics.repo;

import gr.uom.java.ast.metrics.AbstractMetric;
import gr.uom.java.ast.metrics.AbstractMetricCalculator;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.Analyzer;
import gr.uom.se.vcs.analysis.Analyzer.Builder;
import gr.uom.se.vcs.analysis.CommitAnalyzer;
import gr.uom.se.vcs.analysis.version.TagVersionProvider;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

/**
 *
 * @author Theodore Chaikalis
 */
public class RepositoryMetricCalculator extends AbstractMetricCalculator {

    private VCSRepository repository;
    private final TagVersionProvider versionProvider;

    public RepositoryMetricCalculator(VCSRepository repository) throws VCSRepositoryException {
        this.repository = repository;
        versionProvider = new TagVersionProvider(repository);
    }

    @Override
    public void createAndAddMetric(String metricMnemonic) {
        AbstractMetric metric = new RepositoryMetricFactory(versionProvider).make(metricMnemonic);
        if(metric != null)
            metricsToCalculate.put(metricMnemonic, metric);
    }

    public VCSRepository getRepository() {
        return repository;
    }

    public void setRepository(VCSRepository repository) {
        this.repository = repository;
    }

    public TagVersionProvider getVersionProvider() {
        return versionProvider;
    }

    public void makeCalculations() throws VCSRepositoryException, InterruptedException {
        
        Analyzer.Builder<VCSCommit> builder = Analyzer.<VCSCommit>builder();
        for (String metricMnem : metricsToCalculate.keySet()) {
            AbstractMetric metric = metricsToCalculate.get(metricMnem);
            if (metric instanceof RepositoryMetric) {
                RepositoryMetric repoMetric = (RepositoryMetric) metric;
                builder.addParallel(repoMetric.getProcessor());
            }
        }
        CommitAnalyzer analyzer = builder.setThreads(8).
                setTaskQueueSize(1000).
                build(CommitAnalyzer.class);
        builder.setTaskQueueSize(1000);
        versionProvider.collectVersionInfo();
        analyzer.start();
        try {
            for (VCSCommit version : versionProvider) {

            // We should process the version commit
                // because getting all commits for a given
                // version will return all commits, but the version commit
                // itself
                analyzer.process(version);

            // Now for the given version we are processing
                // all commits
                for (VCSCommit commit : versionProvider.getCommits(version)) {
                    analyzer.process(commit);
                }
            }
        } 
        finally {
         // Wee need to run the stop in try finally because
            // if a processor had an exception while running it will
            // be thrown at this moment not while running. And if an
            // exception is thrown then we risk to terminate main thread
            // and leave other threads running!!
            try {
                // Always stop the analyzer before getting the results
                analyzer.stop();
            } finally {
            // ALWAYS call shutdown if analyzer is running any of the processors
                // in parallel. Otherwise you will let other threads running.
                analyzer.shutDown();
            }
        }
    }
}
