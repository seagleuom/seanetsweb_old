package gr.uom.java.ast.metrics;

import gr.uom.java.ast.metrics.source.CBO;
import gr.uom.java.ast.metrics.source.MMIC;
import gr.uom.java.ast.metrics.source.NOM;
import gr.uom.java.ast.metrics.source.LCOM;
import gr.uom.java.ast.metrics.source.ConnectivityMetric;
import gr.uom.java.ast.metrics.source.NOF;
import gr.uom.java.ast.metrics.source.WMC;

/**
 *
 * @author Theodore Chaikalis
 */
public class AbstractMetricVisitor { 
    
    public void visit(AbstractMetric am) {
        
    }
    
    public void visit(CBO cbo) {

    }

    public void visit(LCOM lcom) {

    }

    public void visit(WMC wmc) {

    }

    public void visit(ConnectivityMetric connectivity) {

    }

    public void visit(MMIC importCoupling) {

    }
    
    public void visit(NOF numberOfFields) {

    }
    
     public void visit(NOM numberOfMethods) {

    }
    
    
}
