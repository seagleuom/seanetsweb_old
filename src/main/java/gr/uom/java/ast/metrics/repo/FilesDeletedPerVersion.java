
package gr.uom.java.ast.metrics.repo;

import gr.uom.java.ast.metrics.AbstractMetricVisitor;
import gr.uom.se.vcs.VCSChange;
import gr.uom.se.vcs.analysis.util.CommitEdits;
import gr.uom.se.vcs.analysis.version.VersionProvider;

/**
 *
 * @author Theodore Chaikalis
 */
public class FilesDeletedPerVersion extends FileModificationRepositoryMetric  {
    
     public static final String MNEMONIC = "FILES_DELETED";

    public FilesDeletedPerVersion(VersionProvider versionProvider) {
        super(versionProvider);
    }

    @Override
    public Number getMetricValueForAVersion(String version) {
        CommitEdits commitEdits = getFileChanges().get(version);
        if(commitEdits == null) return 0;
        return commitEdits.getNumberOfFilesWithChange(VCSChange.Type.DELETED);
    }

    @Override
    public void accept(AbstractMetricVisitor visitor) {visitor.visit(this); }
    
    @Override
    public Number getSystemLevelValue() {return 0; }

    @Override
    public Number getMetricValueForClass(String className) { return 0;  }

}
