
package gr.uom.java.ast.metrics.repo;

import gr.uom.java.ast.metrics.AbstractMetric;
import gr.uom.se.util.pattern.processor.Processor;
import gr.uom.se.vcs.analysis.version.VersionProvider;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class RepositoryMetric extends AbstractMetric {

    protected VersionProvider versionProvider;
    
    RepositoryMetric(VersionProvider versionProvider) {
        this.versionProvider = versionProvider;
    }
    
    public abstract Number getMetricValueForAVersion(String version);
    public abstract Processor getProcessor();
}
