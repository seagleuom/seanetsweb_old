package gr.uom.java.ast.metrics.repo;

import gr.uom.se.util.pattern.processor.Processor;
import gr.uom.se.vcs.VCSChange;
import gr.uom.se.vcs.VCSFile;
import gr.uom.se.vcs.VCSResource;
import gr.uom.se.vcs.analysis.util.CommitEdits;
import gr.uom.se.vcs.analysis.version.VersionChangeProcessor;
import gr.uom.se.vcs.analysis.version.VersionProvider;
import gr.uom.se.vcs.walker.filter.VCSFilter;
import gr.uom.se.vcs.walker.filter.resource.ResourceFilterUtility;
import gr.uom.se.vcs.walker.filter.resource.VCSResourceFilter;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class FileModificationRepositoryMetric extends RepositoryMetric {

    protected final VersionChangeProcessor fileModificationProcessor;
    private Map<String, CommitEdits> fileModifications;

    public FileModificationRepositoryMetric(VersionProvider versionProvider) {
        super(versionProvider);
        fileModificationProcessor = new VersionChangeProcessor(
                versionProvider, null /* id */, null /* change filter */, null /*
                 * Resource
                 * filter
                 */,
                true /* calculate intermediate changes in each version */,
                true /* calculate changes for two subsequent versions */,
                (VCSChange.Type[]) null /* calculate all types of change */);
    }
    
    public Map<String, CommitEdits> getFileChanges(){
        if(fileModifications == null)
            fileModifications = fileModificationProcessor.getVersionsChanges();
        return fileModifications;
    }

    @Override
    public Processor getProcessor() {
        return fileModificationProcessor;
    }
    
    
    
    final static VCSResourceFilter<VCSFile> javaFilter = ResourceFilterUtility.suffix(".java");
    final static VCSResourceFilter<VCSFile> testDirFilter = new VCSResourceFilter<VCSFile>() {
        @Override
        public boolean include(VCSFile entity) {
            VCSResource parent = entity.getParent();
            while (parent != null) {
                if (parent.getPath().toLowerCase().endsWith("/test")) {
                    return true;
                }
                parent = parent.getParent();
            }
            return false;
        }

        @Override
        public boolean enter(VCSFile resource) {
            // nothing to do here
            return false;
        }
    };
    @SuppressWarnings("unchecked")
    final static VCSFilter<VCSFile> javaTestFilter = ResourceFilterUtility.and(javaFilter, testDirFilter);

}
