package gr.uom.java.ast;

import gr.uom.java.seanets.graph.MyGraph;
import gr.uom.java.seanets.graph.MyJavaProject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;

public class ASTReader {
    
    private final static Logger logger = Logger.getLogger(ASTReader.class.getName());

    public static SystemObject parseASTAndCreateSystemObject(MyJavaProject javaProject) throws Exception {
        ASTParser parser = ASTParser.newParser(AST.JLS4);
        parser.setKind(ASTParser.K_COMPILATION_UNIT);
        Map options = JavaCore.getOptions();
        JavaCore.setComplianceOptions(JavaCore.VERSION_1_7, options);
        parser.setCompilerOptions(options);

        String[] stringDemo = new String[1];

        String[] bindingKeys = new String[javaProject.getJavaFilePaths().size()];

        parser.setEnvironment(null, new String[]{javaProject.getSourceFolder()}, null, true);
        parser.setResolveBindings(true); // we need bindings later on
        parser.setStatementsRecovery(true);
        parser.setBindingsRecovery(true);

        SystemObject systemObject = new SystemObject();
                
        int i = 0;
        for (String javaFilePath : javaProject.getJavaFilePaths()) {
            try {
                bindingKeys[i] = createBindingKeyFromClassFile(javaFilePath, systemObject);
            } catch (IOException e) {
                logger.log(Level.SEVERE,null,e);
            }
            i++;
        }
        String[] canonicalPaths = javaProject.getJavaFilePaths().toArray(stringDemo);
       

        MyASTFileReader myASTFileReader = new MyASTFileReader(systemObject);
        try {
            parser.createASTs(canonicalPaths, null, bindingKeys, myASTFileReader, null);
            return systemObject;
        } catch (Exception e) {
            logger.log(Level.SEVERE,null,e);
            return null;
        }
    }

    public static int sourceCodeLineCounter(String classCode) {
        String code = new Scanner(classCode).useDelimiter("\\A").next();
        // strip comments
        code = Pattern.compile("/\\*.*?\\*/|//.*?$", Pattern.MULTILINE | Pattern.DOTALL).matcher(code).replaceAll("");
        // split into array using non empty lines as delimiters
        String[] s = Pattern.compile("\\S.*?$", Pattern.MULTILINE).split(code.trim());
        // count
        return s.length;
    }

    private static String createBindingKeyFromClassFile(String filePath, SystemObject systemObject) throws IOException {
        String classString = readFileToString(filePath);
        String fullyQualifiedClassName = "";
        try {
            String packageName = "";
            int packageDeclarationStart = classString.indexOf("package");
            if (packageDeclarationStart != -1) {
                int packageDeclarationEnd = classString.indexOf(";", packageDeclarationStart);
                String packageDeclarationLine = classString.substring(packageDeclarationStart, packageDeclarationEnd);
                packageName = packageDeclarationLine.substring(packageDeclarationLine.lastIndexOf("package") + 7, packageDeclarationLine.length());
            } else {
                packageName = "";
            }
            String className = filePath.substring(filePath.lastIndexOf(File.separator), filePath.indexOf(".java"));
            fullyQualifiedClassName = packageName + "." + className + ";";
            fullyQualifiedClassName = fullyQualifiedClassName.replace(".", File.separator);
            systemObject.addLinesOfCodeForAFile(filePath, sourceCodeLineCounter(classString));
        } catch (Exception e) {
            logger.log(Level.SEVERE,"error with class {0}",classString);
        }
        return fullyQualifiedClassName;
    }

    private static String readFileToString(String filePath) throws IOException {
        StringBuilder fileData = new StringBuilder(1000);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[10];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }

}
