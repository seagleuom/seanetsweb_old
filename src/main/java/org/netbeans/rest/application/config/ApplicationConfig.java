
package org.netbeans.rest.application.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Theodore Chaikalis
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(gr.uom.java.seanets.ws.restful.DBGraphFacadeREST.class);
        resources.add(gr.uom.java.seanets.ws.restful.DBProjectFacadeREST.class);
        resources.add(gr.uom.java.seanets.ws.restful.DBSourceMetricFacadeREST.class);
        resources.add(gr.uom.java.seanets.ws.restful.NewCrossOriginResourceSharingFilter.class);
        resources.add(gr.uom.java.seanets.ws.restful.VersionFacadeREST.class);
    }

}
